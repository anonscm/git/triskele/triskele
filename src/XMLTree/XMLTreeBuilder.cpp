#include "XMLTree/XMLTreeBuilder.hpp"

using namespace obelix::triskele;

void
XMLTreeBuilder::buildTree (Tree &tree) {
  TiXmlDocument doc (fileName.c_str ());
  if (!doc.LoadFile ())
    return;

  const TiXmlElement *treeNode = doc.FirstChild ("Tree")->ToElement ();
  int width, height;
  if (treeNode->QueryIntAttribute ("width", &width) == TIXML_SUCCESS &&
      treeNode->QueryIntAttribute ("height", &height) == TIXML_SUCCESS)
    setTreeSize (tree, Size (width, height));
  else {
    if (treeNode->QueryIntAttribute ("leafCount", &width) != TIXML_SUCCESS)
      return;
    setTreeSize (tree, Size (width, 1));
  }

  DimNodeId nodeCount = getNodeCount (treeNode);
  setNodeCount (tree, nodeCount);

  DimNodeId rootParent = nodeCount;
  compParents[--rootParent] = nodeCount;
  readNodeChildren (treeNode->FirstChild ()->ToElement (), rootParent);

  std::partial_sum (childrenStart, childrenStart+nodeCount+2, childrenStart);
  for (DimNodeId i = 0; i < tree.getLeafCount (); ++i) {
    DimNodeId idP = leafParents[i];
    children[childrenStart[idP+1]++] = i;
  }
  for (DimNodeId i = tree.getLeafCount (); i < tree.getLeafCount ()+nodeCount-1; ++i) {
    DimNodeId idP = compParents[i-tree.getLeafCount ()];
    children[childrenStart[idP+1]++] = i;
  }
}

DimNodeId
XMLTreeBuilder::getNodeCount (const TiXmlElement *node) {
  DimNodeId nodeCount = 0;
  for (const TiXmlNode *child = node->FirstChild ("Node"); child; child = child->NextSibling ("Node")) {
    nodeCount++;
    nodeCount += getNodeCount (child->ToElement ());
  }
  return nodeCount;
}

void
XMLTreeBuilder::readNodeChildren (const TiXmlElement *node, DimNodeId &id) {
  DimNodeId idP = id;
  for (const TiXmlNode *child = node->FirstChild (); child; child = child->NextSibling ()) {
    if (std::string (child->Value ()) == "Node") {
      compParents[--id] = idP;
      std::cout << id << std::endl;
      readNodeChildren (child->ToElement (), id);
      childrenStart[idP+2]++;
    } else if (child->Value () == std::string ("Leaf")) {
      int leafId;
      child->ToElement ()->QueryIntAttribute ("id", &leafId);
      leafParents[leafId] = idP;
      childrenStart[idP+2]++;
    }
  }
}

void
XMLTreeBuilder::exportToFile (const Tree &tree, const std::string &fileName) {
  TiXmlDocument doc;
  TiXmlDeclaration *decl = new TiXmlDeclaration ( "1.0", "", "" );
  doc.LinkEndChild(decl);

  TiXmlElement *treeNode = new TiXmlElement ("Tree");
  doc.LinkEndChild (treeNode);
  treeNode->SetAttribute ("leafCount", tree.getLeafCount ());

  if (tree.getSize ().width != 0)
    treeNode->SetAttribute ("width", tree.getSize ().width);
  if (tree.getSize().height != 0)
    treeNode->SetAttribute ("height", tree.getSize ().height);

  // Construct the tree
  writeNodeChildren (tree, tree.getNodeRoot (), treeNode);
  doc.SaveFile (fileName.c_str ());
}

void
XMLTreeBuilder::writeNodeChildren (const Tree &tree, const DimNodeId &id, TiXmlElement *node) {
  if (id < tree.getLeafCount ()) {
    TiXmlElement *leafNode = new TiXmlElement ("Leaf");
    node->LinkEndChild (leafNode);
    leafNode->SetAttribute ("id", id);
    return;
  }
  TiXmlElement *nodeNode = new TiXmlElement ("Node");
  node->LinkEndChild (nodeNode);

  tree.forEachChild (id - tree.getLeafCount (),
  		     [&tree, &nodeNode] (const DimNodeId &childId) {
  		       writeNodeChildren (tree, childId, nodeNode);
  		     });
}
