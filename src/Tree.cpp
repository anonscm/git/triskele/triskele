#include "triskeleBase.hpp"
#include "Tree.hpp"
#include "Border.hpp"
#include "ArrayTree/GraphWalker.hpp"

using namespace std;
using namespace obelix;
using namespace obelix::triskele;
  
// ================================================================================
Tree::Tree (const Size &size, DimCore coreCount)
  : Tree (coreCount)
{
  resize (size);
}

Tree::Tree (DimCore coreCount)
  : coreCount (coreCount),
    size (),
    leafCount (0),
    nodeCount (0),
    leafParents (),
    compParents (nullptr),
    childrenStart (),
    children (),
    state (State::Void)
{
  clear ();
}

Tree::~Tree () {
  free ();
}

// ================================================================================
void
Tree::clear () {
  nodeCount = leafCount;
  leafParents.assign (leafCount*2, DimImg_MAX);
  weightBounds.resize (0);
  if (!leafCount)
    return;
#ifdef USE_SMART_LOG
  children.assign ((leafCount-1)*2, 0);
  childrenStart.assign (leafCount+1, 0);
#endif
  childrenStart.resize (leafCount+1);
  childrenStart[0] = childrenStart[1] = 0;
}

void
Tree::resize (const Size &size) {
  this->size = size;
  book (size.getPixelsCount ());
}

void
Tree::free () {
  leafParents = vector<DimImg> ();
  compParents = nullptr;
  children = vector<DimNodeId> ();
  childrenStart = vector<DimImg> ();
  weightBounds = vector<DimImg> ();
}

void
Tree::book (const DimImg &leafCount) {
  this->leafCount = leafCount;
  clear ();
  compParents = &leafParents[leafCount];
  children.resize ((leafCount-1)*2);
  childrenStart.resize (leafCount+1);
}

// ================================================================================
bool
Tree::compareTo (const Tree &tree, bool testChildren) const {
  const DimImg leafCount = getLeafCount ();
  const DimImg compCount = getCompCount ();
  DEF_LOG ("Tree::compareTo", "leafCount: " << leafCount << " compCount: " << compCount);

  if (leafCount != tree.getLeafCount () ||
      compCount != tree.getCompCount ()) {
    cerr
      << "leafCount: " << leafCount << " ?= " << tree.getLeafCount () << endl
      << "compCount: " << compCount << " ?= " << tree.getCompCount () << endl
      << flush;
    return false;
  }

  vector<DimImg> pearComps (compCount, DimImg_MAX);

  bool result = true;
  // test parent leaves
  for (DimImg lId = 0; lId < leafCount; ++lId) {
    const DimImg parentId = getLeafParent (lId);
    if (parentId == DimImg_MAX) {
      cerr
	<< "leafId: " << lId << " parentId: max value" << endl
	<< flush;
      result = false;
      break;
    }
    const DimImg pearParentId = tree.getLeafParent (lId);
    LOG ("leafId: " << lId << " parentId: " << parentId << " pearParentId: " << pearParentId);
    const DimImg pearCompId = pearComps[parentId];
    if (pearCompId == DimImg_MAX) {
      pearComps[parentId] = pearParentId;
      continue;
    }
    if (pearCompId != pearParentId) {
      cerr
	<< "leafId: " << lId << " not same parent" << endl
	<< flush;
      result = false;
      break;
    }
  }
  // test parent des comps
  for (DimImg compId = 0; compId < compCount; ++compId) {
    const DimImg pearCompId = pearComps[compId];
    if (pearCompId == DimImg_MAX) {
      cerr
	<< "Reference tree may be not ordered (" << compId << ")" << endl
	<< flush;
      result = false;
      break;
    }
    const DimImg parentId = getCompParent (compId);
    const DimImg pearParentId = tree.getCompParent (pearCompId);
    const DimImg pearCompParentId = pearComps[parentId];
    if (pearCompParentId == DimImg_MAX) {
      pearComps[compId] = pearParentId;
      continue;
    }
    if (pearCompParentId != pearParentId) {
      cerr
	<< "compId: " << compId << " not same parent" << endl
	<< flush;
      result = false;
      break;
    }
  }

  if (testChildren && result) {
    // test children
    for (DimImg compId = 0; compId < compCount; ++compId) {
      const DimImg pearCompId = pearComps[compId];
      const DimImg childrenCount = getChildrenCount (compId);
      if (childrenCount != tree.getChildrenCount (pearCompId)) {
	cerr
	  << "compId: " << pearCompId << " not same childrenCount" << endl
	  << flush;
	result = false;
	break;
      }
      for (DimImg childId = 0; childId < childrenCount; ++childId) {
	DimNodeId nodeId = tree.getChild (pearCompId, childId);
	if (pearCompId != tree.isLeaf (nodeId) ?
	    tree.getLeafParent (tree.getLeafId (nodeId)) :
	    tree.getCompParent (tree.getCompId (nodeId))) {
	  cerr
	    << "not same child for comp: " << pearCompId << " child: " << childId << endl
	    << flush;
	  result = false;
	  break;
	}
      }
    }
  }
  return result;
}

// ================================================================================
void
Tree::checkSpare (const Border &border) const {
  GraphWalker graphWalker (border);
  vector<Rect> tiles;
  vector<Rect> boundaries;
  vector<TileShape> boundaryAxes;
  graphWalker.setTiles (coreCount, Rect (NullPoint, size), tiles, boundaries, boundaryAxes);

  vector<DimImg> vertexMaxBounds;
  vector<DimImg> edgesMaxBounds;
  graphWalker.setMaxBounds (tiles, vertexMaxBounds, edgesMaxBounds);
  DimCore tileCount = tiles.size ();
  vector<DimImg> maxParents (tileCount);

  // check parents
  for (DimCore i = 0; i < tileCount; ++i) {
    DimImg base = vertexMaxBounds [i], top = vertexMaxBounds [i+1];
    DimImg &maxParent = maxParents [i];
    graphWalker.forEachVertexIdx (tiles[i], [this, &base, &top, &maxParent] (DimImg leafId) {
	BOOST_ASSERT (leafParents[leafId] >= base);
	BOOST_ASSERT (leafParents[leafId] < top);
	if (maxParent < leafParents[leafId])
	  maxParent = leafParents[leafId];
      });
  }
  // DimImg *childCount = (DimImg*)&childrenStart[2]; // Only used for the assert (so put to comment to prevent warnings)
  for (DimCore i = 0; i < tileCount; ++i) {
    DimImg base = vertexMaxBounds [i], maxParent = maxParents [i];
    for (DimImg compId = base; compId < maxParent; ++compId) {
      BOOST_ASSERT (compParents[compId] > base);
      BOOST_ASSERT (compParents[compId] != compId);
      BOOST_ASSERT (compParents[compId] < maxParent);
      if (compParents[compId] < compId)
	BOOST_ASSERT (childrenStart[2 + compParents[compId]] > childrenStart[2 + compId]);
      //BOOST_ASSERT (childCount[compParents[compId]] > childCount[compId]); // Edited line to prevent the "Unused variable" warning
    }
    BOOST_ASSERT (compParents[maxParent] == DimImg_MAX);
  }
  {
    vector<bool> parentsMap (vertexMaxBounds[tileCount], false);
    for (DimCore i = 0; i < tileCount; ++i) {
      graphWalker.forEachVertexIdx (tiles[i], [this, &parentsMap] (DimImg leafId) {
	  parentsMap [leafParents [leafId]] = true;
	});
      DimImg base = vertexMaxBounds [i], maxParent = maxParents [i];
      for (DimImg compId = base; compId < maxParent; ++compId)
	parentsMap [compParents [compId]] = true;
      for (DimImg compId = base; compId < maxParent; ++compId)
	BOOST_ASSERT (parentsMap [compId]);
    }
  }
  // check weight
  // XXX monotone

  // check childrenStart
  
}

// ================================================================================
void
Tree::check (const Border& border) const {
  GraphWalker graphWalker (border);
  DimImg compCount = getCompCount ();
  BOOST_ASSERT (compCount < leafCount);
  // check parents
  graphWalker.forEachVertexIdx ([this, &compCount] (const DimImg &leafId) {
      // XXX si border => leafParents [leafId] == DimImg_MAX
      BOOST_ASSERT (leafParents[leafId] < compCount);
    });
  forEachComp ([this, &compCount] (const DimImg &compId) {
      if (compId == compCount-1)
	BOOST_ASSERT (compParents[compId] == DimImg_MAX);
      else {
	BOOST_ASSERT (compParents[compId] > compId);
	BOOST_ASSERT (compParents[compId] < compCount);
      }
    });
  {
    vector<bool> parentsMap (compCount, false);
    //parentsMap.assign (compCount, false);
    forEachNode ([this, &parentsMap] (const DimNodeId &nodeId) {
	if (leafParents[nodeId] == DimImg_MAX)
	  return;
	parentsMap [leafParents [nodeId]] = true;
      });
    for (DimImg compId = 0; compId < compCount; ++compId)
      BOOST_ASSERT (parentsMap [compId]);
  }
  // check weight

  // check weightBounds
  
  // check childrenStart
  {
    vector<DimImg> childrenStart2 (compCount, 0);
    forEachLeaf ([this, &childrenStart2] (const DimImg &leafId) {
	if (leafParents[leafId] == DimImg_MAX)
	  // boder
	  return;
	++childrenStart2 [leafParents [leafId]];
      });
    forEachComp ([this, &childrenStart2, &compCount] (const DimImg &compId) {
	if (compId == compCount-1)
	  return;
	++childrenStart2 [compParents [compId]];
      });
    for (DimImg compId = 0; compId < compCount; ++compId) {
      // check count
      BOOST_ASSERT (childrenStart2 [compId] = childrenStart [compId+1] - childrenStart[compId]);
      // at least 2 children
      BOOST_ASSERT (childrenStart2 [compId] > 1);
    }
  }

  // check children
  {
    vector<DimNodeId> childrenMap (nodeCount, DimImg_MAX);
    forEachComp ([this, &childrenMap] (const DimImg &compId) {
	DimNodeId minChild = childrenStart[compId], maxChild = childrenStart[compId+1];
	for (DimNodeId childId = minChild; childId < maxChild; ++childId) {
	  DimNodeId child = children[childId];
	  BOOST_ASSERT (leafParents [child] == compId);
	  BOOST_ASSERT (childrenMap [child] == DimImg_MAX);
	  childrenMap [child] = compId;
	}
	for (DimNodeId childId = minChild+1; childId < maxChild; ++childId) {
	  BOOST_ASSERT (children[childId-1] <  children[childId]);
	}
      });
    for (DimNodeId child = 0; child < nodeCount-1; ++child)
      BOOST_ASSERT (childrenMap [child] != DimImg_MAX || (child < leafCount && border.isBorder (child)));
  }
}

// ================================================================================
Tree::CPrintTree::CPrintTree (const Tree &tree, DimNodeId nodeCount)
  : tree (tree),
    onRecord (nodeCount),
    nodeCount (onRecord ? nodeCount : tree.getNodeCount ()) {
}

ostream &
Tree::CPrintTree::print (ostream &out) const {
  Size doubleSize (tree.size.width, 2*tree.size.height);
  out << "Tree::printTree: leafCount:" << tree.leafCount
      << " nodeCount:" << (onRecord ? "~" : "") << nodeCount << " compCount:" << nodeCount - tree.leafCount << endl
      << "parent count" << (onRecord ? "" : " children") << endl
      << printMap (&tree.leafParents[0], doubleSize, nodeCount) << endl << endl
      << printMap (&tree.childrenStart[onRecord ? 2 : 0], tree.size, nodeCount - tree.leafCount + 1) << endl << endl;
  if (!onRecord)
    out << printMap (&tree.children[0], doubleSize, nodeCount-1) << endl << endl;
  if (tree.weightBounds.size ()) {
    return out << "weightBounds: " << endl
	       << printMap (&tree.weightBounds[0], tree.size, tree.weightBounds.size ()) << endl << endl;
  }
  return out << "no weightBounds" << endl << endl;
}

Tree::CPrintTree
Tree::printTree (DimNodeId nodeCount) const {
  return CPrintTree (*this, nodeCount);
}

// ostream &
// operator << (ostream& out, const Tree::CPrintTree &cpt) {
//   return cpt.print (out);
// }

// ================================================================================
