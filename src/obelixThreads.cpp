#include "triskeleBase.hpp"

#include "obelixThreads.hpp"
using namespace std;
using namespace obelix;
#include "obelixThreads.tcpp"

// ================================================================================
template<typename DimImgT>
vector<DimImgT>
obelix::getDealThreadBounds (const DimImgT &maxId, const DimCore &coreCount) {
  if (!maxId || !coreCount)
    return vector<DimImgT> (0);
  if (maxId < coreCount) {
    vector<DimImgT> maxIds (maxId+1);
    for (DimImgT core = 0; core <= maxId; ++core)
      maxIds[core] = core;
    return maxIds;
  }
  DimImgT average = min (DimImgT (1), DimImgT (maxId/coreCount));
  vector<DimImgT> maxIds (coreCount+1);
  for (DimCore core = 0; core < coreCount; ++core)
    maxIds[core] = DimImgT (core*average);
  maxIds[coreCount] = maxId;
  return maxIds;
}

// ================================================================================
