
#include "obelixThreads.hpp"
#include "triskeleBase.hpp"
#include "AttributeProfiles.hpp"

using namespace std;
using namespace obelix;
using namespace obelix::triskele;
#include "AttributeProfiles.tcpp"

// ================================================================================
template<typename PixelT>
AttributeProfiles<PixelT>::AttributeProfiles (const Tree &tree)
  : tree (tree),
    leafCount (0) {
  DEF_LOG ("AttributeProfiles::AttributeProfiles", "");
  updateTranscient ();
}

template<typename PixelT>
AttributeProfiles<PixelT>::~AttributeProfiles () {
  DEF_LOG ("AttributeProfiles::~AttributeProfiles", "");
  free ();
}

template<typename PixelT>
void
AttributeProfiles<PixelT>::updateTranscient () {
  DEF_LOG ("AttributeProfiles::updateTranscient", "");
  book (tree.getLeafCount ());
}

template<typename PixelT>
PixelT *
AttributeProfiles<PixelT>::getValues () {
  return &values[0];
}

template<typename PixelT>
const PixelT *
AttributeProfiles<PixelT>::getValues () const {
  return &values[0];
}

// ================================================================================
template<typename PixelT>
template<typename WeightT>
void
AttributeProfiles<PixelT>::setValues (const PixelT defaultValue, const WeightT *weights) {
  updateTranscient ();
  fill_n (&values[0], tree.getLeafCount (), defaultValue);
  PixelT *compAP = &values[tree.getLeafCount ()];
  dealThread (tree.getCompCount (), tree.getCoreCount (), [&compAP, &weights] (const DimCore &threadId, const DimImg &minVal, const DimImg &maxVal) {
      for (DimImg compIdx = minVal; compIdx < maxVal; ++compIdx)
	compAP[compIdx] = weights[compIdx];
    });
}

template<typename PixelT>
template<typename WeightT>
void
AttributeProfiles<PixelT>::setValues (const PixelT *pixels, const WeightT *weights) {
  updateTranscient ();
  PixelT *leafAP = &values[0];
  dealThread (tree.getLeafCount (), tree.getCoreCount (), [&leafAP, &pixels] (const DimCore &threadId, const DimImg &minVal, const DimImg &maxVal) {
      for (DimImg i = minVal; i < maxVal; ++i)
	leafAP[i] = pixels [i];
    });
  PixelT *compAP = leafAP+tree.getLeafCount ();
  dealThread (tree.getCompCount (), tree.getCoreCount (), [&compAP, &weights] (const DimCore &threadId, const DimImg &minVal, const DimImg &maxVal) {
      for (DimImg compIdx = minVal; compIdx < maxVal; ++compIdx)
	compAP[compIdx] = weights[compIdx];
    });
}

// ================================================================================
template<typename PixelT>
void
AttributeProfiles<PixelT>::free () {
  DEF_LOG ("AttributeProfiles::free", "");
  values = vector<PixelT> ();
}

template<typename PixelT>
void
AttributeProfiles<PixelT>::book (const DimImg &leafCount) {
  DEF_LOG ("AttributeProfiles::book", "");
  this->leafCount = leafCount;
  // XXX max : leafCount*2-1
  values.resize (leafCount*2);
}

// ================================================================================
template<typename PixelT>
ostream &
AttributeProfiles<PixelT>::print (ostream &out) const {
  const Size doubleSize (tree.getSize().width, 2*tree.getSize ().height);
  out << "AP" << endl
      << printMap (&values[0], doubleSize, tree.getNodeCount ()) << endl << endl;
  return out;
}
// ================================================================================
