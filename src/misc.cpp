#include <fstream>
#include <boost/algorithm/string.hpp>
#include <boost/assign.hpp>
#include <boost/filesystem.hpp>
#include <iomanip>
#include <time.h>
#include <ctime>
#include <sstream>

#if !defined(_WIN32) && !defined(_WIN64)
#include <sys/ioctl.h>
#endif

#include "ArrayTree/triskeleArrayTreeBase.hpp"
#include "obelixThreads.hpp"
#include "triskeleBase.hpp"
#include "IImage.hpp"

using namespace obelix;
using namespace obelix::triskele;
#include "misc.hpp"
#include "misc.tcpp"

using namespace std;
using namespace boost::filesystem;

using namespace obelix;

// ================================================================================
string
obelix::getOsName () {
#ifdef __linux__
  return "Linux";
#elif __FreeBSD__
  return "FreeBSD";
#elif __unix
  return "Unix";
#elif __unix__
  return "Unix";
#elif __APPLE__
  return "Mac OSX";
#elif __MACH__
  return "Mac OSX";
#elif _WIN32
  return "Windows 32-bit";
#elif _WIN64
  return "Windows 64-bit";
#else
  return "Unknowned OS";
#endif
}

// ================================================================================
uint16_t
obelix::getCols () {
#if defined (_WIN32) || defined (_WIN64)
  return 80;
#else
  struct winsize w;
  ioctl (0, TIOCGWINSZ, &w);
  return w.ws_col;
#endif
}

// ================================================================================
// création exhaustive des templates utilisables

template ostream &obelix::operator << <TreeType>(ostream &out, const vector<TreeType> &v);
template ostream &obelix::operator << <DimImg>(ostream &out, const vector<DimImg> &v);
template ostream &obelix::operator << <double>(ostream &out, const vector<double> &v);
template ostream &obelix::operator << <Size>(ostream &out, const vector<Size> &v);

// ================================================================================
template <typename T>
ostream&
obelix::operator << (ostream& out, const vector<T> &v) {
  if (!v.empty ()) {
    out << "{";
    copy (v.begin (), v.end (), ostream_iterator<T> (out, ", "));
    out << "\b\b}";
  }
  return out;
}

// ================================================================================
string
obelix::getCurrentTime (const string &format) {
  time_t rawtime = time (nullptr);

#if defined(_WIN32) || defined(_WIN64)
  struct tm timeinfo, *timeinfoP (&timeinfo);
  localtime_s (&timeinfo, &rawtime);
#else
  struct tm  *timeinfoP = localtime (&rawtime);
#endif

  ostringstream oss;
  oss << put_time (timeinfoP, format.c_str ());
  return oss.str ();
}

// ================================================================================
string
obelix::ns2string (const double &delta) {
  using namespace boost::chrono;

  ostringstream oss;
  duration<double> ns (delta);
  oss.fill ('0');
  // typedef duration<int, ratio<86400> > days;
  // auto d = duration_cast<days>(ns);
  // ns -= d;
  auto h = duration_cast<hours> (ns);
  ns -= h;
  auto m = duration_cast<minutes> (ns);
  ns -= m;
  oss << setw (2) << h.count () << ":"
      << setw (2) << m.count () << ":"
      << setw (9) << fixed << setprecision (6) << ns.count ();
  return oss.str ();
}

// ================================================================================
string
obelix::addExtensionIfNone (const string &fileName, const string &ext) {
  path p (fileName);
  if (!p.has_extension ())
    return fileName+ext;
  return fileName;
}
string
obelix::replaceExtension (const string &fileName, const string &ext) {
  path p (fileName);
  return p.replace_extension (ext).string ();
}

// ================================================================================
template<typename PixelT>
void
obelix::computeNdvi (const Raster<PixelT> &inputRaster, Raster<PixelT> &ndviRaster, const bool &minChanel, const bool &reverse) {
  DEF_LOG ("obelix::computeNdvi", "minChanel: " << minChanel << " reverse:" << reverse);
  DimImg pixelCount = inputRaster.getSize ().width*inputRaster.getSize ().height;
  LOG ("input: " << inputRaster.getSize () << " ndvi: " << ndviRaster.getSize () << " pixelCount: " << pixelCount);
  BOOST_ASSERT (inputRaster.getSize () == ndviRaster.getSize ());
  const PixelT *inputPixels (inputRaster.getPixels ());
  PixelT *ndviPixels (ndviRaster.getPixels ());

  if (minChanel) {
    LOG ("minChanel");
    for (DimImg pixelId = 0; pixelId < pixelCount; ++pixelId, ++inputPixels, ++ndviPixels)
      *ndviPixels = *inputPixels;
    return;
  }
  int bits = 8*sizeof (PixelT);
  double halfMaxPT = ((1UL << bits) - 1)/2;
  LOG ("maxChanel halfMaxPT:" << halfMaxPT);
  for (DimImg pixelId = 0; pixelId < pixelCount; ++pixelId, ++inputPixels, ++ndviPixels) {
    double q = double (*ndviPixels) + double (*inputPixels);
    if (q == 0.) {
      *ndviPixels = 0;
      continue;
    }
    q = (double (*ndviPixels) - double (*inputPixels)) / q;
    if (reverse)
      q = -q;
    *ndviPixels = PixelT ((q+1.)*halfMaxPT);
  }
}

// ================================================================================
template<typename PixelT>
void
obelix::computeSobel (const Raster<PixelT> &inputRaster, Raster<PixelT> &sobelRaster, vector<vector<double> > gTmp, const unsigned int &coreCount) {
  DEF_LOG ("obelix::computeSobel", "");
  // XXX 2D only
  const PixelT *inputPixels (inputRaster.getPixels ());
  PixelT *sobelPixels (sobelRaster.getPixels ());
  Size size = inputRaster.getSize ();
  DimImg pixelsCount (size.width*size.height);
  gTmp.resize (3);
  for (int i = 0; i < 3; ++i)
    gTmp[i].resize (pixelsCount);
  double *gTmp1 (&gTmp[0][0]);
  double *gTmp2 (&gTmp[1][0]);
  double *gTmp3 (&gTmp[2][0]);

  // tmp1 = img*[-1 0 1]
  dealThread (size.height, coreCount, [&size, &gTmp1, &inputPixels] (const DimCore &threadId, const DimImg &minVal, const DimImg &maxVal) {
      for (DimImg line = minVal; line < maxVal; ++line) {
	DimImg y = line*size.width;
	for (DimImg prevX = 0, x = 1; x < size.width; ++prevX, ++x)
	  gTmp1 [prevX+y] = inputPixels [x+y];
	for (DimImg prevX = 0, x = 1; x < size.width; ++prevX, ++x)
	  gTmp1 [x+y] -= inputPixels [prevX+y];
      }
    });
  //             [1]
  // tmp2 = tmp1*[2]
  //             [1]
  dealThread (size.width, coreCount, [&size, &gTmp2, &gTmp1] (const DimCore &threadId, const DimImg &minVal, const DimImg &maxVal) {
      DimImg pixelCount = size.width*size.height;
      for (DimImg x = minVal; x < maxVal; ++x) {
	for (DimImg y = 0; y < pixelCount; y += size.width)
	  gTmp2 [x+y] = 2*gTmp1 [x+y];
	for (DimImg prevY = 0, y = size.width; y < pixelCount; prevY = y, y += size.width) {
	  gTmp2 [x+prevY] += gTmp1 [x+y];
	  gTmp2 [x+y] += gTmp1 [x+prevY];
	}
      }
    });
  //            [-1]
  // tmp1 = img*[ 0]
  //            [ 1]
  dealThread (size.width, coreCount, [&size, &gTmp1, &inputPixels] (const DimCore &threadId, const DimImg &minVal, const DimImg &maxVal) {
      DimImg pixelCount = size.width*size.height;
      for (DimImg x = minVal; x < maxVal; ++x) {
	for (DimImg prevY = 0, y = size.width; y < pixelCount; prevY = y, y += size.width)
	  gTmp1 [x+prevY] = inputPixels [x+y];
	for (DimImg prevY = 0, y = size.width; y < pixelCount; prevY = y, y += size.width)
	  gTmp1 [x+y] -= inputPixels [x+prevY];
      }
    });
  // tmp3 = tmp1*[1 2 1]
  dealThread (size.height, coreCount, [&size, &gTmp3, &gTmp1] (const DimCore &threadId, const DimImg &minVal, const DimImg &maxVal) {
      for (DimImg line = minVal; line < maxVal; ++line) {
	DimImg y = line*size.width;
	for (DimImg x = 0; x < size.width; ++x)
	  gTmp3 [x+y] = 2*gTmp1 [x+y];
	for (DimImg prevX = 0, x = 1; x < size.width; ++prevX, ++x) {
	  gTmp3 [prevX+y] += gTmp1 [x+y];
	  gTmp3 [x+y] += gTmp1 [prevX+y];
	}
      }
    });
  // g = abs(tmp2)+abs(tmp3)/8
  dealThread (size.width, coreCount, [&size, &sobelPixels, &gTmp2, gTmp3] (const DimCore &threadId, const DimImg &minVal, const DimImg &maxVal) {
      DimImg pixelCount = size.width*size.height;
      for (DimImg x = minVal; x < maxVal; ++x) {
	for (DimSideImg y = 0; y < pixelCount; y += size.width)
	  sobelPixels [x+y] = PixelT ((std::abs (gTmp2 [x+y]) + std::abs (gTmp3 [x+y]))/8);
      }
    });
}

// ================================================================================
