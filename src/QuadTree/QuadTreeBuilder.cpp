#include "QuadTree/QuadTreeBuilder.hpp"

using namespace obelix;
using namespace obelix::triskele;

// ================================================================================
void
QuadTreeBuilder::buildTree (Tree &tree) {
  // Building the tree by setting leafs's and components's parents
  setTreeSize (tree, Size (width, height));
  DimImg parentCount = getStepCount (0, 0, width, height);
  DimImg parentUsed = parentCount;

  // Set the root
  compParents[parentCount-1] = parentCount;
  tree.setNodeCount (parentCount);
  setParents (parentCount, 0, 0, width, height, width, height);

  // Building children array
  std::partial_sum (childrenStart, childrenStart+parentUsed+2, childrenStart);
  for (DimNodeId i = 0; i < tree.getLeafCount (); ++i) {
    DimNodeId idP = leafParents[i];
    children[childrenStart[idP+1]++] = i;
  }

  for (DimNodeId i = tree.getLeafCount (); i < tree.getLeafCount ()+parentUsed-1; ++i) {
    DimNodeId idP = compParents[i-tree.getLeafCount ()];
    children[childrenStart[idP+1]++] = i;
  }
}

// ================================================================================
DimImg
QuadTreeBuilder::getStepCount (const DimSideImg &x, const DimSideImg &y, const DimSideImg &width, const DimSideImg &height) const {
  DimImg nbSteps = 1;
  if (width <= 2 && height <= 2)
    return nbSteps;

  DimSideImg dw1 = width >> 1;
  DimSideImg dh1 = height >> 1;
  DimSideImg dw2 = (width >> 1) + (width & 1);
  DimSideImg dh2 = (height >> 1) + (height & 1);
  DimSideImg coords [4][4] = {
    {x      , y      , dw1, dh1},
    {x + dw1, y      , dw2, dh1},
    {x      , y + dh1, dw1, dh2},
    {x + dw1, y + dh1, dw2, dh2}
  };

  for (int i = 0; i < 4; i++)
    nbSteps += getStepCount (coords[i][0], coords[i][1], coords[i][2], coords[i][3]);
  return nbSteps;
}

DimImg
QuadTreeBuilder::setParents (DimImg &parentId,
			     const DimSideImg &x, const DimSideImg &y, const DimSideImg &width, const DimSideImg &height,
			     const DimSideImg &imgWidth, const DimSideImg &imgHeight, DimImg level) const {
  DEF_LOG ("setParents", "parentId: " << parentId << " x: " << x << " y: " << y << " w: " << width << " h: " << height);
  DimImg localId = --parentId;
  
  if (width <= 2 && height <= 2) {
    for (DimSideImg i = x; i < width + x; i++)
      for (DimSideImg j = y; j < height + y; j++) {
	DimSideImg id = i + j * imgWidth;
	leafParents[id] = localId;
	childrenStart[localId+2]++;
      }
    return localId;
  }
  DimSideImg dw1 = width >> 1;
  DimSideImg dh1 = height >> 1;
  DimSideImg dw2 = (width >> 1) + (width & 1);
  DimSideImg dh2 = (height >> 1) + (height & 1);
  DimSideImg coords [4][4] = {
    {x      , y      , dw1, dh1},
    {x + dw1, y      , dw2, dh1},
    {x      , y + dh1, dw1, dh2},
    {x + dw1, y + dh1, dw2, dh2}
  };
  childrenStart[localId+2] = 4;
  for (int i = 0; i < 4; i++)
    compParents[setParents (parentId, coords[i][0], coords[i][1], coords[i][2], coords[i][3], imgWidth, imgHeight, level+1)] = localId;
  return localId;
}

// ================================================================================
