#include <boost/chrono.hpp>
using namespace boost::chrono;

#include "Attributes/MoIAttributes.hpp"
using namespace obelix::triskele;
#include "MoIAttributes.tcpp"

// ================================================================================
MoIAttributes::MoIAttributes (const Tree &tree, const AreaAttributes &areaAttributes, const XYAttributes &xyAttributes)
  : CompAttribute<double> (tree) {
  DEF_LOG ("MoIAttributes::MoIAttributes", "");
  compute (areaAttributes, xyAttributes);
}

MoIAttributes::~MoIAttributes () {
  DEF_LOG ("MoIAttributes::~MoIAttributes", "");
}

// ================================================================================
template<typename PixelT>
void
MoIAttributes::cut (vector<vector<PixelT> > &allBands, const AttributeProfiles<PixelT> &attributeProfiles,
		   const vector<double> &thresholds) const {
  DEF_LOG ("MoIAttributes::cut", "thresholds:" << thresholds.size ());
  if (!thresholds.size ())
    return;
  auto start = high_resolution_clock::now ();
  double maxValue = CompAttribute<double>::getMaxValue ();
  cerr << "moi max value:" << maxValue << endl;
  CompAttribute<double>::cut (allBands, attributeProfiles, 0,
			      CompAttribute<double>::getScaledThresholds (thresholds, maxValue));
  TreeStats::global.addTime (filteringStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}

// ================================================================================
void
MoIAttributes::compute (const AreaAttributes &areaAttributes, const XYAttributes &xyAttributes) {
  DEF_LOG ("MoIAttributes::compute", "");
  auto start = high_resolution_clock::now ();
  const AverageXY *xy = xyAttributes.getValues ();
  const DimImg *areas = areaAttributes.getValues ();
  computeSameCompLevel ([this, &xy, &areas] (const DimImg &parentId) {
      double tmpMoI = 0.;
      const double xa = xy[parentId].x;
      const double ya = xy[parentId].y;
      tree.forEachChildTI (parentId, [this, &tmpMoI, &xa, &ya, &xy, &areas, &parentId] (const bool &isLeaf, const DimImg &childId) {
	  if (isLeaf)
	    return;
	  const double dx = xy[childId].x - xa;
	  const double dy = xy[childId].y - ya;
	  tmpMoI += (dx*dx+dy*dy)*areas[childId];
	});
      const double card = areas[parentId];
      CompAttribute<double>::values[parentId] = tmpMoI/(card*card);
    });
  TreeStats::global.addTime (moiStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}

// ================================================================================
