#include <boost/chrono.hpp>
using namespace boost::chrono;

#include "triskeleBase.hpp"
#include "Attributes/AreaAttributes.hpp"
using namespace obelix;
using namespace obelix::triskele;
#include "AreaAttributes.tcpp"


// ================================================================================
AreaAttributes::AreaAttributes (const Tree &tree)
  : CompAttribute<DimImg> (tree) {
  DEF_LOG ("AreaAttributes::AreaAttributes", "");
  compute ();
}

AreaAttributes::~AreaAttributes () {
  DEF_LOG ("AreaAttributes::~AreaAttributes", "");
}

// ================================================================================
template<typename PixelT>
void
AreaAttributes::cut (vector<vector<PixelT> > &allBands, const AttributeProfiles<PixelT> &attributeProfiles,
		     const vector<DimImg> &thresholds) const {
  DEF_LOG ("AreaAttributes::cut", "");
  auto start = high_resolution_clock::now ();
  CompAttribute<DimImg>::cut (allBands, attributeProfiles, 1, thresholds);
  TreeStats::global.addTime (filteringStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}

// ================================================================================
void
AreaAttributes::compute () {
  DEF_LOG ("AreaAttributes::compute", "");
  auto start = high_resolution_clock::now ();
  computeSameCompLevel ([this] (const DimImg &parentId) {
      DimImg area = 0;
      tree.forEachChildTI (parentId, [this, &area] (const bool &isLeaf, const DimImg &childId) {
	  area += isLeaf ? 1 : CompAttribute<DimImg>::values [childId];
	});
      CompAttribute<DimImg>::values [parentId] = area;
    });
  TreeStats::global.addTime (areaStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}

// ================================================================================
