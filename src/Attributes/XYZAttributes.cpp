#include <boost/chrono.hpp>
using namespace boost::chrono;

#include "Attributes/XYZAttributes.hpp"
using namespace obelix::triskele;

// ================================================================================
XYZAttributes::XYZAttributes (const Tree &tree, const AreaAttributes &areaAttributes)
  : CompAttribute<AverageXYZ> (tree) {
  DEF_LOG ("XYZAttributes::XYZAttributes", "");
  compute (areaAttributes);
}

XYZAttributes::~XYZAttributes () {
  DEF_LOG ("XYZAttributes::~XYZAttributes", "");
}

// ================================================================================
void
XYZAttributes::compute (const AreaAttributes &areaAttributes) {
  DEF_LOG ("XYZAttributes::compute", "");
  auto start = high_resolution_clock::now ();
  DimImg const width = CompAttribute<AverageXYZ>::tree.getSize ().width;
  DimImg const height = CompAttribute<AverageXYZ>::tree.getSize ().height;
  const DimImg *areas = areaAttributes.getValues ();
  computeSameCompLevel ([this, &width, &height, &areas] (const DimImg &parentId) {
      AverageXYZ tmpXYZ;
      tree.forEachChildTI (parentId, [this, &tmpXYZ, &width, &height, &areas, &parentId] (const bool &isLeaf, const DimImg &childId) {
	  if (isLeaf) {
	    tmpXYZ.x += childId % width;
	    DimImg yz = childId / width;
	    tmpXYZ.y += yz % height;
	    tmpXYZ.z += yz / height;
	    return;
	  }
	  tmpXYZ.x += CompAttribute<AverageXYZ>::values[childId].x*areas[childId];
	  tmpXYZ.y += CompAttribute<AverageXYZ>::values[childId].y*areas[childId];
	  tmpXYZ.z += CompAttribute<AverageXYZ>::values[childId].z*areas[childId];
	});
      CompAttribute<AverageXYZ>::values[parentId].x = tmpXYZ.x/areas[parentId];
      CompAttribute<AverageXYZ>::values[parentId].y = tmpXYZ.y/areas[parentId];
      CompAttribute<AverageXYZ>::values[parentId].z = tmpXYZ.z/areas[parentId];
    });
  TreeStats::global.addTime (xyzStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}

// ================================================================================
