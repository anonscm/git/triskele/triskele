#include <boost/chrono.hpp>
using namespace boost::chrono;

#include "Attributes/XYAttributes.hpp"
using namespace obelix::triskele;

// ================================================================================
XYAttributes::XYAttributes (const Tree &tree, const AreaAttributes &areaAttributes)
  : CompAttribute<AverageXY> (tree) {
  DEF_LOG ("XYAttributes::XYAttributes", "");
  compute (areaAttributes);
}

XYAttributes::~XYAttributes () {
  DEF_LOG ("XYAttributes::~XYAttributes", "");
}

// ================================================================================
void
XYAttributes::compute (const AreaAttributes &areaAttributes) {
  DEF_LOG ("XYAttributes::compute", "");
  auto start = high_resolution_clock::now ();
  DimImg const width = CompAttribute<AverageXY>::tree.getSize ().width;
  const DimImg *areas = areaAttributes.getValues ();
  computeSameCompLevel ([this, &width, &areas] (const DimImg &parentId) {
      AverageXY tmpXY;
      tree.forEachChildTI (parentId, [this, &tmpXY, &width, &areas, &parentId] (const bool &isLeaf, const DimImg &childId) {
	  if (isLeaf) {
	    tmpXY.x += childId % width;
	    tmpXY.y += childId / width;
	    return;
	  }
	  tmpXY.x += CompAttribute<AverageXY>::values[childId].x*areas[childId];
	  tmpXY.y += CompAttribute<AverageXY>::values[childId].y*areas[childId];
	});
      CompAttribute<AverageXY>::values[parentId].x = tmpXY.x/areas[parentId];
      CompAttribute<AverageXY>::values[parentId].y = tmpXY.y/areas[parentId];
    });
  TreeStats::global.addTime (xyzStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}

// ================================================================================
