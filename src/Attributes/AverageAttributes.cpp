#include <boost/chrono.hpp>
using namespace boost::chrono;

#include "triskeleBase.hpp"
#include "IImage.hpp"
#include "Tree.hpp"
#include "Attributes/AreaAttributes.hpp"
#include "Attributes/AverageAttributes.hpp"
using namespace obelix;
using namespace obelix::triskele;
#include "AverageAttributes.tcpp"

// ================================================================================
template<typename PixelT>
AverageAttributes::AverageAttributes (const Tree &tree, const Raster<PixelT> &raster, const AreaAttributes &areaAttributes)
  : CompAttribute<double> (tree) {
  DEF_LOG ("AverageAttributes::AverageAttributes", "");
  compute (raster, areaAttributes);
}

AverageAttributes::~AverageAttributes () {
  DEF_LOG ("AverageAttributes::~AverageAttributes", "");
}

// ================================================================================
template<typename PixelT>
void
AverageAttributes::compute (const Raster<PixelT> &raster, const AreaAttributes &areaAttributes) {
  DEF_LOG ("AverageAttributes::compute", "");
  auto start = high_resolution_clock::now ();
  const PixelT *pixels = raster.getPixels ();
  const DimImg *areas = areaAttributes.getValues ();
  computeSameCompLevel ([this, &pixels, &areas] (const DimImg &parentId) {
      double tmpAverage = 0.;
      tree.forEachChildTI (parentId, [this, &tmpAverage, &pixels, &areas] (const bool &isLeaf, const DimImg &childId) {
	  tmpAverage += isLeaf ? pixels[childId] : (CompAttribute<double>::values [childId] * areas[childId]);
	});
      CompAttribute<double>::values [parentId] = tmpAverage/areas[parentId];
    });
  TreeStats::global.addTime (averageStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}

// ================================================================================
