#include <boost/chrono.hpp>
using namespace boost::chrono;

#include "triskeleBase.hpp"
#include "Attributes/WeightAttributes.hpp"
using namespace obelix;
using namespace obelix::triskele;
#include "WeightAttributes.tcpp"

// ================================================================================
template<typename WeightT>
WeightAttributes<WeightT>::WeightAttributes (const Tree &tree, const bool &decr)
  : CompAttribute<WeightT> (tree, decr) {
  DEF_LOG ("WeightAttributes::WeightAttributes", "");
}

template<typename WeightT>
WeightAttributes<WeightT>::~WeightAttributes () {
  DEF_LOG ("WeightAttributes::~WeightAttributes", "");
}

// ================================================================================
template<typename WeightT>
void
WeightAttributes<WeightT>::resetDecr (bool decr) {
  DEF_LOG ("WeightAttributes::resetDecr", "decr: " << decr);
  CompAttribute<WeightT>::decr = decr;
  CompAttribute<WeightT>::updateTranscient ();
}

// ================================================================================
template<typename WeightT>
void
WeightAttributes<WeightT>::setWeightBounds (Tree &tree) {
  DEF_LOG ("WeightAttributes::setWeightBounds", "");
  DimImg rootId = tree.getCompRoot ();

  DimImg stepsCard = 0;
  WeightT curLevel = CompAttribute<WeightT>::values [0];
  for (DimImg compId = 1; compId <= rootId; ++compId) {
    if (CompAttribute<WeightT>::values [compId] == curLevel)
      continue;
    ++stepsCard;
    curLevel = CompAttribute<WeightT>::values [compId];
  }
  vector<DimImg> &weightBounds (tree.getWeightBounds ());
  weightBounds.clear ();
  weightBounds.reserve (stepsCard+1);
  weightBounds.push_back (0);
  curLevel = CompAttribute<WeightT>::values [0];
  for (DimImg compId = 1; compId <= rootId; ++compId) {
    if (CompAttribute<WeightT>::values [compId] == curLevel)
      continue;
    weightBounds.push_back (compId);
    curLevel = CompAttribute<WeightT>::values [compId];
  }
  weightBounds.push_back (rootId+1);
}


// ================================================================================
template<typename WeightT>
template<typename PixelT>
void
WeightAttributes<WeightT>::cut (vector<vector<PixelT> > &allBands, const AttributeProfiles<PixelT> &attributeProfiles,
		     const vector<WeightT> &thresholds) const {
  DEF_LOG ("WeightAttributes::cut", "");
  auto start = high_resolution_clock::now ();
  vector<WeightT> orderedThresholds (thresholds);
  if (CompAttribute<WeightT>::decr)
    reverse (orderedThresholds.begin (), orderedThresholds.end ());
  CompAttribute<WeightT>::cut (allBands, attributeProfiles, 0., orderedThresholds);
  TreeStats::global.addTime (filteringStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}

// ================================================================================
