#include <boost/chrono.hpp>
using namespace boost::chrono;

#include "Attributes/BoundingBoxAttributes.hpp"
using namespace obelix::triskele;


// ================================================================================
BoundingBoxAttributes::BoundingBoxAttributes (const Tree &tree)
  : CompAttribute<BoundingBox> (tree) {
  DEF_LOG ("BoundingBoxAttributes::BoundingBoxAttributes", "");
  compute ();
}

BoundingBoxAttributes::~BoundingBoxAttributes () {
  DEF_LOG ("BoundingBoxAttributes::~BoundingBoxAttributes", "");
}

// ================================================================================
void
BoundingBoxAttributes::compute () {
  DEF_LOG ("BoundingBoxAttributes::compute", "");
  auto start = high_resolution_clock::now ();
  DimImg const width = CompAttribute<BoundingBox>::tree.getSize ().width;
  DimImg const height = CompAttribute<BoundingBox>::tree.getSize ().height;
  computeSameCompLevel ([this, &width, &height] (const DimImg &parentId) {
      BoundingBox &bb (CompAttribute<BoundingBox>::values[parentId]);
      tree.forEachChildTI (parentId, [this, &bb, &width, &height, &parentId] (const bool &isLeaf, const DimImg &childId) {
	  if (isLeaf) {
	    DimSideImg x = childId % width;
	    DimImg yz = childId / width;
	    DimSideImg y = yz % height;
	    DimChanel z = yz / height;
	    bb.minX = min (bb.minX, x);
	    bb.minY = min (bb.minY, y);
	    bb.minZ = min (bb.minZ, z);
	    bb.maxX = max (bb.maxX, x);
	    bb.maxY = max (bb.maxY, y);
	    bb.maxZ = max (bb.maxZ, z);
	    return;
	  }
	  BoundingBox &bbc (CompAttribute<BoundingBox>::values[childId]);
	  bb.minX = min (bb.minX, bbc.minX);
	  bb.minY = min (bb.minY, bbc.minY);
	  bb.minZ = min (bb.minZ, bbc.minZ);
	  bb.maxX = max (bb.maxX, bbc.maxX);
	  bb.maxY = max (bb.maxY, bbc.maxY);
	  bb.maxZ = max (bb.maxZ, bbc.maxZ);
	});
    });
  TreeStats::global.addTime (bbStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}

// ================================================================================
