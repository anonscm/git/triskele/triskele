#include <iostream>
#include <string>
#include <vector>
#include <algorithm>    // std::sort

#include "obelixDebug.hpp"
#include "triskeleBase.hpp"
#include "Border.hpp"
#include "Tree.hpp"
#include "TreeStats.hpp"
#include "TreeBuilder.hpp"
#include "IImage.hpp"
#include "AttributeProfiles.hpp"
#include "ArrayTree/triskeleArrayTreeBase.hpp"
#include "ArrayTree/triskeleSort.hpp"
#include "ArrayTree/GraphWalker.hpp"
#include "ArrayTree/Leader.hpp"
#include "ArrayTree/Weight.hpp"
#include "ArrayTree/ArrayTreeBuilder.hpp"
#include "Attributes/WeightAttributes.hpp"

using namespace obelix;
using namespace obelix::triskele;

typedef uint16_t PixelT;
typedef uint16_t WeightT;

// ========================================
void
perf (const Raster<PixelT> &raster, const GraphWalker &graphWalker, const TreeType &treeType, const unsigned int &coreCount) {
  ArrayTreeBuilder<PixelT, WeightT> atb (raster, graphWalker, treeType);
  Tree tree (coreCount);
  WeightAttributes<PixelT> weightAttributes (tree, getDecrFromTreetype (treeType));
  atb.buildTree (tree, weightAttributes);
  tree.check (graphWalker.border);
}

// ========================================
int
main (int argc, char **argv, char **envp) {
  if (argc != 5) {
    cerr << "Usage: " << argv[0] << ": {MIN|MAX|TOS|ALPHA} coreCount nbTest imageSize" << endl;
    exit (1);
  }
  string argType (argv[1]);
  TreeType treeType = MIN;
  if (argType == "MIN")
    treeType = MIN;
  else if (argType == "MAX")
    treeType = MAX;
  else if (argType == "TOS")
    treeType= TOS;
  else if (argType == "ALPHA")
    treeType = ALPHA;
  else {
    cerr << "unknown type: " << argType << endl;
    exit (1);
  }
  const DimCore maxCoreCount (atol (argv[2])+1);
  const size_t nbTest (atol (argv[3]));
  const size_t maxImageSize (atol (argv[4]));

  const size_t forbiden = size_t (nbTest*.1);
  const size_t stepImg = max (size_t (1), size_t (maxImageSize*.1));

  srand (time (NULL));

  for (size_t card = stepImg; card < maxImageSize; card += stepImg) {
    const size_t h = size_t (sqrt (card*1.1)*2/30)*10;
    const size_t w = card/h;
    Size size (w, h);
    Border border (size, false);
    GraphWalker graphWalker (border);
    int leafCount = size.getPixelsCount ();
    Raster<PixelT> raster (size);

    cout << endl
	 << "card:" << card << "(" << w << "x" << h << "=" << (w*h) << ") argType:" << argType << " coreCount:" << maxCoreCount << " nbTest:" << nbTest << "(-" << (2*forbiden) << ")"<< endl
	 << "========================================" << flush;
    vector<vector<double>> times (maxCoreCount);
    for (unsigned int coreCount = 1; coreCount < maxCoreCount ; ++coreCount)
      times[coreCount].reserve (nbTest);

    for (unsigned int test = 0; test < nbTest ; ++test) {
      if (!(test%50))
	cout << endl;
      for (int p = 0; p < leafCount; ++p)
	raster.getPixels ()[p] = (PixelT) std::rand ();
      cout << "." << flush;
      for (unsigned int coreCount = 1; coreCount < maxCoreCount ; ++coreCount) {
	TreeStats::global.reset ();
	perf (raster, graphWalker, treeType, coreCount);
	times[coreCount].push_back (ba::mean (TreeStats::global.getTimeStats (buildTreeStats)));
      }
    }
    cout << endl;
    for (unsigned int coreCount = 1; coreCount < maxCoreCount ; ++coreCount) {
      vector<double> &tab (times[coreCount]);
      sort (tab.begin (), tab.end ());
      double average = accumulate (tab.begin ()+forbiden, tab.end ()-forbiden, .0) / nbTest;
      cout << "card:" << card << " treeType:" << argType << " coreCount:" << coreCount << " average: " << average << " throughput:" << size_t (card/average) << endl;
    }
  }
  return 0;
}

// ========================================
