#include <iostream>
#include <boost/filesystem.hpp>

#include "obelixDebug.hpp"
#include "triskeleBase.hpp"
#include "Border.hpp"
#include "Appli/OptGenerator.hpp"
#include "Tree.hpp"
#include "TreeStats.hpp"
#include "TreeBuilder.hpp"
#include "QuadTree/QuadTreeBuilder.hpp"
//#include "XMLTree/XMLTreeBuilder.hpp"
#include "IImage.hpp"

#include "ArrayTree/triskeleArrayTreeBase.hpp"
#include "ArrayTree/triskeleSort.hpp"
#include "ArrayTree/GraphWalker.hpp"
#include "ArrayTree/Leader.hpp"
#include "ArrayTree/Weight.hpp"

#include "ArrayTree/ArrayTreeBuilder.hpp"
#include "Attributes/WeightAttributes.hpp"
#include "AttributeProfiles.hpp"
#include "Attributes/AreaAttributes.hpp"
#include "Attributes/AverageAttributes.hpp"
#include "Attributes/SDAttributes.hpp"
#include "Attributes/XYAttributes.hpp"
#include "Attributes/MoIAttributes.hpp"

using namespace obelix;
using namespace obelix::triskele;

// ================================================================================
template<typename OutPixelT>
void
writeBand (OptGenerator &optGenerator, const GDALDataType &outDataType, OutPixelT *pixels, DimChanel band) {
  if (!optGenerator.oneBand) {
    // XXX bands
    optGenerator.outputImage.writeBand (pixels, band);
    return;
  }
  string outputBaseName  = boost::filesystem::path (optGenerator.outputImage.getFileName ()).replace_extension ("").string ();
  string outputExtension  = boost::filesystem::extension (optGenerator.outputImage.getFileName ());

  ostringstream fileNameStream;
  fileNameStream << outputBaseName << "-" << std::setfill ('0') << std::setw (3) << (band) << outputExtension;
  IImage outputImage (fileNameStream.str ());
  outputImage.createImage (Size (optGenerator.optCrop.size.width, optGenerator.optCrop.size.height), outDataType, 1, optGenerator.inputImage, optGenerator.optCrop.topLeft);

  outputImage.writeFrame (pixels, 0);
}


// ================================================================================
template<typename InPixelT, typename OutPixelT, typename APFunct>
void
apGenerator (OptGenerator &optGenerator, const GDALDataType &outDataType, const APFunct &setAP) {

  vector<TreeType> treeTypes;
  if (optGenerator.minTreeFlag)
    treeTypes.push_back (MIN);
  if (optGenerator.maxTreeFlag)
    treeTypes.push_back (MAX);
  if (optGenerator.tosTreeFlag)
    treeTypes.push_back (TOS);
  if (optGenerator.alphaTreeFlag)
    treeTypes.push_back (ALPHA);
  DimChanel treeTypesCard = treeTypes.size ();
  if (!treeTypesCard)
    cerr << "*** no tree type ! => copy mode" << endl;

  Border border (optGenerator.optCrop.size, optGenerator.border);
  GraphWalker graphWalker (border);
  DimImg leafCount = optGenerator.optCrop.size.getPixelsCount ();
  DimChanel maxThresholds = max (max (max (optGenerator.areaThresholds.size (), optGenerator.levelThresholds.size ()), optGenerator.sdThresholds.size ()), optGenerator.moiThresholds.size ());
  vector <vector <OutPixelT> > allBands (maxThresholds, vector<OutPixelT> (leafCount, 0));

  DimChanel outputBandsCard = optGenerator.selectedBand.getSet ().size ()*(1+treeTypesCard*(optGenerator.areaThresholds.size ()+optGenerator.levelThresholds.size ()+optGenerator.sdThresholds.size ()+optGenerator.moiThresholds.size ()));
  if (!optGenerator.oneBand) {
    // XXX dans frameCount dans size ?
    optGenerator.outputImage.createImage (optGenerator.optCrop.size, outDataType, outputBandsCard, optGenerator.inputImage, optGenerator.optCrop.topLeft);
  }

  Raster<InPixelT> raster;
  if (optGenerator.border) {
    DimChanel bandCount (optGenerator.inputImage.getBandCount ()); // -1); // XXX sans NDVI
    for (DimChanel band = 0; band < bandCount; ++band) {
      optGenerator.inputImage.readBand (raster, band, optGenerator.optCrop.topLeft, optGenerator.optCrop.size);
      for (DimImg idx = 0; idx < leafCount; ++idx)
	if (raster.getValue (idx))
	  border.clearBorder (idx);
    }
    //cerr << "XXX border: " << border.borderCount () << endl;
  }
  
  DimChanel chanel = 0;
  for (DimChanel band : optGenerator.selectedBand.getSet ()) {
    optGenerator.inputImage.readBand (raster, band, optGenerator.optCrop.topLeft, optGenerator.optCrop.size);
    writeBand (optGenerator, outDataType, raster.getPixels (), chanel++);

    for (TreeType treeType : treeTypes) {
      ArrayTreeBuilder<InPixelT, InPixelT> atb (raster, graphWalker, treeType, optGenerator.countingSortCeil);
      Tree tree (optGenerator.coreCount);
      WeightAttributes<InPixelT> weightAttributes (tree, getDecrFromTreetype (treeType));
      atb.buildTree (tree, weightAttributes);

      AttributeProfiles<OutPixelT> attributeProfiles (tree);
      AreaAttributes areaAttributes (tree);
      AverageAttributes averageAttributes (tree, raster, areaAttributes);

      setAP (tree, atb, attributeProfiles, raster, averageAttributes, areaAttributes);

      if (optGenerator.levelThresholds.size ()) {
	vector<InPixelT> thresholds (weightAttributes.getConvertedThresholds (optGenerator.levelThresholds));
	weightAttributes.cut (allBands, attributeProfiles, thresholds);
      	for (DimChanel c = 0; c < optGenerator.levelThresholds.size (); ++c, ++chanel)
      	  writeBand (optGenerator, outDataType, &allBands[c][0], chanel);
      }
      if (optGenerator.areaThresholds.size ()) {
	areaAttributes.cut (allBands, attributeProfiles, optGenerator.areaThresholds);
	for (DimChanel c = 0; c < optGenerator.areaThresholds.size (); ++c, ++chanel)
	  writeBand (optGenerator, outDataType, &allBands[c][0], chanel);
      }
      if (optGenerator.sdThresholds.size ()) {
	SDAttributes sdAttributes (tree, areaAttributes);
	sdAttributes.cut (allBands, attributeProfiles, optGenerator.sdThresholds);
	for (DimChanel c = 0; c < optGenerator.sdThresholds.size (); ++c, ++chanel)
	  writeBand (optGenerator, outDataType, &allBands[c][0], chanel);
      }
      if (optGenerator.moiThresholds.size ()) {
	XYAttributes xyAttributes (tree, areaAttributes);
	MoIAttributes moiAttributes (tree, areaAttributes, xyAttributes);
	moiAttributes.cut (allBands, attributeProfiles, optGenerator.moiThresholds);
	for (DimChanel c = 0; c < optGenerator.moiThresholds.size (); ++c, ++chanel)
	  writeBand (optGenerator, outDataType, &allBands[c][0], chanel);
      }
    }
  }

  cerr << endl << "*** apGenerator done!" << endl
       << TreeStats::global.printDim () << endl
       << TreeStats::global.printTime ();
}

// ================================================================================
template<typename InPixelT>
void
outTypeSelection (OptGenerator &optGenerator) {
  switch (optGenerator.featureType) {
  case FMean:
    apGenerator<InPixelT, InPixelT> (optGenerator, optGenerator.inputImage.getDataType (),
				     [] (Tree &tree, ArrayTreeBuilder<InPixelT, InPixelT> &atb, AttributeProfiles<InPixelT> &attributeProfiles, Raster<InPixelT> &raster, AverageAttributes &averageAttributes, AreaAttributes &areaAttributes) {
				       attributeProfiles.setValues (raster.getPixels (), averageAttributes.getValues ());
				     });
    break;
  case FArea:
    apGenerator<InPixelT, DimImg> (optGenerator, GDT_UInt32,
  				   [] (Tree &tree, ArrayTreeBuilder<InPixelT, InPixelT> &atb, AttributeProfiles<DimImg> &attributeProfiles, Raster<InPixelT> &raster, AverageAttributes &averageAttributes, AreaAttributes &areaAttributes) {
  				     attributeProfiles.setValues (1, areaAttributes.getValues ());
  				   });
    break;
  case FSD:
    apGenerator<InPixelT, double> (optGenerator, GDT_Float64,
  				   [] (Tree &tree, ArrayTreeBuilder<InPixelT, InPixelT> &atb, AttributeProfiles<double> &attributeProfiles, Raster<InPixelT> &raster, AverageAttributes &averageAttributes, AreaAttributes &areaAttributes) {
				     SDAttributes sdAttributes (tree, areaAttributes);
				     attributeProfiles.setValues (0., sdAttributes.getValues ());
  				   });
    break;
  case FMoI:
    apGenerator<InPixelT, double> (optGenerator, GDT_Float64,
  				   [] (Tree &tree, ArrayTreeBuilder<InPixelT, InPixelT> &atb, AttributeProfiles<double> &attributeProfiles, Raster<InPixelT> &raster, AverageAttributes &averageAttributes, AreaAttributes &areaAttributes) {
				     XYAttributes xyAttributes (tree, areaAttributes);
				     MoIAttributes moiAttributes (tree, areaAttributes, xyAttributes);
				     attributeProfiles.setValues (0., moiAttributes.getValues ());
  				   });
    break;
  default:
    apGenerator<InPixelT, InPixelT> (optGenerator, optGenerator.inputImage.getDataType (),
				     [] (Tree &tree, ArrayTreeBuilder<InPixelT, InPixelT> &atb, AttributeProfiles<InPixelT> &attributeProfiles, Raster<InPixelT> &raster, AverageAttributes &averageAttributes, AreaAttributes &areaAttributes) {
				       atb.setAttributProfiles (attributeProfiles);
				     });
  }
}

// ================================================================================
int
main (int argc, char** argv, char** envp) {
  OptGenerator optGenerator (argc, argv);
  DEF_LOG ("main", "");

  switch (optGenerator.inputImage.getDataType ()) {
  case GDT_Byte:
    outTypeSelection<uint8_t> (optGenerator); break;
  case GDT_UInt16:
    outTypeSelection<uint16_t> (optGenerator); break;
  case GDT_Int16:
    outTypeSelection<int16_t> (optGenerator); break;
  case GDT_UInt32:
    outTypeSelection<uint32_t> (optGenerator); break;
  case GDT_Int32:
    outTypeSelection<int32_t> (optGenerator); break;
  case GDT_Float32:
    outTypeSelection<float> (optGenerator); break;
  case GDT_Float64:
    outTypeSelection<double> (optGenerator); break;

  default :
    cerr << "unknown type!" << endl; break;
    return 1;
  }
  return 0;
}

// ================================================================================
