#include "triskeleBase.hpp"
#include "Tree.hpp"
#include "TreeBuilder.hpp"

using namespace obelix;
using namespace obelix::triskele;

// ================================================================================
void
TreeBuilder::buildTree (Tree &tree, TreeBuilder &builder) {
  builder.buildTree (tree);
}

void
TreeBuilder::buildTree (Tree &tree) {
  std::cout << "Test" << std::endl;
}

// ================================================================================
TreeBuilder::TreeBuilder ()
  : leafCount (0),
    nodeCount (0),
    leafParents (nullptr),
    compParents (nullptr),
    children (nullptr),
    childrenStart (nullptr) {
}

TreeBuilder::~TreeBuilder () {
}

// ================================================================================
void
TreeBuilder::setTreeSize (Tree &tree, const Size &size) {
  tree.resize (size);
  updateTranscient (tree);
}

void
TreeBuilder::updateTranscient (Tree &tree) {
  leafCount     = tree.leafCount;
  nodeCount     = tree.nodeCount;
  leafParents   = &tree.leafParents[0];
  compParents   = tree.compParents;
  children      = &tree.children[0];
  childrenStart	= &tree.childrenStart[0];
}

void
TreeBuilder::setNodeCount (Tree &tree, DimNodeId nodeCount) {
  tree.setNodeCount (nodeCount);
  this->nodeCount = nodeCount;
}

// ================================================================================
