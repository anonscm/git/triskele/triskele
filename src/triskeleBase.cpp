#include "triskeleBase.hpp"

using namespace obelix;

// ================================================================================
int
obelix::bitCount [] = {
  0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
  1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
  1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
  2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
  1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
  2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
  2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
  3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 4, 5, 5, 6, 5, 6, 6, 7, 5, 6, 6, 7, 6, 7, 7, 8
};

// ================================================================================
const Point obelix::NullPoint = Point ();
const Size obelix::NullSize = Size ();
const Rect obelix::NullRect = Rect ();

// ================================================================================
Point::Point ()
  : x (0),
    y (0),
    z (0) {
}

Point::Point (const DimSideImg x, const DimSideImg y, const DimChanel z)
  : x (x),
    y (y),
    z (z) {
    }

Point::Point (const Size &size, const DimImg &idx)
  : x (idx % size.width),
    y (idx / size.width),
    z (y / size.height) {
  y %= size.height;
}

ostream &
obelix::operator << (ostream &out, const Point &p) {
  return out << "(" << p.x << "," << p.y << "," << p.z << ")";
}

// ================================================================================
Size::Size ()
  : width (0),
    height (0),
    length (0) {
}

Size::Size (const DimSideImg w, const DimSideImg h, const DimChanel l)
  : width (w),
    height (h),
    length (l) {
}

DimSideImg
Size::getPixelsCount () const {
  return DimImg (width)*DimImg(height)*DimImg(length);
}

ostream &
obelix::operator << (ostream &out, const Size &s) {
  return out << "[" << s.width << "," << s.height << "," << s.length << "]";
}

// ================================================================================
Rect::Rect ()
  : point (),
    size () {
}

Rect::Rect (const Point &orig, const Size &size)
  : point (orig),
    size (size) {
    }


ostream &
obelix::operator << (ostream &out, const Rect &r) {
  return out << "[" << r.point.x << "," << r.point.y << "," << r.point.z << " / " << r.size.width << "x" << r.size.height << "x" << r.size.length << "]";
}

// ================================================================================
