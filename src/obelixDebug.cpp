#include <iomanip>
#include <sstream>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/chrono.hpp>

#include "obelixDebug.hpp"


using namespace std;
using namespace obelix;

bool
Log::debug = false;

size_t
Log::indent = 0;

// ================================================================================
string
Log::getLocalTimeStr () {
  using namespace boost::posix_time;
  using namespace std;
  ptime now = second_clock::second_clock::local_time ();
  stringstream ss;
  auto date = now.date ();
  auto time = now.time_of_day ();
  ss << setfill ('0') << "["
     << setw (2) << static_cast<int> (date.month ()) << "/" << setw (2) << date.day ()
     << "] " << setw (2)
     << time.hours () << ":" << setw (2) << time.minutes ();
  return ss.str();
}

Log::Log (const string &functName)
  : functName (functName) {
  ++indent;
  if (debug)
    cerr << *this << "> ";
}

Log::~Log () {
  if (debug)
    cerr << *this << "<" << endl << flush;
  --indent;
}

ostream &
obelix::operator << (ostream &out, const Log &log) {
  return out << Log::getLocalTimeStr () << setw (3) << setw ((log.indent % 20)*2) << "" << log.functName;
}

// ================================================================================
