#include <stdexcept>
#include <boost/lexical_cast.hpp>

#include "Appli/OptRate.hpp"

using namespace obelix;
using namespace std;

// ================================================================================
bool
OptRate::isPercent () const {
  return percent;
}

double
OptRate::getValue () const {
  return rate;
}

string
OptRate::getRate () const {
  return initOptRate;
}

OptRate::OptRate (const string &option)
  : initOptRate (""),
    percent (false),
    rate (0.) {
  init (option);
}

// ================================================================================
void
OptRate::init (const string &option) {
  try {
    char* pEnd;
    rate = strtod (option.c_str (), &pEnd);
    percent = *pEnd == '%';
    if (percent)
      ++pEnd;
    if (*pEnd != '\0')
      throw invalid_argument ("Bad rate");
    initOptRate = boost::lexical_cast<string> (rate);
    if (percent) {
      rate /= 100.;
      initOptRate += "%";
    }
  } catch (...) {
    throw invalid_argument ("Bad rate option: "+option);
  }
}

// ================================================================================
void
OptRate::check (const string &optionName, const bool &less100percent) const {
  if (rate < 0)
    throw invalid_argument (optionName+" must be > 0 ("+boost::lexical_cast<std::string> (rate)+")");
  if (less100percent && percent && rate > 1)
    throw invalid_argument (optionName+" must be < 100% ("+boost::lexical_cast<std::string> (rate)+")");
}

// ================================================================================
ostream &
obelix::operator << (ostream &out, const OptRate &optRate) {
  return out << optRate.getRate ();
}

istream &
obelix::operator >> (istream &in, OptRate &optRate) {
  string token;
  in >> token;
  try {
    optRate.init (token);
  } catch (...) {
    in.setstate (ios_base::failbit);
  }
  return in;
}

// ================================================================================
