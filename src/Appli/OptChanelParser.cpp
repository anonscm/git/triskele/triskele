#include <stdexcept>
#include <map>
#include <boost/program_options.hpp>

#include "Appli/OptRanges.hpp"
#include "Appli/OptChanelParser.hpp"
#include "misc.hpp"

using namespace std;
using namespace boost;
using namespace boost::program_options;

using namespace obelix;
using namespace obelix::triskele;

namespace po = boost::program_options;

// ================================================================================
const map<string, OptionNames>
OptChanelParser::OptionNamesLabels { {
    "ndviBands", ONNdviBands }, {
    "sobelBands", ONSobelBands }, {
    "copyBands", ONCopyBands }, {
    "withBands", ONWithBands }, {
    "featureType", ONFeatureType }, {
    "attributeType", ONAttributeType }, {
    "treeType", ONTreeType }, {
    "thresholds", ONThresholds }, {
    "dapWeight", ONDapWeight }, {
    "dapPos", ONDapPos }, {
    "haarSizes", ONHaarSizes }, {
    "statSizes", ONStatSizes }
};

OptionNames
OptChanelParser::resolveOption (const string &input) {
  auto itr = OptionNamesLabels.find (input);
  if (itr != OptionNamesLabels.end ())
    return itr->second;
  return ONInvalidOption; 
}

int
OptChanelParser::getOptRank (const string &optName) {
  int val = 0;
  auto it = optRank.find (optName);
  if (it != optRank.end ())
    val = it->second+1;
  optRank[optName] = val;
  return val;
}

// ================================================================================
class MultiSwitch {
public:
  MultiSwitch () {}
};
void
validate (boost::any &, vector<string> const &, MultiSwitch*, long) {
}

// ================================================================================
OptChanelParser::OptChanelParser ()
  : noMixedBand (false),
    noNdviFlag (false),
    noSobelFlag (false),
    noCopyFlag (false),
    noHaarFlag (false),
    noStatFlag (false),
    showChanel (false),
    dapWeightFlags (false),
    spectralDepth (0),
    mixedBandFlag (false),
    description ("Chanel config options", getCols ()) {
  DEF_LOG ("OptChanelParser::OptChanelParser", "");

  description.add_options ()
    ("cfgName", po::value<string> (&cfgName), "config name")

    ("spectralDepth,d", po::value<DimChanel> (&spectralDepth)->default_value (spectralDepth), "spectral band count (0 all bands are spectral = no volume)")
    ("mixedBandFlag", po::bool_switch (&mixedBandFlag)->default_value (mixedBandFlag), "if bands are mixed in frames")

    ("noMixedBand", bool_switch (&noMixedBand), "remove mixed band flag")
    ("noNdvi", bool_switch (&noNdviFlag), "remove ndvi from config")
    ("noSobel", bool_switch (&noSobelFlag), "remove sobel from config")
    ("noCopy", bool_switch (&noCopyFlag), "remove copy from config")
    ("noHaar", bool_switch (&noHaarFlag), "remove haar from config")
    ("noStat", bool_switch (&noStatFlag), "remove stat from config")

    ("ndviBands,n", po::value<vector <OptRanges> > (&ndviBands)->multitoken (), "band1,band2 : produce ndvi band (first band is 0)")
    ("sobelBands,s", po::value<OptRanges> (&sobelBands)->default_value (sobelBands), "band,... : produce sobel band (first band is 0)")
    ("copyBands,c", po::value<OptRanges> (&copyBands)->multitoken (), "copy input band (included ndvi and sobel)")
    ("withBands,b", po::value<vector <OptRanges> > (&selectedBands)->multitoken (), "with input bands do...")

    ("featureType,f", po::value<vector <FeatureType> > (&featureTypes), "feature profiles produced (AP (default attribute profiles), Area, Mean, SD, MoI)")

    ("treeType,t", po::value<vector <TreeType> > (&treeTypes), "select tree type (Min, Max, ToS, Alpha)")
    ("attributeType,a", po::value<vector <AttributeType> > (&attributeType)->multitoken (), "select attribute type (Area, Weight, SD, MoI)")
    ("thresholds", po::value<vector <string> > (&thresholdsValues)->multitoken (), "t1,t2,... : thresholds")

    ("dapWeight", po::value<MultiSwitch>()->zero_tokens (), "use origin weight for DAP")
    ("dapPos", po::value<vector <ApOrigPos> > (&apOrigPos), "position of origin on DAP (apOrigNoPos, apOrigPosBegin, apOrigPosEnd, apOrigPosBoth, apOrigPosEverywhere, apOrigPosBeginMin, apOrigPosBeginMax)")

    ("haarSizes,H", po::value<vector <OptSizes> > (&haarSizes)->multitoken (), "size,... : produce haar for window sizes (size : HxW)")
    ("statSizes,T", po::value<vector <OptSizes> > (&statSizes)->multitoken (), "size,... : produce stat for window sizes (size : HxW)")

    ("loadChanel", po::value<string> (&loadName), "XML file name (config base)")
    ("saveChanel", po::value<string> (&saveName), "XML file name")

    ("showChanel", bool_switch (&showChanel), "Show output chanels")
    ;
}

// ================================================================================
void
OptChanelParser::parse (OptChanel &optChanel, IImage &inputImage, po::basic_parsed_options<char> &parsed) {
  DEF_LOG ("OptChanelParser::parse", "");
  if (!loadName.empty ()) {
    loadName = addExtensionIfNone (loadName, OptChanel::chExt);
    optChanel.loadFile (loadName);
  }

  if (mixedBandFlag && noMixedBand)
    throw invalid_argument ("Can't used mixedBandFlag and noMixedBand in same time");
  if (noMixedBand)
    mixedBandFlag = optChanel.mixedBandFlag = false;
  if (mixedBandFlag)
    optChanel.mixedBandFlag = true;
  optChanel.spectralDepth = spectralDepth;

  // if (inputImage.isEmpty () && optChanel.isEmpty ())
  //   usage ("No valid input image");
  inputImage.readImage (spectralDepth, mixedBandFlag);
  if (!inputImage.isEmpty ()) {
    GDALDataType inputType = inputImage.getDataType ();
    int inputBandsCount = inputImage.getBandCount ();
    if (optChanel.isEmpty ()) {
      LOG ("setType");
      optChanel.setType (inputType, inputBandsCount);
    } else if (!optChanel.inputMatches (inputType, inputBandsCount))
      throw invalid_argument ("Can't parse unknown or inconsistant bands");
  }
  if (!cfgName.empty ())
    optChanel.setName (cfgName);

  if (noNdviFlag)
    optChanel.remove (Ndvi);
  if (noSobelFlag)
    optChanel.remove (Sobel);
  if (noCopyFlag)
    optChanel.removeCopy ();
  if (noHaarFlag)
    optChanel.remove (Haar);
  if (noStatFlag)
    optChanel.remove (Stat);

  for (auto ndvi : ndviBands) {
    if (ndvi.size () != 2)
      throw invalid_argument ("ndvi must define by 2 chanels");
    optChanel.addNdvi (ndvi.first (), ndvi.last ());
  }

  sobelBands.setLimits (0, optChanel.getSize (Original)+optChanel.getSize (Ndvi)-1);
  sobelBands.toSet ();
  for (auto sobel : sobelBands.getSet ())
    optChanel.addSobel (sobel);

  int maxInput = optChanel.getSize (Original)+optChanel.getSize (Ndvi)+optChanel.getSize (Sobel)-1;
  copyBands.setLimits (0, maxInput);
  copyBands.toSet ();
  for (auto copy : copyBands.getSet ())
    optChanel.addCopy (copy);

  bool weightFlag (false);
  for (auto &x : parsed.options) {
    string optName = x.string_key;
    int optRank = getOptRank (optName);
    LOG ("optName: " << optName << " optRank: " << optRank << " switch: " << resolveOption (optName));
    switch (resolveOption (optName)) {
    case ONWithBands:
      selectedBands[optRank].setLimits (0, maxInput);
      selectedBands[optRank].toSet ();
      optChanel.selectInputs (selectedBands [optRank].getSet ());
      break;
    case ONFeatureType:
      optChanel.selectFeatureType (featureTypes [optRank]);
      break;
    case ONTreeType:
      optChanel.selectTreeType (treeTypes [optRank]);
      break;
    case ONAttributeType:
      optChanel.selectAttributeType (attributeType [optRank]);
      break;
    case ONThresholds:
      optChanel.addThresholds (thresholdsValues [optRank]);
      break;

    case ONDapWeight:
      weightFlag = true;
      break;
    case ONDapPos:
      optChanel.addDap (weightFlag, apOrigPos [optRank]);
      weightFlag = false;
      break;

    case ONHaarSizes:
      optChanel.addHaar (haarSizes [optRank].getSizes ());
      break;
    case ONStatSizes:
      optChanel.addStat (statSizes [optRank].getSizes ());
      break;
    default:
      ; //std::cout << "This OptionName value is not handled (" << optName << ")" << endl; //exit(1);
    }
  }

  optChanel.updateChanels ();
  if (!saveName.empty ()) {
    saveName = addExtensionIfNone (saveName, OptChanel::chExt);
    optChanel.saveFile (saveName);
  }
  if (showChanel)
    optChanel.printOutput (cout);
}

// ================================================================================
