
#include "Appli/OptCrop.hpp"

using namespace obelix;

// ================================================================================
OptCrop::OptCrop ()
  : topLeft (NullPoint),
    size (NullSize) {
}

ostream &
obelix::operator << (ostream &out, const OptCrop &optCrop) {
  return out << optCrop.topLeft << " / " << optCrop.size;
}


// ================================================================================
