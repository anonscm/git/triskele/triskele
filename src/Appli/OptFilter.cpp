#define LAST_VERSION "2.2 2019-03-11 ("+getOsName ()+")"

#include <iostream>
#include <string.h>

#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/filesystem.hpp>
#include <boost/thread.hpp>

#include "obelixDebug.hpp"
#include "triskeleBase.hpp"
#include "misc.hpp"
#include "IImage.hpp"
#include "Appli/OptFilter.hpp"
#include "Appli/OptChanelParser.hpp"
#include "Appli/OptCropParser.hpp"

// #ifndef TRISKELE_ERROR
// #define TRISKELE_ERROR(expr) std::cerr << expr << std::endl << std::flush, std::exit (1)
// #endif

using namespace std;
using namespace boost;
using namespace boost::program_options;
using namespace boost::filesystem;

using namespace obelix;
using namespace obelix::triskele;

namespace po = boost::program_options;

// ================================================================================
OptFilter::OptFilter ()
  : debugFlag (false),
    timeFlag (false),
    countingSortCeil (2),
    oneBand (false),
    includeNoDataFlag (false),
    connectivity (Connectivity (Connectivity::C4|Connectivity::CT)),
    coreCount (boost::thread::hardware_concurrency ()) {
}

OptFilter::OptFilter (int argc, char** argv)
  : OptFilter () {
  parse (argc, argv);
}

// ================================================================================
OptCropParser optCropParser;
static OptChanelParser optChanelParser;
static po::options_description mainDescription ("Main options", getCols ());
static po::options_description hide ("Hidded options", getCols ());
static char *prog = NULL;

// ================================================================================
void
OptFilter::usage (string msg, bool hidden) const {
  if (!msg.empty ())
    cout << msg << endl;
  cout << endl
       <<"Usage: " << endl
       <<"       " << prog << " [options] [-i] inputFileName [-o] outputFileName" << endl << endl
       << endl << mainDescription
       << endl << optCropParser.description
       << endl << optChanelParser.description
       << endl;
  if (hidden)
    cout << hide << endl;
  exit (1);
}

void
OptFilter::version () const {
  cout << endl << prog << " version " << LAST_VERSION << endl << endl
       << "  GDAL  : read and write image (http://www.gdal.org/)" << endl
       << "  cct   : inspired by jirihavel library to build trees (https://github.com/jirihavel/libcct)" << endl
       << "  Boost : C++ libraries (http://www.boost.org/)" << endl
       << endl << "  This software is a Obelix team production (http://www-obelix.irisa.fr/)" << endl << endl;
  exit (0);
}
// ================================================================================
static const string inputFile = "input-file";
static const char *const inputFileC = inputFile.c_str ();

void
OptFilter::parse (int argc, char** argv) {
  prog = argv [0];
  bool helpFlag = false, versionFlag = false, useTheForceLuke = false;
  string inputFileName, outputFileName;
  string areaThresholdsName, levelThresholdsName, sdThresholdsName, moiThresholdsName;
  try {
    mainDescription.add_options ()
      ("help", po::bool_switch (&helpFlag), "produce this help message")
      ("version", po::bool_switch (&versionFlag), "display version information")
      ("debug", po::bool_switch (&debugFlag), "debug mode")

      ("timeFlag", po::bool_switch (&timeFlag), "give execution time")
      ("countingSortCeil", po::value<DimSortCeil> (&countingSortCeil)->default_value (countingSortCeil), "force counting sort under ceil (n.b. value in [0..2^16] => 300MB!)")
      ("showConfig", po::value<string> (&showConfig), "show actual configuration on a file")

      ("includeNoDataFlag", po::bool_switch (&includeNoDataFlag), "build tree with all pixels (included no-data)")

      ("input,i", po::value<string> (&inputFileName), "input file name image")
      ("output,o", po::value<string> (&outputFileName), "output file name for hyperbands image contains attributs profiles (or classification image with -g options)")
      ("connectivity", po::value<Connectivity> (&connectivity)->default_value (connectivity), "connectivity")
      ("coreCount", po::value<DimCore> (&coreCount)->default_value (coreCount), "thread used to build tree")
      ;
    hide.add_options ()
      ("useTheForceLuke", po::bool_switch (&useTheForceLuke), "display hidded options")
      ("oneBand", po::bool_switch (&oneBand), "split all bands, one band per image")
      ;

    po::options_description cmd ("All options");
    cmd.add (mainDescription).add (optCropParser.description).add (optChanelParser.description).add (hide).add_options ()
      (inputFileC, po::value<vector<string> > (), "input output")
      ;

    po::positional_options_description p;
    p.add (inputFileC, -1);
    po::variables_map vm;
    po::basic_parsed_options<char> parsed = po::command_line_parser (argc, argv).options (cmd).positional (p).run ();
    store (parsed, vm);
    po::notify (vm);

    if (debugFlag) {
#ifndef ENABLE_LOG
      cout << "No debug option available (was compiled with -DENABLE_LOG)" << endl;
#endif
    }
    Log::debug = debugFlag;
    if (useTheForceLuke)
      usage ("", true);
    if (versionFlag)
      version ();
    if (helpFlag)
      usage ();

    int required = 2;
    if (vm.count ("input"))
      required--;
    if (vm.count ("output"))
      required--;

    int nbArgs = 0;
    if (vm.count (inputFileC))
      nbArgs = vm[inputFileC].as<vector<string> > ().size ();
    if (required-nbArgs != 0)
      usage ("Need one input and one output");
    if (required) {
      vector<string> var = vm[inputFileC].as<vector<string> > ();
      if (outputFileName.empty ())
	outputFileName = var [--required];
      if (inputFileName.empty ())
	inputFileName = var [--required];
    }

    inputImage.setFileName (inputFileName);
    outputImage.setFileName (outputFileName);
    optChanelParser.parse (optChanel, inputImage, parsed);
    optCropParser.parse (optCrop, inputImage, parsed);

    Size imgSize = inputImage.getSize ();
    GDALDataType inputType = inputImage.getDataType ();

    cout
      << "Input:" << inputFileName << " (chanels of " << GDALGetDataTypeName (inputType) << endl
      << "Crop:" << optCrop << endl
      << "Output:" << outputFileName << endl
      << "core count:" << coreCount << endl;

    if (optCrop.topLeft.x || optCrop.topLeft.y ||
	optCrop.size.width != imgSize.width || optCrop.size.height != imgSize.height)
      cout << Log::getLocalTimeStr () << " crop:" << optCrop << endl;

  if (!optChanel.getOutputCount ())
    throw invalid_argument ("No output band selected");

  } catch (std::exception& e) {
    cerr << "error: " << e.what () << endl;
    usage ("Bad options");
  }

  if (!showConfig.empty ()) {
    out.open (showConfig, ios_base::out);
    out
      << endl
      << Log::getLocalTimeStr () << " ChanelFilter " << LAST_VERSION << " config:" << endl << endl
      << *this << endl;
  }
}

// ================================================================================
ostream &
obelix::triskele::operator << (ostream &out, const OptFilter &optFilter) {
  unsigned int leftMarging = 25;
  return out
    SHOW_OPTION_FLAG ("--debugFlag", optFilter.debugFlag)
    SHOW_OPTION_FLAG ("--timeFlag", optFilter.timeFlag)
    SHOW_OPTION ("--countingSortCeil", optFilter.countingSortCeil)
    SHOW_OPTION ("--showConfig", optFilter.showConfig)
    SHOW_OPTION_FLAG ("--oneBand", optFilter.oneBand)
    SHOW_OPTION_FLAG ("--includeNoDataFlag", optFilter.includeNoDataFlag)
    SHOW_OPTION ("--connectivity", optFilter.connectivity << " (" << int (optFilter.connectivity) << ")")
    SHOW_OPTION ("--coreCount", optFilter.coreCount)

    SHOW_OPTION ("--input", optFilter.inputImage)
    SHOW_OPTION ("--output", optFilter.outputImage)

    << endl
    << "OptCrop: " << endl << optFilter.optCrop << endl
    << endl
    << "OptChanel: " << endl << optFilter.optChanel << endl
    << flush;
}

// ================================================================================
