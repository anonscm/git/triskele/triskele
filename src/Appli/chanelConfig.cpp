#include <iostream>
#include <iterator>

#include <boost/program_options.hpp>

using namespace std;
using namespace boost;
using namespace boost::program_options;

#include "obelixDebug.hpp"
#include "IImage.hpp"

#include "misc.hpp"
#include "Appli/OptChanel.hpp"
#include "Appli/OptChanelParser.hpp"

using namespace obelix;
using namespace obelix::triskele;

namespace po = boost::program_options;

// ================================================================================
static OptChanelParser optChanelParser;
static po::options_description mainDescription ("Main options", getCols ());
static po::options_description hide ("Hidded options", getCols ());
static char *prog = NULL;

// ================================================================================
void
usage (const string &msg = "", const bool &hidden = false) {
  if (!msg.empty ())
    cout << msg << endl;
  cout << endl
       <<"Usage: " << endl
       <<"       " << prog << " [options] [inputImage]" << endl
       << endl <<"  create configuration for Triskele" << endl
       << endl << mainDescription
       << endl << optChanelParser.description
       << endl;
  exit (1);
  if (hidden)
    cout << hide << endl;
  exit (1);
}

// ================================================================================
static const string inputFile = "input-file";
static const char *const inputFileC = inputFile.c_str ();

// ================================================================================
int
main (int argc, char** argv) {
  // debug = true;
  prog = argv [0];
  try {

    bool helpFlag (false), debugFlag = false, useTheForceLuke = false;
    string inputName;

    mainDescription.add_options ()
      ("help", po::bool_switch (&helpFlag), "produce this help message")
      ("input,i", po::value<string> (&inputName), "input file name image (to determine the number bands)")
      ;

    hide.add_options ()
      ("useTheForceLuke", po::bool_switch (&useTheForceLuke), "display hidded options")
      ("debug", po::bool_switch (&debugFlag), "debug mode")
      ;

    po::options_description cmd ("All options");
    cmd.add (mainDescription).add (optChanelParser.description).add (hide).add_options ()
      (inputFileC, po::value<vector<string> > (), "input output")
      ;

    po::positional_options_description p;
    p.add (inputFileC, -1);
    po::variables_map vm;
    po::basic_parsed_options<char> parsed = po::command_line_parser (argc, argv).options (cmd).positional (p).run ();
    store (parsed, vm);
    po::notify (vm);

    if (debugFlag) {
#ifndef ENABLE_LOG
      cout << "No debug option available (was compiled with -DENABLE_LOG)" << endl;
#endif
    }
    Log::debug = debugFlag;
    if (useTheForceLuke)
      usage ("", true);
    if (helpFlag)
      usage ();

    if (vm.count (inputFileC)) {
      vector<string> var = vm[inputFileC].as<vector<string> > ();
      int nbArgs = vm[inputFileC].as<vector<string> > ().size ();
      int setArgs = 0;
      if (inputName.empty ())
	inputName = var [setArgs++];
      if (nbArgs > setArgs)
	usage ("Too much arguments");
    }

    OptChanel optChanel;
    IImage inputImage (inputName);
    optChanelParser.parse (optChanel, inputImage, parsed);

    if (debugFlag)
      cout << optChanel;

    return 0;
  } catch (std::exception& e) {
    cerr << "error: " << e.what() << endl;
    usage ();
    return 1;
  } catch (...) {
    cerr << "Exception of unknown type!" << endl;
    return 1;
  }
}

// ================================================================================
