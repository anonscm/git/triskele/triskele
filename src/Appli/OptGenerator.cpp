#define LAST_VERSION "2.2 2019-03-11 ("+getOsName ()+")"

#include <iostream>
#include <string.h>

#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/filesystem.hpp>
#include <boost/thread.hpp>

#include "obelixDebug.hpp"
#include "triskeleBase.hpp"
#include "misc.hpp"
#include "IImage.hpp"
#include "Appli/OptGenerator.hpp"
#include "Appli/OptCropParser.hpp"

#ifndef TRISKELE_ERROR
#define TRISKELE_ERROR(expr) std::cerr << expr << std::endl << std::flush, std::exit (1)
#endif

using namespace std;
using namespace boost::filesystem;

using namespace obelix;
using namespace obelix::triskele;

// ================================================================================
template<typename T>
vector<T>
readThresholds (string fileName) {
  vector<T> thresholds;
  if (fileName.empty ()) {
    return thresholds;
  }
  std::ifstream ifs;
  ifs.open (fileName);
  string line;
  while (getline (ifs, line)) {
    stringstream ss (line);
    T value;
    ss >> value;
    if (ss.fail ())
      continue;
    thresholds.push_back (value);
  }
  if (!thresholds.size ())
    cerr << "*** readThresholds: empty file: " << fileName << endl;
  sort (thresholds.begin (), thresholds.end ());
  return thresholds;
}

// ================================================================================
OptGenerator::OptGenerator ()
  : debugFlag (false),
    spectralDepth (0),
    mixedBandFlag (false),
    countingSortCeil (2),
    coreCount (boost::thread::hardware_concurrency ()),
    maxTreeFlag (false),
    minTreeFlag (false),
    tosTreeFlag (false),
    alphaTreeFlag (false),
    border (false),
    oneBand (false),
    connectivity (Connectivity (Connectivity::C4|Connectivity::CT)),
    featureType (FeatureType::FAP) {
    }

OptGenerator::OptGenerator (int argc, char** argv)
  : OptGenerator () {
  parse (argc, argv);
}

// ================================================================================
namespace po = boost::program_options;
OptCropParser optCropParser;
po::options_description mainDescription ("Main options", getCols ());
po::options_description hide ("Hidded options", getCols ());
char *prog = NULL;

// ================================================================================
void
OptGenerator::usage (string msg, bool hidden) {
  if (!msg.empty ())
    cout << msg << endl;
  cout << endl
       <<"Usage: " << endl
       <<"       " << prog << " [options] [-i] inputFileName [-o] outputFileName" << endl << endl
       << endl << mainDescription
       << endl << optCropParser.description
       << endl;
  if (hidden)
    cout << hide << endl;
  exit (1);
}

void
version () {
  cout << endl << prog << " version " << LAST_VERSION << endl << endl
       << "  GDAL  : read and write image (http://www.gdal.org/)" << endl
       << "  cct   : inspired by jirihavel library to build trees (https://github.com/jirihavel/libcct)" << endl
       << "  Boost : C++ libraries (http://www.boost.org/)" << endl
       << endl << "  This software is a Obelix team production (http://www-obelix.irisa.fr/)" << endl << endl;
  exit (0);
}

// ================================================================================
static const string inputFile = "input-file";
static const char *const inputFileC = inputFile.c_str ();

void
OptGenerator::parse (int argc, char** argv) {
  prog = argv [0];
  bool helpFlag = false, versionFlag = false, useTheForceLuke = false;
  string inputFileName, outputFileName;
  string areaThresholdsName, levelThresholdsName, sdThresholdsName, moiThresholdsName;
  try {
    mainDescription.add_options ()
      ("help", po::bool_switch (&helpFlag), "produce this help message")
      ("version", po::bool_switch (&versionFlag), "display version information")
      ("debug", po::bool_switch (&debugFlag), "debug mode")

      ("spectralDepth,d", po::value<DimChanel> (&spectralDepth)->default_value (spectralDepth), "spectral band count (0 all bands are spectral = no volume)")
      ("mixedBandFlag", po::bool_switch (&mixedBandFlag)->default_value (mixedBandFlag), "if bands are mixed in frames")

      ("band,b", po::value<OptRanges> (&selectedBand)->default_value (selectedBand), "select input band (first band is 0)")

      ("border", po::bool_switch (&border), "build tree without border pixels (no-data)")
      ("connectivity", po::value<Connectivity> (&connectivity)->default_value (connectivity), "connectivity")
      ("input,i", po::value<string> (&inputFileName), "input file name image")
      ("output,o", po::value<string> (&outputFileName), "output file name hyperbands image (contains attributs profiles)")

      ("maxTree", po::bool_switch (&maxTreeFlag), "build max-tree")
      ("minTree", po::bool_switch (&minTreeFlag), "build min-tree")
      ("tosTree", po::bool_switch (&tosTreeFlag), "build tree-of-shape")
      ("alphaTree", po::bool_switch (&alphaTreeFlag), "build alpha-tree")

      ("featureType", po::value<FeatureType> (&featureType)->default_value (FAP), "feature profiles produced (AP = attribut profiles)")

      ("area,A", po::value<string> (&areaThresholdsName), "cut according area attributs")
      ("weight,W", po::value<string> (&levelThresholdsName), "cut according level attributs")
      ("SD,S", po::value<string> (&sdThresholdsName), "cut according standard deviation attributs")
      ("MoI,M", po::value<string> (&moiThresholdsName), "cut according moment of inertia attributs")
      ;

    hide.add_options ()
      ("useTheForceLuke", po::bool_switch (&useTheForceLuke), "display hidded options")
      ("coreCount", po::value<DimCore> (&coreCount)->default_value (coreCount), "thread used to build tree")
      ("oneBand", po::bool_switch (&oneBand), "split all bands, one band per image")

      ("countingSortCeil", po::value<DimSortCeil> (&countingSortCeil)->default_value (countingSortCeil), "force counting sort under ceil (n.b. value in [0..2^16] => 300MB!)")

      // ("no-border", po::bool_switch (&optGenerator.noBorder), "build tree with all pixels (included no-data)")
      // ("dap", po::bool_switch (&optGenerator.dapFlag), "produce DAP rather than AP")
      // ("use-all-orig", po::bool_switch (&optGenerator.useAllOrigFlag), "force use all original band")
      // ("dap-no-orig", po::bool_switch (&optGenerator.dapNoOrigFlag), "force don't use original AP band")
      // ("dap-orig-pos", po::value<unsigned int> (&apOrigPos), (string ("position of origin on DAP (default ")+
      // 							      boost::lexical_cast<std::string> (apOrigPos)+
      // 							      " = "+apOrigPosName [apOrigPos]+")").c_str ())
      // ("dap-orig-weight", po::bool_switch (&optGenerator.dapOrigWeightFlag), "use origin weight for dap")
      ;

    po::options_description cmd ("All options");
    cmd.add (mainDescription).add (optCropParser.description).add (hide).add_options ()
      (inputFileC, po::value<vector<string> > (), "input thresholds output")
      ;

    po::positional_options_description p;
    p.add (inputFileC, -1);
    po::variables_map vm;
    po::basic_parsed_options<char> parsed = po::command_line_parser (argc, argv).options (cmd).positional (p).run ();
    store (parsed, vm);
    po::notify (vm);

    if (useTheForceLuke)
      usage ("", true);
    if (versionFlag)
      version ();
    if (helpFlag)
      usage ();

    int required = 2;
    if (vm.count ("input"))
      required--;
    if (vm.count ("output"))
      required--;

    int nbArgs = 0;
    if (vm.count (inputFileC))
      nbArgs = vm[inputFileC].as<vector<string> > ().size ();
    if (required-nbArgs != 0)
      usage ("Need one input and one output");
    if (required) {
      vector<string> var = vm[inputFileC].as<vector<string> > ();
      if (outputFileName.empty ())
	outputFileName = var [--required];
      if (inputFileName.empty ())
	inputFileName = var [--required];
    }

    if (debugFlag) {
#ifndef ENABLE_LOG
      cout << "No debug option available. You must compile without: -DENABLE_LOG" << endl;
#endif
      Log::debug = true;
    }

    areaThresholds = readThresholds<DimImg> (areaThresholdsName);
    levelThresholds = readThresholds<double> (levelThresholdsName);
    sdThresholds = readThresholds<double> (sdThresholdsName);
    moiThresholds = readThresholds<double> (moiThresholdsName);

    inputImage.setFileName (inputFileName);
    outputImage.setFileName (outputFileName);
    inputImage.readImage (spectralDepth, mixedBandFlag);

    GDALDataType inputType = inputImage.getDataType ();

    DimChanel bandInputCount = inputImage.getBandCount ();
    if (selectedBand.empty ())
      selectedBand = OptRanges (0, bandInputCount-1);
    selectedBand.setLimits (0, bandInputCount-1);
    selectedBand.toSet ();

    optCropParser.parse (optCrop, inputImage, parsed);

  cout
    << "Input:" << inputFileName << " (chanels of " << GDALGetDataTypeName (inputType) << endl
    << "Crop:" << optCrop << " band:" << selectedBand << endl
    << "Output:" << outputFileName << endl
    << "core count:" << coreCount << endl;

  if (!selectedBand.empty () &&
      (selectedBand.first () < 0 ||
       selectedBand.last () >= bandInputCount))
    TRISKELE_ERROR ("Band out of image (0 <= " << selectedBand.first () << " " << selectedBand.last () << " " << selectedBand << " < " << bandInputCount << ")");

  } catch (...) {
    usage ("Bad options");
  }
}

// ================================================================================
