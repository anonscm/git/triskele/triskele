#include <stdexcept>
#include <string>
#include "Appli/OptSizes.hpp"

using namespace std;
using namespace obelix;
using namespace boost;


// ================================================================================
OptSizes::OptSizes (const string &option) {
  reset ();
  init (option);
}

OptSizes::OptSizes (const OptSizes &optSizes)
  : OptSizes () {
  sizes = optSizes.sizes;
}

// ================================================================================
void
OptSizes::reset () {
  sizes.clear ();
}

void
OptSizes::init (const string &option) {
  reset ();
  if (option.empty ())
    return;
  vector <string> fields;
  vector <string> intervals;
  try {
    split (fields, option, is_any_of (","));
    for (auto &field : fields) {
      intervals.clear();
      split (intervals, field, is_any_of ("x"));
      switch (intervals.size ()) {
      case 0:
	continue;
      case 1:
	throw invalid_argument ("Bad range");
	continue;
      case 2:
	sizes.push_back (Size (stol ((string&) intervals[0]),
						   stol ((string&) intervals[1])));
	continue;
      case 3:
	sizes.push_back (Size (stol ((string&) intervals[0]),
						   stol ((string&) intervals[1])));
	continue;
      default:
	throw invalid_argument ("Bad range");
	return;
	;
      }
    }
  } catch (...) {
    throw invalid_argument ("Bad size option: "+option);
  }
}

ostream &
obelix::operator << (ostream &out, const OptSizes &optSizes) {
  out << "{";
  string sep = "";
  for (auto& i : optSizes.getSizes ()) {
    out << sep << i;
    sep = ", ";
  }
  return out << "}";
}

istream &
obelix::operator >> (istream &in, OptSizes &optSizes) {
  string token;
  in >> token;
  optSizes.init (token);
  return in;
}

// ================================================================================
