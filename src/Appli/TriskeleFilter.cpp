#include <iostream>
#include <algorithm>
#include <boost/chrono.hpp>
#include <boost/filesystem.hpp>

#include "obelixDebug.hpp"
#include "TreeStats.hpp"
#include "IntegralImage.hpp"

#include "ArrayTree/triskeleArrayTreeBase.hpp"
#include "ArrayTree/ArrayTreeBuilder.hpp"
#include "Attributes/WeightAttributes.hpp"
#include "AttributeProfiles.hpp"
#include "Attributes/AreaAttributes.hpp"
#include "Attributes/AverageAttributes.hpp"
#include "Attributes/SDAttributes.hpp"
#include "Attributes/XYAttributes.hpp"
#include "Attributes/MoIAttributes.hpp"

#include "Appli/OptChanel.hpp"
#include "Appli/TriskeleFilter.hpp"

using namespace std;
using namespace boost::chrono;
using namespace obelix;
using namespace obelix::triskele;
#include "TriskeleFilter.tcpp"

// ================================================================================
template <typename PixelT>
TriskeleFilter<PixelT>::TriskeleFilter (IImage &inputImage, IImage &outputImage,
					const Point &topLeft, const Size &size, const OptFilter &optFilter)
  : inputImage (inputImage),
    outputImage (outputImage),
    optFilter (optFilter),
    topLeft (topLeft),
    size (size),
    border (size, !optFilter.includeNoDataFlag),
    graphWalker (border, optFilter.connectivity),
    pixelCount (size.getPixelsCount ()),
    inputRaster (size),
    outputRaster (size),
    ndviRaster (optFilter.optChanel.getNdviCount (), Raster<PixelT> (size)),
    sobelRasters (optFilter.optChanel.getSobelCount (), Raster<PixelT> (size)),
    integralImage (pixelCount),
    doubleIntegralImage (pixelCount),
    tree (optFilter.coreCount),
    weightAttributes (tree, false) {
  DEF_LOG ("TriskeleFilter::TriskeleFilter", "size: " << size << " pixelCount: " << pixelCount);
  if (!optFilter.oneBand)
    outputImage.createImage (size, inputImage.getDataType (), optFilter.optChanel.getOutputCount (), inputImage, topLeft);
}

// ================================================================================
template <typename PixelT>
TriskeleFilter<PixelT>::~TriskeleFilter () {
  DEF_LOG ("TriskeleFilter::~TriskeleFilter", "");
}

// ================================================================================
template <typename PixelT>
void
TriskeleFilter<PixelT>::parseInput () {
  DEF_LOG ("TriskeleFilter::parseInput", "");
  updateBorder ();

  vector<InputChanel *> activeOrderedChanels = optFilter.optChanel.getActiveOrder ();
  waitingAP.clear ();
  for (InputChanel *inputChanelP : activeOrderedChanels)
    if (inputChanelP->chanelType == AP)
      waitingAP [inputChanelP->generateBy[0]] [inputChanelP->treeType] [inputChanelP->attributeType] = inputChanelP;

  for (InputChanel *inputChanelP : activeOrderedChanels) {
    InputChanel &inputChanel (*inputChanelP);
    if (inputChanel.chanelType == Original)
      readInput (inputChanel.inputId);
    if (inputChanel.doWrite)
      dumpInput (inputChanel);

    updateIntegralImage (inputChanel);

    for (InputChanel *childP : inputChanel.produce) {
      if (!childP->prodWrite)
	continue;
      updateSobel (inputChanel, *childP);
      updateNdvi (inputChanel, *childP);

      writeHaar (inputChanel, *childP);
      writeStat (inputChanel, *childP);
      writeAP (inputChanel, *childP);
    }
  }
  outputImage.close ();
  gTmp = vector<vector<double> > ();
}

// ================================================================================
template <typename PixelT>
Raster<PixelT> &
TriskeleFilter<PixelT>::getProducerRaster (const InputChanel &inputChanel) {
  DEF_LOG ("TriskeleFilter::getProducerRaster", "");
  switch (inputChanel.chanelType) {
  case Original:
    LOG ("inputRaster");
    return inputRaster;
  case Ndvi:
    LOG ("ndviRaster:" << inputChanel.typeRank);
    return ndviRaster [inputChanel.typeRank];
  case Sobel:
    LOG ("sobelRaster:" << inputChanel.typeRank);
    return sobelRasters [inputChanel.typeRank];
  default:
    break;
  }
  throw invalid_argument ("getProducerRaster not input chanel");
}

// ================================================================================
template <typename PixelT>
void
TriskeleFilter<PixelT>::readInput (const DimChanel &chanel) {
  DEF_LOG ("TriskeleFilter::readInput", "chanel: " << chanel);
  auto start = high_resolution_clock::now ();
  inputImage.readBand<PixelT> (inputRaster, chanel, topLeft, size);
  TreeStats::global.addTime (readStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}

// ================================================================================
template <typename PixelT>
void
TriskeleFilter<PixelT>::updateBorder () {
  DEF_LOG ("TriskeleFilter::updateBorder", "");
  PixelT *inputPixels = inputRaster.getPixels ();
  border.reset (!optFilter.includeNoDataFlag);
  if (!optFilter.includeNoDataFlag) {
    for (const InputChanel *inputChanelP : optFilter.optChanel.getOriginal ()) {
      readInput (inputChanelP->inputId);

      auto start = high_resolution_clock::now ();
      for (DimImg pixelId = 0; pixelId < pixelCount; ++pixelId)
	if (inputPixels [pixelId])
	  border.clearBorder (pixelId);
      TreeStats::global.addTime (borderStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
    }
    border.reduce ();
  }
}

// ================================================================================
template <typename PixelT>
void
TriskeleFilter<PixelT>::updateIntegralImage (const InputChanel &inputChanel) {
  DEF_LOG ("TriskeleFilter::updateIntegralImage", "inputChanel: " << inputChanel);
  if (inputChanel.prodInetgralImage) {
    LOG ("produce arithmetic bands");
    auto start = high_resolution_clock::now ();
    computeIntegralImage (size, getProducerRaster (inputChanel).getPixels (), &integralImage[0]);
    TreeStats::global.addTime (iiStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
  }
  if (inputChanel.prodDoubleInetgralImage) {
    auto start = high_resolution_clock::now ();
    doubleMatrix (size, getProducerRaster (inputChanel).getPixels (), &doubleIntegralImage[0]);
    computeIntegralImage (size, &doubleIntegralImage[0], &doubleIntegralImage[0]);
    TreeStats::global.addTime (iiStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
  }
}

// ================================================================================
template <typename PixelT>
void
TriskeleFilter<PixelT>::updateSobel (const InputChanel &inputChanel, const InputChanel &sobelChanel) {
  if (sobelChanel.chanelType != Sobel)
    return;
  DEF_LOG ("TriskeleFilter::updateSobel", "sobelChanel: " << sobelChanel);
  auto start = high_resolution_clock::now ();
  computeSobel (getProducerRaster (inputChanel), sobelRasters[sobelChanel.typeRank], gTmp, optFilter.coreCount);
  TreeStats::global.addTime (sobelStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}

// ================================================================================
template <typename PixelT>
void
TriskeleFilter<PixelT>::updateNdvi (const InputChanel &inputChanel, const InputChanel &ndviChanel) {
  if (ndviChanel.chanelType != Ndvi)
    return;
  DEF_LOG ("TriskeleFilter::updateNdvi", "ndviChanel: " << ndviChanel);
  auto start = high_resolution_clock::now ();
  computeNdvi (getProducerRaster (inputChanel),
	       ndviRaster[ndviChanel.typeRank],
	       ndviChanel.generateBy [0] == &inputChanel,
	       ndviChanel.generateBy [0]->inputId > ndviChanel.generateBy [1]->inputId);
  TreeStats::global.addTime (ndviStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}

// ================================================================================
template <typename PixelT>
void
TriskeleFilter<PixelT>::writeHaar (const InputChanel &inputChanel, const InputChanel &haarChanel) {
  if (haarChanel.chanelType != Haar)
    return;
  DEF_LOG ("TriskeleFilter::writeHaar", "haarChanel: " << haarChanel);
  if (!haarChanel.doWrite)
    return;
  vector<vector<double> > haarOutput (4, vector<double> (pixelCount));
  for (const Size pattern : haarChanel.winSizes) {
    TextWin haar (size, pattern);
    for (DimImg pixelId = 0; pixelId < pixelCount; ++pixelId) {
      // XXX uniquement en 2D
      Point p = idx2point (size, pixelId);
      double hx (haar.getHx (&integralImage [0], p.x, p.y, pixelId));
      double hy (haar.getHy (&integralImage [0], p.x, p.y, pixelId));
      haarOutput [0][pixelId] = hx;	// hx+M/2 => pixel
      haarOutput [1][pixelId] = hx+hy;	// (hx+hy)/2+M/2 => pixel
      haarOutput [2][pixelId] = hy;	// hx+M/2 => pixel
      haarOutput [3][pixelId] = hy-hx;	// (hx+hy)/2+M/2 => pixel
    }
    for (DimChanel localChanel (0), chanel (haarChanel.outputRank); localChanel < 4; ++localChanel, ++chanel)
      writeDoubleBand (&haarOutput[localChanel][0], chanel);
  }
}

// ================================================================================
template <typename PixelT>
void
TriskeleFilter<PixelT>::writeStat (const InputChanel &inputChanel, const InputChanel &statChanel) {
  if (statChanel.chanelType != Stat)
    return;
  DEF_LOG ("TriskeleFilter::writeStat", "statChanel: " << statChanel);
  if (!statChanel.doWrite)
    return;
  vector<vector<double> > statOutput (3, vector<double> (pixelCount));
  double mean, std, ent;
  for (const Size pattern : statChanel.winSizes) {
    TextWin stat (size, pattern);
    // XXX uniquement en 2D
    for (DimImg pixelId = 0; pixelId < pixelCount; ++pixelId) {
      Point p = idx2point (size, pixelId);
      stat.getMeanStdEnt (mean, std, ent, &integralImage[0], &doubleIntegralImage[0], p.x, p.y, pixelId);
      statOutput [0][pixelId] = mean;	// => pixel
      statOutput [1][pixelId] = std;	// => double
      statOutput [2][pixelId] = ent;	// => double
    }
    for (DimChanel localChanel (0), chanel (statChanel.outputRank); localChanel < 3; ++localChanel, ++chanel)
      writeDoubleBand (&statOutput[localChanel][0], chanel);
  }
}

// ================================================================================
template <typename PixelT>
void
TriskeleFilter<PixelT>::writeAP (const InputChanel &inputChanel, const InputChanel &apChanel) {
  if (apChanel.chanelType != AP)
    return;
  DEF_LOG ("TriskeleFilter::writeAP", "apChanel: " << apChanel);
  if (!apChanel.prodWrite)
    return;

  if (waitingAP[&inputChanel].empty ())
    return;
  Raster<PixelT> &inputRaster (getProducerRaster (inputChanel));
  for (const pair<TreeType, map <AttributeType, const InputChanel*> > &item : waitingAP[&inputChanel]) {
    const TreeType &treeType = item.first;
    LOG ("treeType: " << treeType);

    ArrayTreeBuilder<PixelT, PixelT> atb (inputRaster, graphWalker, treeType, optFilter.countingSortCeil);
    weightAttributes.resetDecr (getDecrFromTreetype (apChanel.treeType));
    atb.buildTree (tree, weightAttributes);

    switch (apChanel.featureType) {
    case FMean:
      writeAP2<PixelT> (atb, inputRaster, [] (Tree &tree, ArrayTreeBuilder<PixelT, PixelT> &atb, AttributeProfiles<PixelT> &attributeProfiles,
					      const Raster<PixelT> &raster, AverageAttributes &averageAttributes, AreaAttributes &areaAttributes) {
			  attributeProfiles.setValues (raster.getPixels (), averageAttributes.getValues ());
			}, item.second, [this] (PixelT *pixels, const DimChanel &chanel) { writePixelBand (pixels, chanel); });
      break;
    case FArea:
      writeAP2<DimImg> (atb, inputRaster, [] (Tree &tree, ArrayTreeBuilder<PixelT, PixelT> &atb, AttributeProfiles<DimImg> &attributeProfiles,
					      const Raster<PixelT> &raster, AverageAttributes &averageAttributes, AreaAttributes &areaAttributes) {
			  attributeProfiles.setValues (1, areaAttributes.getValues ());
			}, item.second, [this] (DimImg *pixels, const DimChanel &chanel) { writeDimImgBand (pixels, chanel); });
      break;
    case FSD:
      writeAP2<double> (atb, inputRaster, [] (Tree &tree, ArrayTreeBuilder<PixelT, PixelT> &atb, AttributeProfiles<double> &attributeProfiles,
					      const Raster<PixelT> &raster, AverageAttributes &averageAttributes, AreaAttributes &areaAttributes) {
			  SDAttributes sdAttributes (tree, areaAttributes);
			  attributeProfiles.setValues (0., sdAttributes.getValues ());
			}, item.second, [this] (double *pixels, const DimChanel &chanel) { writeDoubleBand (pixels, chanel); });
      break;
    case FMoI:
      writeAP2<double> (atb, inputRaster, [] (Tree &tree, ArrayTreeBuilder<PixelT, PixelT> &atb, AttributeProfiles<double> &attributeProfiles,
					      const Raster<PixelT> &raster, AverageAttributes &averageAttributes, AreaAttributes &areaAttributes) {
			  XYAttributes xyAttributes (tree, areaAttributes);
			  MoIAttributes moiAttributes (tree, areaAttributes, xyAttributes);
			  attributeProfiles.setValues (0., moiAttributes.getValues ());
			}, item.second, [this] (double *pixels, const DimChanel &chanel) { writeDoubleBand (pixels, chanel); });
      break;
    default:
      writeAP2<PixelT> (atb, inputRaster, [] (Tree &tree, ArrayTreeBuilder<PixelT, PixelT> &atb, AttributeProfiles<PixelT> &attributeProfiles,
					      const Raster<PixelT> &raster, AverageAttributes &averageAttributes, AreaAttributes &areaAttributes) {
			  atb.setAttributProfiles (attributeProfiles);
			}, item.second, [this] (PixelT *pixels, const DimChanel &chanel) { writePixelBand (pixels, chanel); });
    }
  }
  waitingAP[&inputChanel].clear ();
}

// ================================================================================
template <typename PixelT>
template <typename OutPixelT, typename APFunct, typename WFunct>
void
TriskeleFilter<PixelT>::writeAP2 (ArrayTreeBuilder<PixelT, PixelT> &atb, const Raster<PixelT> &raster, const APFunct &setAP, const map <AttributeType, const InputChanel*> &attrProd, const WFunct &wFunct) {
  DEF_LOG ("TriskeleFilter::writeAP2", "");
  AttributeProfiles<OutPixelT> attributeProfiles (tree);
  // atb.setAttributProfiles (attributeProfiles);
  AreaAttributes areaAttributes (tree);
  AverageAttributes averageAttributes (tree, raster, areaAttributes);
  setAP (tree, atb, attributeProfiles, raster, averageAttributes, areaAttributes);

  for (const pair<AttributeType, const InputChanel*> &item2 : attrProd) {
    LOG ("apChanel: " << *item2.second);
    const AttributeType &attributeType (item2.second->attributeType);
    LOG ("attributeType: " << attributeType);
    vector<vector<OutPixelT> > apOutput (item2.second->thresholds.size (), vector<OutPixelT> (pixelCount));
    switch (attributeType) {
    case AArea: {
      vector<DimImg> thresholds (areaAttributes.getConvertedThresholds (item2.second->thresholds));
      areaAttributes.cut (apOutput, attributeProfiles, thresholds);
    } break;
    case AWeight: {
      vector<PixelT> thresholds (weightAttributes.getConvertedThresholds (item2.second->thresholds));
      weightAttributes.cut (apOutput, attributeProfiles, thresholds);
    } break;
    case ASD: {
      SDAttributes sdAttributes (tree, areaAttributes);
      sdAttributes.cut (apOutput, attributeProfiles, item2.second->thresholds);
    } break;
    case AMoI: {
      XYAttributes xyAttributes (tree, areaAttributes);
      MoIAttributes moiAttributes (tree, areaAttributes, xyAttributes);
      moiAttributes.cut (apOutput, attributeProfiles, item2.second->thresholds);
    } break;
    }
    if (item2.second->doWrite)
      for (DimChanel localChanel (0), chanel (item2.second->outputRank); localChanel < apOutput.size (); ++localChanel, ++chanel)
	wFunct (&apOutput[localChanel][0], chanel);

    if (item2.second->produce.empty ())
      continue;
    vector<const OutPixelT*> dapSrcIdx (apOutput.size ()+1, nullptr);
    vector<OutPixelT> convertSrc;
    if (typeid (PixelT) == typeid (OutPixelT))
      dapSrcIdx[0] = (OutPixelT*) raster.getPixels ();
    else {
      convertSrc.assign (raster.getPixelsVector ().begin (), raster.getPixelsVector ().end ());
      dapSrcIdx[0] = &convertSrc[0];
    }
    for (DimChanel i = 1; i < dapSrcIdx.size (); ++i)
      dapSrcIdx[i] = &apOutput [i-1][0];

    for (InputChanel *grandchildP : item2.second->produce)
      writeDAP (dapSrcIdx, *grandchildP);
  }
}

// ================================================================================
template <typename PixelT>
template <typename OutPixelT>
void
TriskeleFilter<PixelT>::writeDAP (const vector<const OutPixelT*> &dapSrcIdx, const InputChanel &dapChanel) {
  DEF_LOG ("TriskeleFilter::writeDAP", "dapChanel: " << dapChanel);
  BOOST_ASSERT (dapChanel.chanelType == DAP);
  if (!dapChanel.doWrite)
    return;
  vector<DimChanel> dapChanelIdx = dapChanel.getRelIdx ();
  vector<vector<double> > dapOutput (dapChanelIdx.size ()-1, vector<double> (pixelCount));
  const bool dapWeight (dapChanel.dapWeight);
  for (DimImg pixelId = 0; pixelId < pixelCount; ++pixelId)
    for (DimImg localChanel = 1; localChanel < dapChanelIdx.size (); ++localChanel) {
      double dapValue = dapSrcIdx [dapChanelIdx [localChanel]][pixelId] - dapSrcIdx [dapChanelIdx [localChanel-1]][pixelId];
      if (dapWeight)
	dapValue *= dapSrcIdx [0][pixelId];
      if (dapValue < 0)
	dapValue = - dapValue;
      dapOutput [localChanel-1][pixelId] = dapValue;
    }
  for (DimChanel localChanel (0), chanel (dapChanel.outputRank); localChanel < dapOutput.size (); ++localChanel, ++chanel)
    writeDoubleBand (&dapOutput[localChanel][0], chanel);
}

// ================================================================================
template <typename PixelT>
void
TriskeleFilter<PixelT>::getOneBandImage (IImage& oneBandImage, const GDALDataType &dataType, const DimChanel &band) const {
  string outputBaseName  = boost::filesystem::path (outputImage.getFileName ()).replace_extension ("").string ();
  string outputExtension  = boost::filesystem::extension (outputImage.getFileName ());

  ostringstream fileNameStream;
  fileNameStream << outputBaseName << "-" << std::setfill ('0') << std::setw (3) << (band) << outputExtension;
  oneBandImage.setFileName (fileNameStream.str ());
  oneBandImage.createImage (optFilter.optCrop.size, dataType, 1, inputImage, optFilter.optCrop.topLeft);
}

// ================================================================================
template <typename PixelT>
void
TriskeleFilter<PixelT>::dumpInput (const InputChanel &inputChanel) {
  PixelT *pixels = nullptr;
  switch (inputChanel.chanelType) {
  case Original: pixels = inputRaster.getPixels (); break;
  case Ndvi:     pixels = ndviRaster [inputChanel.typeRank].getPixels (); break;	
  case Sobel:    pixels = sobelRasters [inputChanel.typeRank].getPixels (); break;
  default:
    break;
  }
  if (!pixels)
    return;
  DEF_LOG ("TriskeleFilter::dumpInput", "inputChanel: " << inputChanel);
  writePixelBand (pixels, inputChanel.outputRank);
}

// ================================================================================
template<typename PixelT>
void
TriskeleFilter<PixelT>::writePixelBand (PixelT *pixels, const DimChanel &chanel) {
  DEF_LOG ("TriskeleFilter::writePixelBand", "chanel: " << chanel);
  auto start = high_resolution_clock::now ();
  if (optFilter.oneBand) {
    IImage oneBandImage;
    getOneBandImage (oneBandImage, outputImage.getDataType (), chanel);
    oneBandImage.writeBand (pixels, 0);
  } else
    outputImage.writeBand (pixels, chanel);
  TreeStats::global.addTime (writeStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}

// ================================================================================
template<typename PixelT>
void
TriskeleFilter<PixelT>::writeDoubleBand (double *pixels, const DimChanel &chanel) {
  DEF_LOG ("TriskeleFilter::writeDoubleBand", "chanel: " << chanel);
  auto start = high_resolution_clock::now ();
  if (optFilter.oneBand) {
    IImage oneBandImage;
    getOneBandImage (oneBandImage, GDT_Float64, chanel);
    oneBandImage.writeBand (pixels, 0);
  } else
    outputImage.writeDoubleBand (pixels, chanel);
  TreeStats::global.addTime (writeStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}

// ================================================================================
template<typename PixelT>
void
TriskeleFilter<PixelT>::writeDimImgBand (DimImg *pixels, const DimChanel &chanel) {
  DEF_LOG ("TriskeleFilter::writeDimImgBand", "chanel: " << chanel);
  auto start = high_resolution_clock::now ();
  if (optFilter.oneBand) {
    IImage oneBandImage;
    getOneBandImage (oneBandImage, GDT_UInt32, chanel);
    oneBandImage.writeBand (pixels, 0);
  } else
    outputImage.writeDimImgBand (pixels, chanel);
  TreeStats::global.addTime (writeStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}

// ================================================================================
