#include <iostream>
#include <boost/filesystem.hpp>

#include "obelixDebug.hpp"
#include "triskeleBase.hpp"
#include "Appli/OptFilter.hpp"
#include "Appli/TriskeleFilter.hpp"

using namespace obelix;
using namespace obelix::triskele;

// ================================================================================
template<typename PixelT>
void
filterImage (OptFilter &optFilter) {
  TriskeleFilter<PixelT> triskeleFilter (optFilter.inputImage, optFilter.outputImage, optFilter.optCrop.topLeft, optFilter.optCrop.size, optFilter);
  triskeleFilter.parseInput ();
  if (optFilter.timeFlag)
    cerr << TreeStats::global.printDim () << endl
	 << TreeStats::global.printTime ();
}

// ================================================================================
int
main (int argc, char** argv, char** envp) {
  Log::debug = true;
  OptFilter optFilter (argc, argv);
  DEF_LOG ("main", "");

  switch (optFilter.inputImage.getDataType ()) {
  case GDT_Byte:
    filterImage<uint8_t> (optFilter); break;
  case GDT_UInt16:
    filterImage<uint16_t> (optFilter); break;
  case GDT_Int16:
    filterImage<int16_t> (optFilter); break;
  case GDT_UInt32:
    filterImage<uint32_t> (optFilter); break;
  case GDT_Int32:
    filterImage<int32_t> (optFilter); break;
  case GDT_Float32:
    filterImage<float> (optFilter); break;
  case GDT_Float64:
    filterImage<double> (optFilter); break;

  default :
    cerr << "unknown type!" << endl; break;
    return 1;
  }
  return 0;
}

// ================================================================================
