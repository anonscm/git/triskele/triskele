#include <stdexcept>

#include "Appli/OptRanges.hpp"

using namespace obelix;
using namespace boost;


// ================================================================================
OptRanges::OptRanges (const string &option)
  : minFlag (false),
    maxFlag (false),
    minInter (0),
    maxInter (0) {
  reset ();
  init (option);
}

OptRanges::OptRanges (int first, int last)
  : OptRanges () {
  set.resize (abs(last-first+1));
  std::iota (std::begin (set), std::end (set), min (first, last));
}

OptRanges::OptRanges (const OptRanges &optRanges)
  : OptRanges () {
  set = optRanges.set;
  minFlag = optRanges.minFlag;
  maxFlag = optRanges.maxFlag;
  minInter = optRanges.minInter;
  maxInter = optRanges.maxInter;
}

// ================================================================================
void
OptRanges::reset () {
  set.clear ();
  minFlag = maxFlag = false;
  minInter = maxInter = 0;
}

void
OptRanges::init (const string &option) {
  reset ();
  if (option.empty ())
    return;
  vector <string> fields;
  vector <string> intervals;
  try {
    split (fields, option, is_any_of (","));
    for (auto &field : fields) {
      intervals.clear();
      split (intervals, field, is_any_of ("-"));
      switch (intervals.size ()) {
      case 0:
	continue;
      case 1:
	set.push_back (stoi ((string&)intervals[0]));
	continue;
      case 2: {
	bool noMin = !intervals[0][0] || intervals[0][0] == '*';
	bool noMax = !intervals[1][0] || intervals[1][0] == '*';
	if (noMin && noMax) {
	  minFlag = true;
	  maxFlag = true;
	  continue;
	}
	if (noMin) {
	  int val = stoi ((string&)intervals[1]);
	  minInter = minFlag ? max (minInter, val) : val;
	  minFlag = true;
	  continue;
	}
	if (noMax) {
	  int val = stoi ((string&)intervals[0]);
	  maxInter = maxFlag ? min (maxInter, val) : val;
	  maxFlag = true;
	  continue;
	}
	int a = stoi ((string&)intervals[0]);
	int b = stoi ((string&)intervals[1]);
	if (a > b)
	  std::swap (a, b);
	for (int i = a; i <= b; ++i)
	  if (!contains (i))
	    set.push_back (i);
	continue;
      }
      default:
	throw invalid_argument ("Bad range");
      }
    }
  } catch (...) {
    throw invalid_argument ("Bad range option: "+option);
  }
}

// ================================================================================
void
OptRanges::setLimits (int minOpt, int maxOpt) {
  if (minOpt > maxOpt)
    std::swap (minOpt, maxOpt);
  if (minFlag && maxFlag) {
    for (int i = minOpt; i <= maxOpt; ++i)
      if (!contains (i))
	set.push_back (i);
    return;
  }
  if (minFlag) {
    for (int i = minOpt; i <= minInter; ++i)
      if (!contains (i))
	set.push_back (i);
    if (maxFlag)
      maxInter = max (maxInter, minInter+1);
  }
  if (maxFlag)
    for (int i = maxInter; i <= maxOpt; ++i)
      if (!contains (i))
	set.push_back (i);
}

// ================================================================================
OptRanges &
OptRanges::toSet () {
  sort (set.begin (), set.end ());
  set.erase (unique (set.begin (), set.end ()), set.end ());
  return *this;
}

bool
OptRanges::contains (int value) const {
  return size () && find (set.begin (), set.end (), value) != set.end ();
}

// ================================================================================
ostream &
obelix::operator << (ostream &out, const OptRanges &optRanges) {
  out << "[";
  string sep = "";
  for (auto& i : optRanges.getSet ()) {
    out << sep << i;
    sep = ", ";
  }
  return out << "]";
}

istream &
obelix::operator >> (istream &in, OptRanges &optRanges) {
  string token;
  in >> token;
  optRanges.init (token);
  return in;
}

// ================================================================================
