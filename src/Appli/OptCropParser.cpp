#include <stdexcept>
#include <boost/program_options.hpp>

#include "obelixDebug.hpp"
#include "misc.hpp"

#include "Appli/OptCrop.hpp"
#include "Appli/OptCropParser.hpp"

using namespace std;
using namespace boost;
using namespace boost::program_options;

using namespace obelix;

namespace po = boost::program_options;

// ================================================================================

OptCropParser::OptCropParser ()
  : left (-1),
    top (-1),
    width (-1),
    height (-1),
    description ("Crop options", getCols ()) {
  DEF_LOG ("OptCropParser::OptCropParser", "");

  description.add_options ()
      ("left,x", po::value<long> (&left)->default_value (left), "left crop (-1 = center)")
      ("top,y", po::value<long> (&top)->default_value (top), "top crop (-1 = middle)")
      ("width,w", po::value<long> (&width)->default_value (width), "width crop (-1 = input width)")
      ("height,h", po::value<long> (&height)->default_value (height), "height crop (-1 input height)")
    ;
}

// ================================================================================
void
OptCropParser::parse (OptCrop &optCrop, const IImage &inputImage, po::basic_parsed_options<char> &parsed) {
  DEF_LOG ("OptCropParser::parse", "");

  Size imgSize = inputImage.getSize ();
  if (imgSize.isNull ())
    throw invalid_argument ("empty input image");
  if (width < 0 || width > (long) imgSize.width)
    width = left <= 0 ? (long) imgSize.width : min (1L, (long) imgSize.width-left);
  if (height < 0 || height > (long) imgSize.height)
    height = top <= 0 ? (long) imgSize.height : min (1L, (long) imgSize.height-top);
  if (left < 0)
    left = ((long) imgSize.width - width)/2;
  if (top < 0)
    top = ((long) imgSize.height - height)/2;
  if (left > (long) imgSize.width - width)
    left = (long) imgSize.width - width;
  if (top > (long) imgSize.height - height)
    top = (long) imgSize.height - height;
  optCrop.topLeft = Point ((DimSideImg) left, (DimSideImg) top, (DimChanel) 0);
  optCrop.size = Size ((DimSideImg) width, (DimSideImg) height, imgSize.length);
}


// ================================================================================
