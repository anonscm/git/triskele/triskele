#include <stdexcept>
#include <boost/assign.hpp>
#include <boost/assign/list_of.hpp>
#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>
#include <set>
#include <queue>
#include <algorithm>
#include <tinyxml.h>

#include "misc.hpp"
#include "obelixDebug.hpp"
#include "triskeleGdalGetType.hpp"

#include "Appli/OptChanel.hpp"

using namespace std;
using namespace boost::filesystem;
using namespace obelix;
using namespace obelix::triskele;

// ================================================================================
const string
OptChanel::chExt (".ch");

// ================================================================================
const string
obelix::triskele::apOrigPosLabels[] = {
  "apOrigNoPos", "apOrigPosBegin", "apOrigPosEnd", "apOrigPosBoth",
  "apOrigPosEverywhere", "apOrigPosBeginMin", "apOrigPosBeginMax"
};
const map<string, ApOrigPos>
obelix::triskele::ApOrigPosMap = boost::assign::map_list_of
  ("aporignopos", apOrigNoPos)
  ("aporigposbegin", apOrigPosBegin)
  ("aporigposend", apOrigPosEnd)
  ("aporigposboth", apOrigPosBoth)
  ("aporigposeverywhere", apOrigPosEverywhere)
  ("aporigposbeginmin", apOrigPosBeginMin)
  ("aporigposbeginmax", apOrigPosBeginMax)
  ;

ostream &
obelix::triskele::operator << (ostream &out, const ApOrigPos &pos) {
  BOOST_ASSERT (pos >= apOrigNoPos && pos <= apOrigPosBeginMax);
  return out << apOrigPosLabels[pos];
}

istream &
obelix::triskele::operator >> (istream &in, ApOrigPos &apOrigPos) {
  string token;
  in >> token;
  auto pos = ApOrigPosMap.find (boost::algorithm::to_lower_copy (token));
  if (pos == ApOrigPosMap.end ())
    in.setstate (ios_base::failbit);
  else
    apOrigPos = pos->second;
  return in;
}

// ================================================================================
const string
obelix::triskele::chanelTypeLabels[] = {
  "Original", "Ndvi", "Sobel", "AP", "DAP", "Haar", "Stat"
};

const map<string, ChanelType>
obelix::triskele::chanelTypeMap = boost::assign::map_list_of
  ("original", Original)
  ("ndvi", Ndvi)
  ("sobel", Sobel)
  ("ap", AP)
  ("dap", DAP)
  ("haar", Haar)
  ("stat", Stat)
  ;

ostream &
obelix::triskele::operator << (ostream &out, const ChanelType &chanelType) {
  if (chanelType >= Original && chanelType <= Stat)
    return out << chanelTypeLabels[chanelType];
  BOOST_ASSERT (false);
  return out << (int) chanelType;
}

istream &
obelix::triskele::operator >> (istream &in, ChanelType &chanelType) {
  string token;
  in >> token;
  auto pos = chanelTypeMap.find (boost::algorithm::to_lower_copy (token));
  if (pos == chanelTypeMap.end ())
    in.setstate (ios_base::failbit);
  else
    chanelType = pos->second;
  return in;
}

// ================================================================================
InputChanel::InputChanel (const ChanelType &chanelType, const DimChanel &inputId, const bool &doWrite)
  : chanelType (chanelType),
    inputId (inputId),
    treeType (MIN),
    featureType (FAP),
    attributeType (AArea),
    dapWeight (false),
    apOrigPos (apOrigPosBegin),
    doWrite (doWrite) {
  resetTransient ();
  DEF_LOG ("InputChanel::InputChanel", "inputId: " << inputId << " this: " << *this);
}
InputChanel::InputChanel (const ChanelType &chanelType, const DimChanel &inputId, const bool &doWrite, const vector<InputChanel *> &generateBy)
  : InputChanel (chanelType, inputId, doWrite) {
  this->generateBy = generateBy;
}

void
InputChanel::resetTransient () {
  prodWrite = prodNewImage = prodInetgralImage = prodDoubleInetgralImage = prodDap = false;
  copyRank = typeRank = origDapRank = integralImageRank = doubleIntegralImageRank = outputRank = 0;
}

bool
InputChanel::updateProdWrite () {
  return prodWrite = prodWrite || doWrite || [this] {
    for (InputChanel* child : produce)
      if (child->updateProdWrite ())
	return true;
    return false;
  } ();
}

// ================================================================================
vector<DimChanel>
InputChanel::getRelIdx () const {
  BOOST_ASSERT (chanelType == DAP);
  bool decr (getDecrFromTreetype (generateBy [0]->treeType));
  DimChanel nbApSrc (generateBy[0]->thresholds.size ());
  vector<DimChanel> result;
  DimChanel nbDapIdx (nbApSrc+1);
  switch (apOrigPos) {
  case apOrigNoPos:		--nbDapIdx; break;
  case apOrigPosBoth:		++nbDapIdx; break;
  case apOrigPosEverywhere:	nbDapIdx += nbApSrc-2; break;
  default:
    ;//std::cout << "This apOrigPos value is not handled\n"; exit(1);
  }
  result.resize (nbDapIdx, DimChanel (0));
  DimChanel start = 0;
  switch (apOrigPos) {
  case apOrigPosBeginMin:	start = 1; break;
  default:
    ; //std::cout << "This apOrigPos value is not handled\n"; exit(1);
  }
  if (decr && (apOrigPos == apOrigPosBeginMin || apOrigPos == apOrigPosBeginMax))
    start = 1 - start;
  if (apOrigPos == apOrigPosEverywhere)
    for (DimChanel i = 0; i < nbApSrc; ++i)
      result [2*i] = i;
  else
    for (DimChanel i = 0; i < nbApSrc; ++i, ++start)
      result [start] = i;
  return result;
}

// ================================================================================
void
InputChanel::writeXml (TiXmlElement *triskeleNode) const {
  DEF_LOG ("InputChanel::writeXml", *this);
  if (chanelType == Original && !doWrite)
    return;
  TiXmlElement *elem (new TiXmlElement (chanelTypeLabels [chanelType]));
  elem->SetAttribute ("id", inputId);
  if (doWrite)
    elem->SetAttribute ("doWrite", "true");
  if (generateBy.size ())
    elem->SetAttribute ("from", generateBy[0]->inputId);
  if (generateBy.size () > 1)
    elem->SetAttribute ("from2", generateBy[1]->inputId);
  for (Size size : winSizes) {
    TiXmlElement *elemSize (new TiXmlElement ("Size"));
    elemSize->SetAttribute ("width", size.width);
    elemSize->SetAttribute ("height", size.height);
    elem->LinkEndChild (elemSize);
  }
  if (chanelType == AP) {
    elem->SetAttribute ("treeType", treeTypeLabels[treeType]);
    elem->SetAttribute ("featureType", featureTypeLabels[featureType]);
    elem->SetAttribute ("attributeType", attributeLabels[attributeType]);
  }
  for (double threshold : thresholds) {
    TiXmlElement *elemThreshold (new TiXmlElement ("Cut"));
    elemThreshold->SetAttribute ("threshold", threshold);
    elem->LinkEndChild (elemThreshold);
  }
  if (chanelType == DAP) {
    if (dapWeight)
      elem->SetAttribute ("dapWeight", "true");
    elem->SetAttribute ("apOrigPos", apOrigPosLabels[apOrigPos]);
  }
  for (string comment : localComments) {
    TiXmlComment *child = new TiXmlComment ();
    child->SetValue (comment);
    elem->LinkEndChild (child);
  }
  triskeleNode->LinkEndChild (elem);
}

ostream &
obelix::triskele::operator << (ostream &out, const InputChanel &inputChanel) {
  out << "[" << inputChanel.chanelType << " " << inputChanel.inputId;
  if (inputChanel.generateBy.size ()) {
    out << "(" << inputChanel.generateBy[0]->inputId;
    if (inputChanel.generateBy.size () > 1)
      out << "," << inputChanel.generateBy[1]->inputId;
    out << ")";
  }
  if (inputChanel.doWrite)
    out << " > " << inputChanel.outputRank << " C:" << inputChanel.copyRank;
  if (inputChanel.prodWrite)
    out << " R:" << inputChanel.typeRank;
  if (inputChanel.prodInetgralImage)
    out << " II:" << inputChanel.integralImageRank;
  if (inputChanel.prodDoubleInetgralImage)
    out << " dII:" << inputChanel.doubleIntegralImageRank;
  if (inputChanel.prodNewImage)
    out << " prodNewImage";
  if (inputChanel.prodDap)
    out << " prodDap";
  if (inputChanel.winSizes.size ())
    out << " " << inputChanel.winSizes;
  if (inputChanel.thresholds.size ())
    obelix::operator<< (out << " " << inputChanel.treeType << " " << inputChanel.featureType << " " << inputChanel.attributeType << " ", inputChanel.thresholds);
  if (inputChanel.chanelType == DAP)
    out << (inputChanel.dapWeight ? " dapWeight" : "") << " " << inputChanel.apOrigPos;
  return out << "]";
}

// ================================================================================
void
OptChanel::resetTransient () {
  nextChanelUniqId = copyCount = originalUsedCount = origDapUsedCount = ndviUsedCount = sobelUsedCount = integralImageCount = doubleIntegralImageCount = apCount = dapCount = haarCount = statCount = outputCount = 0;
}

DimChanel
OptChanel::getSize (const ChanelType &chanelType) const {
  return chanels [chanelType].size ();
}

OptChanel::OptChanel ()
  : created (getCurrentTime ()),
    inputType (GDT_Unknown),
    chanels (ChanelType::Stat+1, vector <InputChanel *> (0)),
    selectedTreeType (MIN),
    selectedFeatureType (FAP),
    selectedAttributeType (AArea),
    spectralDepth (0),
    mixedBandFlag (false) {
  resetTransient ();
}

OptChanel::~OptChanel () {
}

bool
OptChanel::operator == (const OptChanel &rhs) const {
  try {
    if (inputType != rhs.inputType ||
	chanels[Original].size () != rhs.chanels[Original].size () ||
	outputOrder.size () != rhs.outputOrder.size ())
      return false;
    // XXX verif apOrder, dapOrder, haarOrder, statOrder
    for (map<DimChanel, InputChanel *>::const_iterator it = outputOrder.cbegin ();
	 it != outputOrder.cend (); ++it) {
      DimChanel rank = it->first;
      const InputChanel *left (outputOrder.at (rank)), *right (rhs.outputOrder.at (rank));
      if (left->chanelType != right->chanelType ||
	  left->typeRank != right->typeRank)
	return false;
      for (DimChanel parentId = 0; parentId < left->generateBy.size (); ++parentId)
	if (left->generateBy [parentId]->chanelType != right->generateBy [parentId]->chanelType ||
	    left->generateBy [parentId]->typeRank != right->generateBy [parentId]->typeRank)
	  return false;
      switch (left->chanelType) {
      case Original:
      case Ndvi:
      case Sobel:
	break;
      case AP:
	if (left->treeType != right->treeType ||
	    left->featureType != right->featureType ||
	    left->attributeType != right->attributeType ||
	    left->thresholds.size () != right->thresholds.size ())
	  return false;
	for (DimChanel idx = 0; idx < left->thresholds.size (); ++idx)
	  if (left->thresholds[idx] != right->thresholds[idx])
	    return false;
	break;
      case DAP:
	if (left->dapWeight != right->dapWeight ||
	    left->apOrigPos != right->apOrigPos)
	  return false;
      case Haar:
      case Stat:
	if (left->winSizes.size () != right->winSizes.size ())
	  return false;
	for (DimChanel idx = 0; idx < left->winSizes.size (); ++idx)
	  if (left->winSizes[idx] != right->winSizes[idx])
	    return false;
	break;
      }
    }
    return true;
  } catch (...) {
    return false;
  }
}

bool
OptChanel::isEmpty () const {
  return inputType == GDT_Unknown || !chanels[Original].size ();
}

bool
OptChanel::inputMatches (const GDALDataType &inputType, const DimChanel &inputBandsCount) const {
  return this->inputType == inputType && chanels[Original].size () == inputBandsCount;
}

void
OptChanel::setType (const GDALDataType &inputType, const DimChanel &inputBandsCount) {
  this->inputType = inputType;
  remove (chanels[Original]);
  BOOST_ASSERT (chanels[Original].empty ());
  for (nextChanelUniqId = 0; nextChanelUniqId < inputBandsCount; ++nextChanelUniqId)
    chanels [Original].push_back (new InputChanel (Original, nextChanelUniqId));
}

void
OptChanel::setName (const string &name) {
  this->name = name;
}

// ================================================================================
void
OptChanel::loadFile (const string &fileName) {
  static const set<string> inputChanelSet = {
    chanelTypeLabels [Original], chanelTypeLabels [Ndvi], chanelTypeLabels [Sobel], chanelTypeLabels [AP],
    chanelTypeLabels [DAP], chanelTypeLabels [Haar], chanelTypeLabels [Stat]
  };

  DEF_LOG ("OptChanel::loadFile", "fileName: " << fileName);
  TiXmlDocument doc (fileName.c_str ());
  if (!doc.LoadFile ()) {
    LOG ("Can't load");
    throw invalid_argument (string ("Can't load chanel config file ") + fileName +" (" + current_path ().string () + ")!");
  }
  string typeValue;
  int inputBandsCount (0);
  const TiXmlNode *triskeleRoot (doc.FirstChild ("Triskele"));
  if (!triskeleRoot) {
    LOG ("Not a chanel config file!");
    throw invalid_argument ("Not a chanel config file!");
  }
  const TiXmlElement *triskeleElem = doc.FirstChild ("Triskele")->ToElement ();
  if (triskeleElem->QueryStringAttribute ("type", &typeValue) != TIXML_SUCCESS ||
      triskeleElem->QueryIntAttribute ("bandCount", &inputBandsCount) != TIXML_SUCCESS)
    return;
  outComments.clear ();
  comments.clear ();
  for (const TiXmlNode *child = doc.FirstChild (); child; child = child->NextSibling ())
    if (child->Type () == TiXmlNode::TINYXML_COMMENT)
      outComments.push_back (child->Value ());

  int sp (0);
  bool mb (false);
  setType (GDALGetDataTypeByName (typeValue.c_str ()), inputBandsCount);
  triskeleElem->QueryIntAttribute ("spectralDepth", &sp);
  triskeleElem->QueryBoolAttribute ("mixedBand", &mb);
  mixedBandFlag = mb;
  spectralDepth = sp;
  triskeleElem->QueryStringAttribute ("name", &name);
  triskeleElem->QueryStringAttribute ("created", &created);
  triskeleElem->QueryStringAttribute ("modified", &modified);

  for (const TiXmlNode *child = triskeleElem->FirstChild (); child; child = child->NextSibling ())
    try {
      if (child->Type () == TiXmlNode::TINYXML_COMMENT) {
	comments.push_back (child->Value ());
	continue;
      }
      string token (child->Value ());
      if (inputChanelSet.find (token) == inputChanelSet.end ()) {
	// XXX trace
	continue;
      }
      ChanelType chanelType = chanelTypeMap.find (boost::algorithm::to_lower_copy (token))->second;

      const TiXmlElement *elem = child->ToElement ();
      bool doWrite (false);
      int inputId (0), from (0), from2 (0);
      vector<Size> winSizes;
      int width (0), height (0);
      vector<double> thresholds;
      double threshold (0);
      string treeType, featureType (featureTypeLabels[FAP]), attributeType;
      bool dapWeight (false);
      string dapPos;
      vector<string> localComments;

      if (elem->QueryIntAttribute ("id", &inputId) != TIXML_SUCCESS)
	continue;
      LOG ("chanelType: " << chanelType << " id: " << inputId);
      elem->QueryBoolAttribute ("doWrite", &doWrite);
      if (chanelType != Original &&
	  elem->QueryIntAttribute ("from", &from) != TIXML_SUCCESS)
	continue;
      if (chanelType == Ndvi &&
	  elem->QueryIntAttribute ("from2", &from2) != TIXML_SUCCESS)
	continue;
      if (chanelType == AP &&
	  (elem->QueryStringAttribute ("treeType", &treeType) != TIXML_SUCCESS ||
	   elem->QueryStringAttribute ("attributeType", &attributeType) != TIXML_SUCCESS))
	continue;
      elem->QueryStringAttribute ("featureType", &featureType);
      elem->QueryBoolAttribute ("dapWeight", &dapWeight);
      if (chanelType == DAP &&
	  elem->QueryStringAttribute ("apOrigPos", &dapPos) != TIXML_SUCCESS)
	continue;

      switch (chanelType) {
      case Original:
	chanels [Original][inputId]->doWrite = doWrite;
	break;
      case Ndvi:
	addNdvi (inputId, from, from2, doWrite);
	break;
      case Sobel:
	addSobel (inputId, from, doWrite);
	break;
      case AP:
	for (const TiXmlNode *child2 = child->FirstChild ("Cut"); child2; child2 = child2->NextSibling ("Cut")) {
	  if (child2->Type () == TiXmlNode::TINYXML_COMMENT) {
	    localComments.push_back (child->Value ());
	    continue;
	  }
	  if (child2->ToElement ()->QueryDoubleAttribute ("threshold", &threshold) != TIXML_SUCCESS)
	    continue;
	  thresholds.push_back (threshold);
	}
	addThresholds (inputId, from, doWrite, localComments,
		       treeTypeMap.find (boost::algorithm::to_lower_copy (treeType))->second,
		       featureTypeMap.find (boost::algorithm::to_lower_copy (featureType))->second,
		       attributeMap.find (boost::algorithm::to_lower_copy (attributeType))->second,
		       thresholds);
	break;
      case DAP:
	addDap (inputId, from, doWrite, dapWeight,
		ApOrigPosMap.find (boost::algorithm::to_lower_copy (dapPos))->second);
	break;
      case Haar:
      case Stat:
	for (const TiXmlNode *child2 = child->FirstChild ("Size"); child2; child2 = child2->NextSibling ("Size")) {
	  if (child2->Type () == TiXmlNode::TINYXML_COMMENT) {
	    localComments.push_back (child->Value ());
	    continue;
	  }
	  if (child2->ToElement ()->QueryIntAttribute ("width", &width) != TIXML_SUCCESS ||
	      child2->ToElement ()->QueryIntAttribute ("height", &height) != TIXML_SUCCESS)
	    continue;
	  winSizes.push_back (Size (width, height));
	}
	addTexture (chanelType, inputId, from, doWrite, localComments, winSizes);
	break;
      }
    } catch (std::exception& e) {
      cerr << "error: " << e.what() << ". Loading file continue..." << endl;
    }
  updateChanelUniqId ();
}

void
OptChanel::saveFile (const string &fileName) const {
  DEF_LOG ("OptChanel::saveFile", "fileName: " << fileName);
  // XXX update (); //

  TiXmlDeclaration *decl = new TiXmlDeclaration ( "1.0", "", "" );
  TiXmlDocument doc;
  doc.LinkEndChild (decl);

  for (string comment : outComments) {
    TiXmlComment *child = new TiXmlComment ();
    child->SetValue (comment);
    doc.LinkEndChild (child);
  }
  TiXmlElement *triskeleElem = new TiXmlElement ("Triskele");
  doc.LinkEndChild (triskeleElem);
  triskeleElem->SetAttribute ("type", GDALGetDataTypeName (inputType));
  triskeleElem->SetAttribute ("bandCount", chanels [Original].size ());
  if (spectralDepth)
    triskeleElem->SetAttribute ("spectralDepth", spectralDepth);
  if (mixedBandFlag)
    triskeleElem->SetAttribute ("mixedBand", "true");
  if (!name.empty ())
    triskeleElem->SetAttribute ("name", name);
  if (!created.empty ())
    triskeleElem->SetAttribute ("created", created);
  triskeleElem->SetAttribute ("modified", getCurrentTime ());

  vector<InputChanel *> circularDefinition;
  for (InputChanel *inputChanel : printableOrder (circularDefinition))
    inputChanel->writeXml (triskeleElem);
  for (string comment : comments) {
    TiXmlComment *child = new TiXmlComment ();
    child->SetValue (comment);
    triskeleElem->LinkEndChild (child);
  }
  doc.SaveFile (fileName.c_str ());
}

// ================================================================================
InputChanel *
OptChanel::findChanel (const DimChanel &chanelId) {
  DEF_LOG ("InputChanel::findChanel", "chanelId: " << chanelId);
  for (DimChanel chanelType = 0; chanelType < chanels.size (); ++chanelType) {
    vector <InputChanel *> &dedicatedChanel (chanels[chanelType]);
    for (DimChanel i = 0; i < dedicatedChanel.size (); ++i) {
      LOG ("test: " << *dedicatedChanel[i]);
      if (dedicatedChanel[i]->inputId == chanelId)
	return dedicatedChanel[i];
    }
  }
  LOG ("not found!");
  return nullptr;
}

void
OptChanel::updateChanelUniqId () {
  int newValue (0);
  for (int chanelType = Original; chanelType <= Stat; ++chanelType) {
    vector <InputChanel *> &dedicatedChanel (chanels[chanelType]);
    for (DimChanel i = 0; i < dedicatedChanel.size (); ++i)
      newValue = DimChanel (max (newValue, 1+dedicatedChanel[i]->inputId));
  }
  nextChanelUniqId = newValue;
}

void
OptChanel::updateChanels () {
  DEF_LOG ("InputChanel::updateChanels", "");
  // XXX trier AP par input/tree/featureType/attributeType => ne pas recalculer l'arbre
  vector <InputChanel *> circularDefinition;
  vector <InputChanel *> orderedInputChanels (printableOrder (circularDefinition));
  remove (circularDefinition);
  resetTransient ();
  for (InputChanel *inputChanel : orderedInputChanels) {
    inputChanel->resetTransient ();
    inputChanel->inputId = nextChanelUniqId++;
    inputChanel->produce.clear ();
    for (InputChanel *parent : inputChanel->generateBy)
      parent->produce.push_back (inputChanel);
  }
  outputOrder.clear ();
  apOrder.clear ();
  dapOrder.clear ();
  haarOrder.clear ();
  statOrder.clear ();
  for (InputChanel *inputChanel : orderedInputChanels) {
    inputChanel->updateProdWrite ();
    for (InputChanel* child : inputChanel->produce) {
      if (!child->updateProdWrite ())
	continue;
      switch (child->chanelType) {
      case Sobel: inputChanel->prodNewImage = true; break;
      case AP:
	for (InputChanel* grandChild : child->produce) {
	  if (!grandChild->updateProdWrite ())
	    continue;
	  if (grandChild->chanelType == DAP)
	    inputChanel->prodDap = true;
	  break;
	}
      case Haar: inputChanel->prodInetgralImage = true; break;
      case Stat: inputChanel->prodInetgralImage = inputChanel->prodDoubleInetgralImage = true; break;
      default:
	; //std::cout << "This chanelType value is not handled\n"; exit(1);
      }
    }
    if (inputChanel->prodInetgralImage)
      inputChanel->integralImageRank = integralImageCount++;
    if (inputChanel->prodDoubleInetgralImage)
      inputChanel->doubleIntegralImageRank = doubleIntegralImageCount++;
    if (inputChanel->updateProdWrite ())
      switch (inputChanel->chanelType) {
      case Original:
	if (inputChanel->prodDap && !inputChanel->doWrite)
	  inputChanel->origDapRank = origDapUsedCount++;
	inputChanel->typeRank = originalUsedCount++;
	break;
      case Ndvi: inputChanel->typeRank = ndviUsedCount++; break;
      case Sobel: inputChanel->typeRank = sobelUsedCount++; break;
      case AP:
	apOrder[inputChanel->typeRank = apCount++] = inputChanel;
	break;
      case DAP:
	dapOrder[inputChanel->typeRank = dapCount++] = inputChanel;
	break;
      case Haar:
	haarOrder[inputChanel->typeRank = haarCount++] = inputChanel;
	break;
      case Stat:
	statOrder[inputChanel->typeRank = statCount++] = inputChanel;
	break;
      }
    if (inputChanel->doWrite) {
      inputChanel->outputRank = outputCount;
      outputOrder[outputCount] = inputChanel;
      switch (inputChanel->chanelType) {
      case Original:
      case Ndvi:
      case Sobel:
	inputChanel->copyRank = copyCount++;
	++outputCount;
	break;
      case AP:
	outputCount += inputChanel->thresholds.size ();
	break;
      case DAP:
	outputCount += inputChanel->generateBy[0]->thresholds.size ();
	switch (inputChanel->apOrigPos) {
	case apOrigNoPos:		--outputCount; break;
	case apOrigPosBoth:		++outputCount; break;
	case apOrigPosEverywhere:	outputCount += inputChanel->generateBy[0]->thresholds.size ()-2; break;
	default:
	  ;//std::cout << "This apOrigPos value is not handled\n"; exit(1);
  }
	break;
      case Haar:
	outputCount += 4;
	break;
      case Stat:
	outputCount += 3;
	break;
      }
    }
  }
}

// ================================================================================
vector<InputChanel*>
OptChanel::printableOrder (vector<InputChanel*> &circularDefinition) const {
  DEF_LOG ("InputChanel::printableOrder", "original.size: " << chanels [Original].size ());
  vector<InputChanel*> result (chanels [Original]);
  queue<InputChanel*> toPrint;
  for (int chanelType = Ndvi; chanelType <= Stat; ++chanelType)
    for (DimChanel i = 0; i < chanels [chanelType].size (); ++i)
      toPrint.push (chanels [chanelType][i]);
  result.reserve (result.size ()+toPrint.size ());

  queue<InputChanel*> nextStage;
  for (DimChanel toPrintSize = 0;;) {
    toPrintSize = toPrint.size ();
    if (!toPrintSize)
      return result;

    while (toPrint.size ())
      [&] {
	InputChanel* inputChanel = toPrint.front ();
	toPrint.pop ();
	for (InputChanel* parent : inputChanel->generateBy) {
	  if (find (result.begin (), result.end (), parent) != result.end ())
	    continue;
	  nextStage.push (inputChanel);
	  return;
	}
	result.push_back (inputChanel);
      } ();

    nextStage.swap (toPrint);
    if (toPrintSize == toPrint.size ()) {
      LOG ("chanels loop or orphans");
      circularDefinition.clear ();
      for (;!toPrint.empty (); toPrint.pop ())
	circularDefinition.push_back (toPrint.front ());
      return result;
    }
  }
  // XXX never execute
}

vector<InputChanel*>
OptChanel::getActiveOrder () const {
  vector<InputChanel *> circularDefinition;
  vector <InputChanel *> orderedInputChanels (printableOrder (circularDefinition));
  vector<InputChanel*> result;
  result.reserve (orderedInputChanels.size ());
  for (InputChanel *inputChanel : orderedInputChanels)
    if (inputChanel->prodWrite)
      result.push_back (inputChanel);
  return result;
}

// ================================================================================
void
OptChanel::selectInputs (const vector<int> &chanelsId) {
  DEF_LOG ("OptChanel::selectInputs", "chanelsId: " << chanelsId.size ());
  selectedSet.clear ();
  selectedSet.reserve (chanelsId.size ());
  for (int id : chanelsId) {
    InputChanel *inputChanel = findChanel (id);
    if (inputChanel && inputChanel->chanelType <= Sobel)
      // XXX vérifier double
      selectedSet.push_back (findChanel (id));
  }
}
void
OptChanel::selectTreeType (const TreeType &treeType) {
  selectedTreeType = treeType;
}
void
OptChanel::selectFeatureType (const FeatureType &featureType) {
  selectedFeatureType = featureType;
}
void
OptChanel::selectAttributeType (const AttributeType &attributeType) {
  this->selectedAttributeType = attributeType;
}
vector <InputChanel *>
OptChanel::selectedAP () {
  vector <InputChanel *> result;
  vector <InputChanel *> &dedicatedChanel (chanels[AP]);
  for (InputChanel *inputChanel : selectedSet)
    for (InputChanel *apChanel : dedicatedChanel) {
      BOOST_ASSERT (apChanel->generateBy.size () == 1);
      if (apChanel->generateBy[0] == inputChanel &&
	  apChanel->treeType == selectedTreeType &&
	  apChanel->featureType == selectedFeatureType &&
	  apChanel->attributeType == selectedAttributeType)
	result.push_back (apChanel);
    }
  return result;
}

// ================================================================================
void
OptChanel::remove (const vector<InputChanel *> &toRemove) {
  DEF_LOG ("OptChanel::remove", "toRemove.size: " << toRemove.size ());
  vector<InputChanel *> immutableRemove (toRemove);
  for (InputChanel *inputChanel : immutableRemove) {
    vector<InputChanel *> &vector (chanels[inputChanel->chanelType]);
    vector.erase (find (vector.begin (), vector.end (), inputChanel));
  }
}
void
OptChanel::remove (const ChanelType &chanelType) {
  remove (chanels [chanelType]);
  BOOST_ASSERT (chanels [chanelType].empty ());
}
void
OptChanel::removeCopy () {
  for (ChanelType chanelType : {Original, Ndvi, Sobel})
    for (DimChanel i = 0; i < chanels [chanelType].size (); ++i)
      chanels [chanelType][i]->doWrite = false;
}
void
OptChanel::removeThresholds () {
  DEF_LOG ("OptChanel::removeThresholds", "selectedTreeType: " << selectedTreeType << "selectedFeatureType: " << selectedFeatureType << " selectedAttributeType: " << selectedAttributeType);
  remove (selectedAP ());
}
  
// ================================================================================
void
OptChanel::addNdvi (const DimChanel &first, const DimChanel &last) {
  addNdvi (nextChanelUniqId++, first, last, false);
}
void
OptChanel::addNdvi (const DimChanel &id, const DimChanel &first, const DimChanel &last, const bool &doWrite) {
  DEF_LOG ("OptChanel::addNdvi", "id:" << id << " first: " << first << " last: " << last);
  if (first == last)
    throw std::invalid_argument ("ndvi input must be different");
  InputChanel *firstChanel (findChanel (first)), *lastChanel (findChanel (last));
  if (!firstChanel || !lastChanel)
    throw std::invalid_argument ("ndvi out of input");
  for (const InputChanel *registeredNdvi : chanels [Ndvi])
    if (registeredNdvi->generateBy[0] == firstChanel && registeredNdvi->generateBy[1] == lastChanel)
      return;
  if (findChanel (id))
    throw invalid_argument ("ndvi id already used");
  LOG ("id:" << id << " first: " << *firstChanel << " last: " << *lastChanel);
  chanels [Ndvi].push_back (new InputChanel (Ndvi, id, doWrite, boost::assign::list_of (firstChanel) (lastChanel)));
}

void
OptChanel::addSobel (const DimChanel &chanel) {
  addSobel (nextChanelUniqId++, chanel, false);
}
void
OptChanel::addSobel (const DimChanel &id, const DimChanel &chanel, const bool &doWrite) {
  DEF_LOG ("OptChanel::addSobel", chanel);
  InputChanel *originalChanel (findChanel (chanel));
  if (!originalChanel)
    throw std::invalid_argument ("sobel out of input");
  for (const InputChanel *registeredSobel : chanels [Sobel])
    if (registeredSobel->generateBy[0] == originalChanel)
      return;
  if (findChanel (id))
    throw std::invalid_argument ("sobel id already used");
  chanels [Sobel].push_back (new InputChanel (Sobel, id, doWrite, boost::assign::list_of (originalChanel)));
}

void
OptChanel::addCopy (const DimChanel &chanel) {
  DEF_LOG ("OptChanel::addCopy", chanel);
  InputChanel *inputChanel (findChanel (chanel));
  if (!inputChanel)
    throw std::invalid_argument ("copy out of input");
  inputChanel->doWrite = true;
}

// ================================================================================
void
OptChanel::addThresholds (const string &thresholds) {
  DEF_LOG ("OptChanel::addThresholds", "thresholds: " << thresholds);
  if (thresholds.empty ()) {
    removeThresholds ();
    return;
  }
  vector <string> values;
  split (values, thresholds, is_any_of (","));
  vector <double> doubles (values.size ());
  std::transform (values.begin (), values.end (), doubles.begin (), [] (const std::string& val) { return std::stod (val); });
  addThresholds (doubles);
}

void
OptChanel::addThresholds (const vector<double> &thresholds) {
  DEF_LOG ("OptChanel::addThresholds", "thresholds.size: " << thresholds.size ());
  for (InputChanel *inputChanel : selectedSet)
    addThresholds (nextChanelUniqId++, true, selectedTreeType, selectedFeatureType, selectedAttributeType, thresholds, inputChanel, vector<string> ());
}
void
OptChanel::addThresholds (const DimChanel &id, const DimChanel &from, const bool &doWrite, const vector<string> &localComments,
			  const TreeType &treeType, const FeatureType &featureType, const AttributeType &attributeType, const vector<double> &thresholds) {
  // XXX test from exist
  addThresholds (id, doWrite, treeType, featureType, attributeType, thresholds, findChanel (from), localComments);
}
void
OptChanel::addThresholds (const DimChanel &id, const bool &doWrite,
			     const TreeType &treeType, const FeatureType &featureType, const AttributeType &attributeType, const vector<double> &thresholds,
			     InputChanel *inputChanel, const vector<string> &localComments) {
  
  DEF_LOG ("OptChanel::addThresholds", "id:" << id << " inputChanel:" << *inputChanel << " treeType: " << treeType << " featureType: " << featureType << " attributeType: " << attributeType);
  vector <InputChanel *> &dedicatedChanel (chanels[AP]);
  for (InputChanel *apChanel : dedicatedChanel) {
    BOOST_ASSERT (apChanel->generateBy.size () == 1);
    if (apChanel->generateBy[0] == inputChanel &&
	apChanel->treeType == treeType &&
	apChanel->featureType == featureType &&
	apChanel->attributeType == attributeType) {
      apChanel->doWrite = doWrite;
      apChanel->thresholds = thresholds;
      apChanel->localComments = localComments;
      return;
    }
  }
  InputChanel *apChanel = new InputChanel (AP, id, doWrite, boost::assign::list_of (inputChanel));
  apChanel->treeType = treeType;
  apChanel->featureType = featureType;
  apChanel->attributeType = attributeType;
  apChanel->thresholds = thresholds;
  apChanel->localComments = localComments;
  dedicatedChanel.push_back (apChanel);
}

// ================================================================================
void
OptChanel::addDap (const bool &dapWeight, const ApOrigPos &apOrigPos) {
  for (InputChanel *inputChanel : selectedAP ()) {
    addDap (nextChanelUniqId++, true, dapWeight, apOrigPos, inputChanel);
    inputChanel->doWrite = false;
  }
}
void
OptChanel::addDap (const DimChanel &id, const DimChanel &from, const bool &doWrite, const bool &dapWeight, const ApOrigPos &apOrigPos) {
  DEF_LOG ("OptChanel::addDap", "id: " << id << " from: " << from << " dapWeight: " << dapWeight << " apOrigPos: " << apOrigPos);
  addDap (id, doWrite, dapWeight, apOrigPos, findChanel (from));
}
void
OptChanel::addDap (const DimChanel &id, const bool &doWrite, const bool &dapWeight, const ApOrigPos &apOrigPos, InputChanel *inputChanel) {
  DEF_LOG ("OptChanel::addDap", "inputChanel: " << *inputChanel);
  vector <InputChanel *> &dedicatedChanel (chanels[DAP]);
  for (InputChanel *dapChanel : dedicatedChanel) {
    BOOST_ASSERT (dapChanel->generateBy.size () == 1);
    if (dapChanel->generateBy[0] == inputChanel) {
      dapChanel->doWrite = doWrite;
      dapChanel->dapWeight = dapWeight;
      dapChanel->apOrigPos = apOrigPos;
      return;
    }
  }
  InputChanel *dapChanel = new InputChanel (DAP, id, doWrite, boost::assign::list_of (inputChanel));
  dapChanel->dapWeight = dapWeight;
  dapChanel->apOrigPos = apOrigPos;
  dedicatedChanel.push_back (dapChanel);
  LOG (*dapChanel);
}

// ================================================================================
void
OptChanel::addHaar (const vector<Size> &winSizes) {
  addTexture (Haar, winSizes);
}
void
OptChanel::addStat (const vector<Size> &winSizes) {
  addTexture (Stat, winSizes);
}
void
OptChanel::addTexture (const ChanelType &chanelType, const vector<Size> &winSizes) {
  for (InputChanel *inputChanel : selectedSet)
    addTexture (chanelType, nextChanelUniqId++, true, winSizes, inputChanel, vector<string> ());
}

void
OptChanel::addTexture (const ChanelType &chanelType, const DimChanel &id, const DimChanel &from, const bool &doWrite, const vector<string> &localComments,
		       const vector<Size> &winSizes) {
  // XXX test from exist
  addTexture (chanelType, id, doWrite, winSizes, findChanel (from), localComments);
}

void
OptChanel::addTexture (const ChanelType &chanelType, const DimChanel &id, const bool &doWrite, const vector<Size> &winSizes,
		       InputChanel *inputChanel, const vector<string> &localComments) {
  DEF_LOG ("OptChanel::addTexture", chanelType << " id:" << id << " inputChanel:" << *inputChanel);
  vector <InputChanel *> &dedicatedChanel (chanels[chanelType]);
  for (InputChanel *textChanel : dedicatedChanel) {
    BOOST_ASSERT (textChanel->generateBy.size () == 1);
    if (textChanel->generateBy[0] == inputChanel) {
      textChanel->doWrite = doWrite;
      textChanel->winSizes = winSizes;
      textChanel->localComments = localComments;
      return;
    }
  }
  InputChanel *textChanel = new InputChanel (chanelType, id, doWrite, boost::assign::list_of (inputChanel));
  textChanel->winSizes = winSizes;
  textChanel->localComments = localComments;
  dedicatedChanel.push_back (textChanel);
}

// ================================================================================
ostream &
OptChanel::printOutput (ostream &out) const {
  out << "    name: " << name << endl;
  out << " created: " << created << endl;
  out << "modified: " << modified << endl;
  out << "   image: " << chanels.size () << " * " << GDALGetDataTypeName (inputType) << endl << endl;
  // XXX format nombre
  for (map<DimChanel, InputChanel *>::const_iterator it = outputOrder.cbegin ();
       it != outputOrder.cend (); ++it) {
    InputChanel &inputChanel (*it->second);
    DimChanel outputRank (inputChanel.outputRank);
    if (!inputChanel.doWrite)
      continue;
    switch (inputChanel.chanelType) {
    case Original:
    case Ndvi:
    case Sobel:
      out << setw (4) << right << outputRank << ": " << inputChanel.chanelType;
      if (inputChanel.generateBy.size ()) {
	out << " (";
	string sep = "";
	for (InputChanel *parent : inputChanel.generateBy) {
	  out << sep << parent->chanelType << ":" << parent->typeRank;
	  sep = ", ";
	}
	out << ")";
      }
      out << endl;
      break;
    case AP:
      for (DimChanel chanel = 0; chanel < inputChanel.thresholds.size (); ++chanel)
	out << setw (4) << right << outputRank++ << ": " << inputChanel.chanelType << " " << inputChanel.treeType << "/" << inputChanel.featureType << "/" << inputChanel.attributeType << " " << inputChanel.thresholds[chanel] << endl;
      break;
    case DAP: {
      vector<DimChanel> dapIdx = inputChanel.getRelIdx ();
      InputChanel &parent (*inputChanel.generateBy[0]);
      vector<string> srcIdx (parent.thresholds.size ()+1);
      srcIdx [0] = chanelTypeLabels[parent.chanelType]+":"+boost::lexical_cast<std::string> (parent.typeRank);
      for (DimChanel chanel = 0; chanel < parent.thresholds.size (); ++chanel)
	srcIdx [chanel+1] = treeTypeLabels[parent.treeType] + "/" + featureTypeLabels[parent.featureType] + "/" + attributeLabels[parent.attributeType] + " " + boost::lexical_cast<std::string> (parent.thresholds[chanel]);

      for (DimChanel i = 0; i < dapIdx.size () -1; ++i) {
	out << setw (4) << right << outputRank++ << ": " << inputChanel.chanelType << "/" << inputChanel.apOrigPos << " ";
	if (inputChanel.dapWeight)
	  out << srcIdx [0] << " * ";
	out << "|" << inputChanel.chanelType << srcIdx [i+1] << " - " << srcIdx [i] << "|";
	out << endl;
      }
    }
      break;
    case Haar:
      for (Size size : inputChanel.winSizes) {
	out << setw (4) << right << outputRank++ << ": " << inputChanel.chanelType << " " << size.width << "x" << size.height << " hx" << endl;
	out << setw (4) << right << outputRank++ << ": " << inputChanel.chanelType << " " << size.width << "x" << size.height << " hx+hy" << endl;
	out << setw (4) << right << outputRank++ << ": " << inputChanel.chanelType << " " << size.width << "x" << size.height << " hy" << endl;
	out << setw (4) << right << outputRank++ << ": " << inputChanel.chanelType << " " << size.width << "x" << size.height << " hy-hx" << endl;
      }
      break;
    case Stat:
      for (Size size : inputChanel.winSizes) {
	out << setw (4) << right << outputRank++ << ": " << inputChanel.chanelType << " " << size.width << "x" << size.height << " mean" << endl;
	out << setw (4) << right << outputRank++ << ": " << inputChanel.chanelType << " " << size.width << "x" << size.height << " std" << endl;
	out << setw (4) << right << outputRank++ << ": " << inputChanel.chanelType << " " << size.width << "x" << size.height << " ent" << endl;
      }
      break;
    }
  }
  return out;
}

// ================================================================================
ostream &
OptChanel::printState (ostream &out) const {
  if (!modified.empty () || !name.empty ())
    out << modified << " " << name;
  if (spectralDepth)
    out << " spectralDepth: " << spectralDepth;
  out << (mixedBandFlag ? " mixedBand" : "") << endl
      << " (c:" << copyCount << " o:" << originalUsedCount << " d" << origDapUsedCount
      << " n:" << ndviUsedCount << " s:" << sobelUsedCount
      << " a:" << apCount << " d:" << dapCount
      << " II:" << integralImageCount << " dII:" << doubleIntegralImageCount
      << " h:" << haarCount << " st:" << statCount
      << " O:" << outputCount << ")"
      << endl;
  for (DimChanel chanelType = 0; chanelType < chanels.size (); ++chanelType) {
    const vector <InputChanel *> &dedicatedChanel (chanels[chanelType]);
    for (DimChanel i = 0; i < dedicatedChanel.size (); ++i)
      out << *dedicatedChanel[i] << endl;
  }
  return out;
}

// ================================================================================
ostream &
obelix::triskele::operator << (ostream &out, const OptChanel &optChanel) {
  return optChanel.printState (out);
  // XXX affichage des options
}

// ================================================================================
