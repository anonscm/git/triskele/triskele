#include <boost/algorithm/string.hpp>
#include <boost/assert.hpp>
#include <boost/filesystem.hpp>

using namespace std;

#include "IImage.hpp"
using namespace obelix;
#include "IImage.tcpp"


// ================================================================================
template<typename PixelT>
void
Raster<PixelT>::setSize (const Size &size) {
  DEF_LOG ("Raster::setSize",  "size: " << size);
  if (this->size == size)
    return;
  LOG ("Resize !!!");
  pixels.resize (size.getPixelsCount ());
  this->size = size;
}

// ================================================================================
template<typename PixelT>
Raster<PixelT>::Raster (const Size &size)
  : size (NullSize),
    pixels () {
  DEF_LOG ("Raster::Raster", "size: " << size);
  setSize (size);
}

template<typename PixelT>
Raster<PixelT>::Raster (const Raster &raster, const Rect &tile)
  : size (tile.size),
    pixels () {
  DEF_LOG ("Raster::Raster", "tile: " << tile);
  tile.cpToTile (raster.size, &raster.pixels[0], pixels);
}

template<typename PixelT>
Raster<PixelT>::~Raster () {
  DEF_LOG ("Raster::~Raster", "");
}

// ================================================================================
void
IImage::setFileName (string fileName) {
  DEF_LOG ("IImage::setFileName", "fileName: " << fileName);
  if (this->fileName == fileName)
    return;
  this->fileName = fileName;
  if (read)
    close ();
}

void
IImage::getGeo (string& projectionRef, vector<double> &geoTransform) const {
  projectionRef = this->projectionRef;
  geoTransform = this->geoTransform;
}

void
IImage::setGeo (const string& projectionRef, vector<double> geoTransform, const Point &topLeft) {
  DEF_LOG ("IImage::setGeo", "projectionRef:" << projectionRef);
  // move TopLeft cf WordFile
  double
    x (geoTransform[0]),
    xScale (geoTransform[1]),
    xSkew (geoTransform[2]),
    y (geoTransform[3]),
    ySkew (geoTransform[4]),
    yScale (geoTransform[5]);
  double
    xTrans = x + topLeft.x*xScale + topLeft.y*xSkew,
    yTrans = y + topLeft.x*ySkew + topLeft.y*yScale;
  geoTransform[0] = xTrans;
  geoTransform[3] = yTrans;
  CPLErr err = gdalOutputDataset->SetGeoTransform (&geoTransform[0]);
  if (err != CE_None)
    cerr << "IImage::setGeo: can't set geoTransform for " << fileName << endl;
  err = gdalOutputDataset->SetProjection (projectionRef.c_str ());
  if (err != CE_None)
    cerr << "IImage::setGeo: can't set projection for " << fileName << endl;
}

// ================================================================================
size_t
IImage::gdalCount = 0;

IImage::IImage (const string &imageFileName)
  : projectionRef (""),
    geoTransform (6, 0),
    fileName (imageFileName),
    size (NullSize),
    bandCount (0),
    mixedBandFlag (false),
    dataType (GDT_Unknown),
    gdalInputDataset (nullptr),
    gdalOutputDataset (nullptr),
    read (false)
{
  DEF_LOG ("IImage::IImage", "imageFileName: " << imageFileName);
}

IImage::~IImage () {
  close ();
}

// ================================================================================
void
IImage::readImage (const DimChanel &spectralDepth, const bool mixedBandFlag) {
  DEF_LOG ("IImage::readImage", "fileName: " << fileName << " spectralDepth: " << spectralDepth << " mixedBandFlag: " << mixedBandFlag);
  BOOST_ASSERT (gdalInputDataset == nullptr);
  BOOST_ASSERT (gdalOutputDataset == nullptr);
  close ();
  if (fileName.empty ())
    return;
  if (!gdalCount++)
    GDALAllRegister ();
  dataType = GDT_Unknown;
  gdalInputDataset = (GDALDataset *) GDALOpen (fileName.c_str (), GA_ReadOnly);
  if (!gdalInputDataset) {
    cerr << "GDALError: can't define dataset" << endl;
    return;
  }

  this->mixedBandFlag = mixedBandFlag;
  bandCount = gdalInputDataset->GetRasterCount ();
  BOOST_ASSERT (!spectralDepth || bandCount%spectralDepth == 0);
  size = Size (gdalInputDataset->GetRasterXSize (),
	       gdalInputDataset->GetRasterYSize (),
	       spectralDepth ? bandCount/spectralDepth : 1);
  if (spectralDepth)
    bandCount = spectralDepth;
  LOG ("size: " << size << " x " << bandCount);
  read = true;
  projectionRef = gdalInputDataset->GetProjectionRef ();
  CPLErr err = gdalInputDataset->GetGeoTransform (&geoTransform[0]);
  if (err != CE_None)
    cerr << "IImage::readImage: can't read geoTransform from " << fileName << endl;
  else
    LOG ("geoTransform: " << geoTransform [0] << " " << geoTransform [1] << " " <<  geoTransform [2] << " " <<  geoTransform [3] << " " <<  geoTransform [4] << " " <<  geoTransform [5]);

  for (DimChanel band = 0; band < bandCount; ++band) {
    GDALRasterBand &poBand = *gdalInputDataset->GetRasterBand (band+1);
    GDALDataType bandType = poBand.GetRasterDataType ();
    LOG ("band " << band << " " << GDALGetDataTypeName (bandType));
    if (dataType == GDT_Unknown) {
      dataType = bandType;
      continue;
    }
    if (dataType != bandType) {
      dataType = GDT_Unknown;
      LOG ("Can't parse inconsistant bands");
      // exit (1);
      return;
    }
  }
  LOG ("gdalCount: " << gdalCount);
}

// ================================================================================
void
IImage::createImage (const Size &size, const GDALDataType &dataType, const DimChanel &nbBands) {
  DEF_LOG ("IImage::createImage", "fileName: " << fileName << " size: " << size
	   << " dataType: " << GDALGetDataTypeName (dataType) << " nbBands: " << nbBands);
  BOOST_ASSERT (gdalInputDataset == nullptr);
  BOOST_ASSERT (gdalOutputDataset == nullptr);
  this->size = size;
  this->dataType = dataType;
  if (!gdalCount++)
    GDALAllRegister ();
  string fileExtension  = boost::filesystem::extension (fileName);
  boost::algorithm::to_lower (fileExtension);
  if (!boost::iequals (fileExtension, ".tif") &&
      !boost::iequals (fileExtension, ".tiff")) {
    cerr << "!!! Warning !!!" << endl
	 << "Output image not a TIF file <" << fileName << endl;
    BOOST_ASSERT (false);
  }
  GDALDriver *driverTiff = GetGDALDriverManager ()->GetDriverByName ("GTiff");
  remove (fileName.c_str ());
  bandCount = nbBands;
  gdalOutputDataset = driverTiff->Create (fileName.c_str (), size.width, size.height, size.length*nbBands, dataType, NULL);
  LOG("gdalCount: " << gdalCount);
}

void
IImage::createImage (const Size &size, const GDALDataType &dataType, const DimChanel &nbBands,
		     const IImage &inputImage, const Point &topLeft)
{
  createImage (size, dataType, nbBands);
  string projectionRef;
  vector<double> geoTransform;
  inputImage.getGeo (projectionRef, geoTransform);
  setGeo (projectionRef, geoTransform, topLeft);
}

// ================================================================================
void
IImage::close () {
  DEF_LOG ("IImage::close", "fileName: " << fileName);
  if (gdalOutputDataset) {
    GDALClose (gdalOutputDataset);
    gdalOutputDataset = nullptr;
    if (!--gdalCount)
      GDALDestroyDriverManager ();
  }
  if (gdalInputDataset) {
    GDALClose (gdalInputDataset);
    gdalInputDataset = nullptr;
    if (!--gdalCount)
      GDALDestroyDriverManager ();
  }
  BOOST_ASSERT (gdalCount >= 0);
  LOG ("gdalCount:" << gdalCount);
  read = false;
}

// ================================================================================
ostream &
obelix::operator << (ostream &out, const IImage &iImage) {
  out << iImage.getFileName ();
  if (!iImage.isRead ())
    return out;
  return out << ": " << iImage.getSize ()
	     << " (" << iImage.getBandCount () << " chanel(s) of " << GDALGetDataTypeName (iImage.getDataType ())  << ")";
}

// ================================================================================
template<typename PixelT>
void
IImage::readBand (Raster<PixelT> &raster, const DimChanel &band) const {
  DEF_LOG ("IImage::readBand", "band: " << band);
  readBand (raster, band, NullPoint, size);
}

template<typename PixelT>
void
IImage::readBand (Raster<PixelT> &raster, DimChanel band, const Point &cropOrig, const Size &cropSize) const {
  DEF_LOG ("IImage::readBand",  "band: " << band << " crop: " << cropOrig << " - " << cropSize);
  raster.setSize (cropSize);
  for (DimChanel z = 0; z < size.length; ++z)
    readFrame (raster.getFrame (z), mixedBandFlag ? (band+z*size.length) : (band*size.length+z), cropOrig, cropSize);
}

template<typename PixelT>
void
IImage::readFrame (PixelT *pixels, DimChanel band, const Point &cropOrig, const Size &cropSize) const {
  DEF_LOG ("IImage::readBand",  "band: " << band << " crop: " << cropOrig << " - " << cropSize);
  BOOST_ASSERT (gdalInputDataset);
  band++; // !!! GDAL bands start at 1 (not 0 :-( )
  GDALRasterBand &poBand = *gdalInputDataset->GetRasterBand (band);
  GDALDataType bandType = poBand.GetRasterDataType ();
  CPLErr err = poBand.RasterIO (GF_Read, cropOrig.x, cropOrig.y, cropSize.width, cropSize.height,
				pixels, cropSize.width, cropSize.height, bandType, 0, 0);
  if (err != CE_None)
    cerr << "IImage::readBand: can't acces " << fileName << endl;
}

// ================================================================================
template<typename PixelT>
void
IImage::writeBand (PixelT *pixels, DimChanel band) const {
  for (DimChanel z = 0; z < size.length; ++z)
    writeFrame (pixels+size.getPixelsCount ()/size.length*z, band*size.length+z);
}

template<typename PixelT>
void
IImage::writeFrame (PixelT *pixels, DimChanel band) const {
  DEF_LOG ("IImage::writeBand", "band: " << band << " size:" << size << " " << GDALGetDataTypeName (dataType));
  BOOST_ASSERT (gdalOutputDataset);
  band++; // !!! GDAL layers starts at 1 (not 0 :-( )
  GDALRasterBand &poBand = *gdalOutputDataset->GetRasterBand (band);
  CPLErr err = poBand.RasterIO (GF_Write, 0, 0, size.width, size.height,
				pixels, size.width, size.height, dataType, 0, 0);
  if (err != CE_None)
    cerr << "IImage::writeFrame: can't acces " << fileName << endl;
}

void
IImage::writeDoubleBand (double *pixels, DimChanel band) const {
  for (DimChanel z = 0; z < size.length; ++z)
    writeDoubleFrame (pixels+size.getPixelsCount ()/size.length*z, band*size.length+z);
}

void
IImage::writeDoubleFrame (double *pixels, DimChanel band) const {
  // for (DimChanel z = 0; z < size.length; ++z)
  //   writeFrame (pixels+size.getPixelsCount ()/size.length*z, band*size.length+z)
  DEF_LOG ("IImage::writeDoubleBand", "band: " << band << " size:" << size);
  BOOST_ASSERT (gdalOutputDataset);
  band++; // !!! GDAL layers starts at 1 (not 0 :-( )
  GDALRasterBand &poBand = *gdalOutputDataset->GetRasterBand (band);
  CPLErr err = poBand.RasterIO (GF_Write, 0, 0, size.width, size.height, pixels, size.width, size.height, GDT_Float64, 0, 0);
  if (err != CE_None)
    cerr << "IImage::writeDoubleFrame: can't acces " << fileName << endl;
}

void
IImage::writeDimImgBand (DimImg *pixels, DimChanel band) const {
  for (DimChanel z = 0; z < size.length; ++z)
    writeDimImgFrame (pixels+size.getPixelsCount ()/size.length*z, band*size.length+z);
}

void
IImage::writeDimImgFrame (DimImg *pixels, DimChanel band) const {
  // for (DimChanel z = 0; z < size.length; ++z)
  //   writeFrame (pixels+size.getPixelsCount ()/size.length*z, band*size.length+z)
  DEF_LOG ("IImage::writeDimImgFrame", "band: " << band << " size:" << size);
  BOOST_ASSERT (gdalOutputDataset);
  band++; // !!! GDAL layers starts at 1 (not 0 :-( )
  GDALRasterBand &poBand = *gdalOutputDataset->GetRasterBand (band);
  CPLErr err = poBand.RasterIO (GF_Write, 0, 0, size.width, size.height, pixels, size.width, size.height, GDT_UInt32, 0, 0);
  if (err != CE_None)
    cerr << "IImage::writeDimImgFrame: can't acces " << fileName << endl;
}

// ================================================================================
