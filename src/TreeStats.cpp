#include <boost/chrono.hpp>
#include <iomanip>

#include "misc.hpp"
#include "TreeStats.hpp"

using namespace obelix::triskele;
using namespace std;
  
// ================================================================================
TreeStats TreeStats::global;

const string TreeStats::timeTypeLabels [] = {
  "border",
  "NDVI",
  "sobel",
  "IntegralImages",

  "build tree",
  "  setup  ",
  "  to tiles  ",
  "  sort edges  ",
  "  parents  ",
  "  from tiles  ",
  "  merge  ",
  "  forest mgt. ",
  "  index  ",
  "  compress  ",
  "  children  ",

  "set AP",
  "area",
  "average",
  "bb",
  "xyz",
  "moi",
  "sd",
  "filtering",
  "copy image",
  "GDAL read",
  "GDAL write",
  "sum bands",
  
  "Learning",
  "clean Fg/Bg",
  "Check Acc.",
  "Prediction",

  "*** OUT OF TimeType ***"
};

// ================================================================================
TreeStats::TreeStats ()
  : leavesStats (ALPHA+1),
    compStats (ALPHA+1),
    timeStats (TimeTypeCard) {
  reset ();
    }

void
TreeStats::reset () {
  for (unsigned int i = MIN; i <= ALPHA; ++i) {
    leavesStats[i] = TreeStatsDim ();
    compStats[i] = TreeStatsDim ();
  }
  for (unsigned int i = 0; i < TimeTypeCard; ++i)
    timeStats[i] = TreeStatsDouble ();
}

// ================================================================================
TreeStats::CPrintDim::CPrintDim (const TreeStats &treeStats)
  : treeStats (treeStats) {
}

ostream &
TreeStats::CPrintDim::print (ostream &out) const {
  bool empty = true;
  for (unsigned int i = MIN; i <= ALPHA; ++i)
    if (!(empty = !ba::count (treeStats.leavesStats[i])))
      break;
  if (empty)
    return out;
  print (out, treeStats.leavesStats, "Leaf");
  return print (out, treeStats.compStats, "Comp");
}

ostream &
TreeStats::CPrintDim::print (ostream &out, const vector<TreeStatsDim> stats, const string &msg) const {
  out << endl
      << setw (11) << left << msg  << "\t"
      << setw (3)  << left << "Count" << "\t"
      << setw (9) << left << "Mean"  << "\t"
      << setw (9) << left << "Min"  << "\t"
      << setw (9) << left << "Max"  << endl;
  for (unsigned int i = 0; i <= ALPHA; ++i) {
    if (!ba::count (stats[i]))
      continue;
    out << setw (11) << right << treeTypeLabels [i] << "\t"
	<< setw (3) << ba::count (stats[i]) << "\t"
	<< setw (9) << DimImg (ba::mean (stats[i])) << "\t"
	<< setw (9) << ba::min (stats[i])  << "\t"
	<< setw (9) << ba::max (stats[i])
	<< endl << flush;
  }
  return out;
}

TreeStats::CPrintDim
TreeStats::printDim () const {
  return CPrintDim (*this);
}
// ostream&
// operator << (ostream& out, const TreeStats::CPrintDim &cpd) {
//   return cpd.print (out);
// }


// ================================================================================
TreeStats::CPrintTime::CPrintTime (const TreeStats &treeStats)
  : treeStats (treeStats) {
}
ostream &
TreeStats::CPrintTime::print (ostream &out) const {
  bool empty = true;
  for (unsigned int i = 0; i < TimeTypeCard; ++i)
    if (!(empty = !ba::count (treeStats.timeStats[i])))
      break;
  if (empty)
    return out;
  out << endl
      << setw (16) << left << "Time"  << "\t"
      << setw (15) << left << "Sum"   << "\t"
      << setw (3)  << left << "Count" << "\t"
      << setw (15) << left << "Mean"  << "\t"
      << setw (15) << left << "Min"  << "\t"
      << setw (15) << left << "Max"  << endl;
  for (unsigned int i = 0; i < TimeTypeCard; ++i) {
    if (!ba::count (treeStats.timeStats[i]))
      continue;
    out << setw (16) << right << timeTypeLabels[i] << "\t"
	<< ns2string (ba::sum (treeStats.timeStats[i]))  << "\t" << setw (3) << ba::count (treeStats.timeStats[i]) << "\t"
	<< ns2string (ba::mean (treeStats.timeStats[i])) << "\t"
	<< ns2string (ba::min (treeStats.timeStats[i]))  << "\t"
	<< ns2string (ba::max (treeStats.timeStats[i]))
	<< endl << flush;
  }
  return out;
}

TreeStats::CPrintTime
TreeStats::printTime () const {
  return CPrintTime (*this);
}
// ostream&
// operator << (ostream& out, const TreeStats::CPrintTime &cpt) {
//   return cpt.print (out);
// }

// ================================================================================
