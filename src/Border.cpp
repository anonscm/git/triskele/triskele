#include <boost/assert.hpp>


#include "Border.hpp"
using namespace obelix;

// ================================================================================
DimImg
Border::getMapLength (const DimImg &pixelsCount) {
  return (pixelsCount+7)/8;
}

// ================================================================================
bool
Border::exists () const {
  return map.size ();
}

// ================================================================================
void
Border::clearBorder (const DimImg &idx) {
  if (!map.size ())
    createMap ();
  BOOST_ASSERT (idx < pixelsCount);
  map[idx/8] &= ~(1 << (idx%8));
}

void
Border::clearBorder (const Point &p) {
  clearBorder (point2idx (size, p));
}

// ================================================================================
void
Border::setBorder (const DimImg &idx) {
  if (!map.size ())
    createMap ();
  BOOST_ASSERT (idx < pixelsCount);
  map[idx/8] |= (1 << (idx%8));
}

void
Border::setBorder (const Point &p) {
  setBorder (point2idx (size, p));
}

const Size &
Border::getSize () const {
  return size;
}

// ================================================================================
Border::Border ()
  : pixelsCount (0),
    mapLength (0),
    size (),
    map (),
    defaultVal (false) {
}

Border::Border (const Size &size, bool defaultVal)
  : pixelsCount (size.getPixelsCount ()),
    mapLength (getMapLength (pixelsCount)),
    size (size),
    map (),
    defaultVal (defaultVal) {
    }

Border::Border (const Border &border, const Rect &tile)
  : Border (tile.size, border.defaultVal) {
  if (!border.map.size ())
    return;
  // XXX todo
  // width >= 8 => for bytes
  // width < 8 => for bits
  BOOST_ASSERT (false);
}

Border::~Border () {
}

// ================================================================================
void
Border::reset (bool defaultVal) {
  this->defaultVal = defaultVal;
  map.clear ();
}

void
Border::reduce () {
  DimImg count = borderCount ();
  if (!count)
    reset (false);
  else if (count == pixelsCount)
    reset (true);
}

DimImg
Border::borderCount () const {
  DimImg result (0);
  for (DimImg i = 0; i < mapLength; ++i)
    result += bitCount[map[i]];
  return result;
}

void
Border::createMap () {
  map.assign (mapLength, defaultVal ? 0xFF : 0);
  if (!(mapLength && defaultVal))
    return;
  map[mapLength-1] &= 0xFF >> (8-(pixelsCount%8))%8;
}

ostream &
obelix::operator << (ostream &out, const Border &border) {
  if (border.size.width > printMapMaxSide || border.size.height > printMapMaxSide || border.size.length > printMapMaxSide) {
    return out << "map too big to print!" << std::endl;
  }
  for (DimSideImg t = 0, idx = 0; t < border.size.length; ++t) {
    for (DimSideImg y = 0; y < border.size.height; ++y) {
      for (DimSideImg x = 0; x < border.size.width; ++x, ++idx)
	out << (border.isBorder (idx) ? "  B " : "  - ");
      out << endl;
    }
    out << endl;
  }
  return out;
}

// ================================================================================
