
#include "ArrayTree/GraphWalker.hpp"
using namespace obelix;
using namespace obelix::triskele;

// ================================================================================
GraphWalker::GraphWalker (const Border &border, const Connectivity &connectivity)
  : border (border),
    connectivity (connectivity) {
  DEF_LOG ("GraphWalker::GraphWalker", "size: " << border.getSize () << " connectivity: " << connectivity);
  BOOST_ASSERT (!border.getSize ().isNull ());
}

// ================================================================================
const Size &
GraphWalker::getSize () const {
  return border.getSize ();
}

DimEdge
GraphWalker::edgeMaxCount () const {
  return edgeMaxCount (border.getSize ());
}

// ================================================================================
DimEdge
GraphWalker::edgeMaxCount (const Size &tileSize) const {
  if (tileSize.isNull ())
    return 0;
  const DimImg wt (tileSize.width);
  const DimImg wp (tileSize.width-1);
  const DimImg ht (tileSize.height);
  const DimImg hp (tileSize.height-1);
  const DimImg lt (tileSize.length);
  const DimImg lp (tileSize.length-1);
  DimEdge plan (0);
  if (connectivity & Connectivity::C4)
    plan += wt*hp + wp*ht;
  if (connectivity & Connectivity::C6P)
    plan += wp*hp;
  if (connectivity & Connectivity::C6N)
    plan += wp*hp;
  plan *= lt;

  DimEdge deep (0);
  if (connectivity & Connectivity::CT)
    deep += wt*ht;
  if (connectivity & Connectivity::CTP)
    deep += 2*(wt*hp + wp*ht);
  if (connectivity & Connectivity::CTN)
    deep += 4*wp*hp;
  deep *= lp;
  return plan+deep;
}

DimEdge
GraphWalker::edgeBoundarySideMaxCount (const TileShape &boundaryAxe, const Size &size) const {
  DEF_LOG ("GraphWalker::edgeBoundarySideMaxCount", "boundaryAxe:" << boundaryAxe << " size:" << size);
  DimEdge result (0);
  if (size.isNull ()) {
    LOG ("null: " << size);
    return result;
  }
  switch (boundaryAxe) {
  case Vertical:
    if (connectivity & Connectivity::C4)
      result += size.height * size.length;
    if (connectivity & Connectivity::C6P)
      result += (size.height-1) * size.length;
    if (connectivity & Connectivity::C6N)
      result += (size.height-1) * size.length;
    if (connectivity & Connectivity::CTP)
      result += 2 * size.height * (size.length-1);
    if (connectivity & Connectivity::CTN)
      result += 4 * (size.height-1) * (size.length-1);
    break;

  case Horizontal:
    if (connectivity & Connectivity::C4)
      result += size.width * size.length;
    if (connectivity & Connectivity::C6P)
      result += (size.width-1) * size.length;
    if (connectivity & Connectivity::C6N)
      result += (size.width-1) * size.length;
    if (connectivity & Connectivity::CTP)
      result += 2 * size.width * (size.length-1);
    if (connectivity & Connectivity::CTN)
      result += 4 * (size.width-1) * (size.length-1);
    break;

  case Plane:
    if (connectivity & Connectivity::CT)
      result += size.width*size.height;
    if (connectivity & Connectivity::CTP)
      result +=
	2 * ((size.width-1) * size.height +
	     size.width * (size.height-1));
    if (connectivity & Connectivity::CTN)
      result += 4 * (size.width-1) * (size.height-1);
    break;

  default:
    BOOST_ASSERT (false);
  }
  LOG ("result: " << result);
  return result;
}

// ================================================================================
void
GraphWalker::setTiles (const unsigned int &coreCount, vector<Rect> &tiles, vector<Rect> &boundaries, vector<TileShape> &boundaryAxes) const {
  setTiles (coreCount, Rect (NullPoint, border.getSize ()), tiles, boundaries, boundaryAxes);
}

void
GraphWalker::setTiles (const unsigned int &coreCount, const Rect &rect, vector<Rect> &tiles, vector<Rect> &boundaries, vector<TileShape> &boundaryAxes) const {
  // 
  BOOST_ASSERT (coreCount);
  DEF_LOG ("GraphWalker::setTiles", "coreCount:" << coreCount << " rect:" << rect);
  DimSideImg maxSide = max (max (rect.size.width, rect.size.height), DimSideImg (rect.size.length));
  if (coreCount < 2 ||
      maxSide < 4 ||
      rect.size.getPixelsCount () < 12) {
    LOG ("rect:" << rect);
    tiles.push_back (rect);
    return;
  }
  TileShape cutAxe = Vertical;
  if (maxSide == rect.size.width)
    cutAxe = Vertical;
  else if (maxSide == rect.size.height)
    cutAxe = Horizontal;
  else if (maxSide == rect.size.length)
    cutAxe = Plane;
  bool odd = coreCount & 0x1;
  DimImg thin = maxSide / (odd ? coreCount : 2);
  Rect tileA (rect);
  Rect tileB (rect);
  switch (cutAxe) {
  case Vertical:
    tileA.size.width = thin;
    tileB.point.x += thin;
    tileB.size.width -= thin;
    break;
  case Horizontal:
    tileA.size.height = thin;
    tileB.point.y += thin;
    tileB.size.height -= thin;
    break;
  case Plane:
    tileA.size.length = thin;
    tileB.point.z += thin;
    tileB.size.length -= thin;
    break;
  default:
    BOOST_ASSERT (false);
  }
  setTiles ((odd ? 1 : coreCount/2), tileA, tiles, boundaries, boundaryAxes);
  boundaries.push_back (tileB);
  boundaryAxes.push_back (cutAxe);
  setTiles ((odd ? coreCount-1 : coreCount/2), tileB, tiles, boundaries, boundaryAxes);
}

// ================================================================================
void
GraphWalker::setMaxBounds (const vector<Rect> &tiles, vector<DimImg> &vertexMaxBounds, vector<DimImg> &edgesMaxBounds) const {
  unsigned int tileCount = tiles.size ();
  vertexMaxBounds.resize (tileCount+1);
  edgesMaxBounds.resize (tileCount+1);
  DimImg vertexBase = 0, edgesBase = 0;
  vertexMaxBounds [0] = edgesMaxBounds [0] = 0;
  for (unsigned int i = 0; i < tileCount; ++i) {
    Size zoneSize (tiles[i].size);
    vertexMaxBounds [i+1] = vertexBase += zoneSize.getPixelsCount (); /* -1, but prety LOG */
    edgesMaxBounds [i+1] = edgesBase += edgeMaxCount (zoneSize);
  }
}

// ================================================================================
