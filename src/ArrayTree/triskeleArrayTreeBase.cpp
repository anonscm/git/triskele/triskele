#include <boost/algorithm/string/join.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/assign.hpp>
#include <sstream>
#include <string>

#include "ArrayTree/triskeleArrayTreeBase.hpp"

using namespace std;
using namespace obelix::triskele;

// ========================================
const string
obelix::triskele::tileShapeLabels[] = {
  "Surface", "Horizontal", "Vertical", "Plane"
};
const map<string, TileShape> 
obelix::triskele::tileShapeMap = boost::assign::map_list_of
  ("volume", Volume)
  ("horizontal", Horizontal)
  ("vertical", Vertical)
  ("Plane", Plane)
  ;

const string
obelix::triskele::treeTypeLabels[] = {
  "Min", "Max", "ToS", "Alpha"
};
const map<string, TreeType>
obelix::triskele::treeTypeMap = boost::assign::map_list_of
  ("min", MIN)
  ("max", MAX)
  ("tos", TOS)
  ("alpha", ALPHA)
  ;

// ========================================
ostream &
obelix::triskele::operator << (ostream &out, const Connectivity &c) {
  BOOST_ASSERT (c >= C1 && c <= (C8|CT|CTP|CTN));
  vector<string> result;
  if (C8 & (c == C8))
    result.push_back ("C8");
  else {
    if (C4 & c)
      result.push_back ("C4");
    if (C6P & c)
      result.push_back ("C6P");
    else if (C6N & c)
      result.push_back ("C6N");
  }
  if (CT & c)
    result.push_back ("CT");
  if (CTP & c)
    result.push_back ("CTP");
  if (CTN & c)
    result.push_back ("CTN");
  return out << boost::algorithm::join (result, "|");
}

istream &
obelix::triskele::operator >> (istream &in, Connectivity &c) {
  string token;
  in >> token;
  std::istringstream buffer (token);
  c = C1;
  for (std::string item; std::getline (buffer, item, '|'); ) {
    if (item == "C4")
      c = Connectivity (c|C4);
    else if (item == "C6P")
      c = Connectivity (c|C6P);
    else if (item == "C6N")
      c = Connectivity (c|C6N);
    else if (item == "C8")
      c = Connectivity (c|C8);
    else if (item == "CT")
      c = Connectivity (c|CT);
    else if (item == "CTP")
      c = Connectivity (c|CTP);
    else if (item == "CTN")
      c = Connectivity (c|CTN);
    else
      in.setstate (ios_base::failbit);
  }
  return in;
}

ostream &
obelix::triskele::operator << (ostream &out, const TileShape &tileShape) {
  BOOST_ASSERT (tileShape >= Volume && tileShape <= Plane);
  return out << tileShapeLabels[tileShape];
}

istream &
obelix::triskele::operator >> (istream &in, TileShape &tileShape) {
  string token;
  in >> token;
  auto pos = tileShapeMap.find (boost::algorithm::to_lower_copy (token));
  if (pos == tileShapeMap.end ())
    in.setstate (ios_base::failbit);
  else
    tileShape = pos->second;
  return in;
}

ostream &
obelix::triskele::operator << (ostream &out, const TreeType &treeType) {
  BOOST_ASSERT (treeType >= MIN && treeType <= ALPHA);
  return out << treeTypeLabels[treeType];
}
istream &
obelix::triskele::operator >> (istream &in, TreeType &treeType) {
  string token;
  in >> token;
  auto pos = treeTypeMap.find (boost::algorithm::to_lower_copy (token));
  if (pos == treeTypeMap.end ())
    in.setstate (ios_base::failbit);
  else
    treeType = pos->second;
  return in;
}

// ========================================
