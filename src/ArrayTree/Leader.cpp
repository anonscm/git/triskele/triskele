
#include "ArrayTree/Leader.hpp"

using namespace obelix;
using namespace obelix::triskele;

// ================================================================================
Leader::Leader (DimImg vertexCount)
  : size (vertexCount),
    leaders (size, DimImg_MAX) {
}

Leader::~Leader () {
  free ();
}

// ================================================================================
void
Leader::book (DimImg vertexCount) {
  SMART_DEF_LOG ("Leader::book", "vertexCount:" << vertexCount);
  size = vertexCount;
  leaders.assign (size, DimImg_MAX);
}

void
Leader::free () {
  SMART_DEF_LOG ("Leader::free", "");
  leaders = vector<DimImg> ();
  size = 0;
}

// ================================================================================
DimImg *
Leader::getLeaders () {
  return &leaders[0];
}

vector<DimImg> &
Leader::getLeadersVector () {
  return leaders;
}

// ================================================================================
