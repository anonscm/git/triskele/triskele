#include <boost/algorithm/string.hpp>
#include <boost/assign.hpp>

#include "obelixDebug.hpp"
#include "obelixThreads.hpp"
#include "CompAttribute.hpp"

#include "triskeleBase.hpp"
#include "Attributes/XYAttributes.hpp"

using namespace obelix;
using namespace obelix::triskele;
#include "CompAttribute.tcpp"

// ================================================================================
const string obelix::triskele::attributeLabels[] = {
  "Area", "Weight", "SD", "MoI"
};
const map<string, AttributeType>
obelix::triskele::attributeMap = boost::assign::map_list_of
  ("area", AArea)
  ("weight", AWeight)
  ("sd", ASD)
  ("moi", AMoI)
  ;

ostream &
obelix::triskele::operator << (ostream &out, const AttributeType &attributeType) {
  BOOST_ASSERT (attributeType >= AArea && attributeType <= AMoI);
  return out << attributeLabels[attributeType];
}
istream &
obelix::triskele::operator >> (istream &in, AttributeType &attributeType) {
  string token;
  in >> token;
  auto pos = attributeMap.find (boost::algorithm::to_lower_copy (token));
  if (pos == attributeMap.end ())
    in.setstate (ios_base::failbit);
  else
    attributeType = pos->second;
  return in;
}

// ================================================================================
const string
obelix::triskele::featureTypeLabels[] = {
  "AP", "Area", "Mean", "SD", "MoI"
};
const map<string, FeatureType>
obelix::triskele::featureTypeMap = boost::assign::map_list_of
  ("ap", FAP)
  ("area", FArea)
  ("mean", FMean)
  ("SD", FSD)
  ("moi", FMoI)
  ;

ostream &
obelix::triskele::operator << (ostream &out, const FeatureType &featureType) {
  BOOST_ASSERT (featureType >= FAP && featureType <= FMoI);
  return out << featureTypeLabels[featureType];
}
istream &
obelix::triskele::operator >> (istream &in, FeatureType &featureType) {
  string token;
  in >> token;
  auto pos = featureTypeMap.find (boost::algorithm::to_lower_copy (token));
  if (pos == featureTypeMap.end ())
    in.setstate (ios_base::failbit);
  else
    featureType = pos->second;
  return in;
}

// ========================================
template<typename AttrT>
vector<AttrT>
CompAttribute<AttrT>::getScaledThresholds (const vector<double> &thresholds, const AttrT &maxValue) {
  vector<AttrT> result;
  for (double percent : thresholds)
    result.push_back (percent*maxValue);
  return result;
}

template<typename AttrT>
vector<AttrT>
CompAttribute<AttrT>::getConvertedThresholds (const vector<double> &thresholds) {
  vector<AttrT> result;
  for (double value : thresholds)
    result.push_back ((AttrT) value);
  return result;
}

// ========================================
template<typename AttrT>
CompAttribute<AttrT>::CompAttribute (const Tree &tree, const bool &decr)
  : tree (tree),
    leafCount (0),
    values (0),
    decr (decr)
{
  DEF_LOG ("CompAttribute::CompAttribute", "decr: " << decr);
  updateTranscient ();
}

template<typename AttrT>
CompAttribute<AttrT>::~CompAttribute () {
  DEF_LOG ("CompAttribute::~CompAttribute", "");
}

template<typename AttrT>
void
CompAttribute<AttrT>::updateTranscient () {
  DEF_LOG ("CompAttribute::updateTranscient", "");
  // XXX max : leafCount-1
  book (tree.getLeafCount ());
}

template<typename AttrT>
const AttrT *
CompAttribute<AttrT>::getValues () const {
  return &values[0];
}

template<typename AttrT>
AttrT *
CompAttribute<AttrT>::getValues () {
  return &values[0];
}

template<typename AttrT>
AttrT
CompAttribute<AttrT>::getMaxValue () const {
  if (!leafCount)
    return 0;
  AttrT max = values[0];
  CompAttribute<AttrT>::tree.forEachComp ([this, &max] (const DimImg &compId) {
      if (values[compId] > max)
	max = values[compId];
    });
  return max;
}

// ========================================
template<typename AttrT>
template<typename PixelT>
void
CompAttribute<AttrT>::cut (vector<vector<PixelT> > &allBands, const AttributeProfiles<PixelT> &attributeProfiles,
			   const AttrT &pixelAttrValue, const vector<AttrT> &thresholds) const {
  DEF_LOG ("CompAttribute::cut", "coreCount:" << CompAttribute<AttrT>::tree.getCoreCount () << " thresholds:" << thresholds.size ());
  dealThread (CompAttribute<AttrT>::leafCount, CompAttribute<AttrT>::tree.getCoreCount (), [this, &allBands, &attributeProfiles, &pixelAttrValue, &thresholds] (const DimCore &threadId, const DimImg &minVal, const DimImg &maxVal) {
      for (DimImg leafId = minVal; leafId < maxVal; ++leafId)
	cutOnPos (allBands, attributeProfiles, leafId, pixelAttrValue, thresholds);
    });
}

template<typename AttrT>
template<typename PixelT>
void
CompAttribute<AttrT>::cutOnPos (vector<vector<PixelT> > &allBands, const AttributeProfiles<PixelT> &attributeProfiles,
				const DimImg &leafId, const AttrT &pixelAttrValue, const vector<AttrT> &thresholds) const {
  // no debug (to many pixels)
  DimImg parentId = CompAttribute<AttrT>::tree.getLeafParent (leafId);
  DimChanel thresholdsSize = thresholds.size ();
  if (parentId == DimImg_MAX) {
    // border case
    for (DimChanel chanel = 0; chanel < thresholdsSize; ++chanel)
      allBands[chanel][leafId] = 0;
    return;
  }
  DimNodeId nodeId = leafId;
  DimImg curId = 0; // XXX OK sauf pour le premier parent
  AttrT curValue = pixelAttrValue;
  if (curValue == CompAttribute<AttrT>::values [parentId]) {
    // skip pixel on flat zone
    curId = parentId;
    nodeId = ((DimNodeId)curId)+CompAttribute<AttrT>::leafCount;
    parentId = CompAttribute<AttrT>::tree.getCompParent (curId);
  }
  const PixelT *apValues = attributeProfiles.getValues ();
  DimImg rootId = CompAttribute<AttrT>::tree.getCompRoot ();
  for (DimChanel chanel = 0; chanel < thresholdsSize; ++chanel) {
    AttrT ceil = thresholds[chanel];
    for ( ; curId < rootId; ) {
      if (decr) {
	if (curValue < ceil)
	  break;
      } else {
	if (curValue > ceil)
	  break;
      }
      if (parentId == DimImg_MAX || (curId >= parentId && curId != 0)) {
	// cerr << "CompAttribute::cutOnPos find sub-root:" << rootId << " rootId:" << rootId << endl;
	// find root
	for (; chanel < thresholdsSize; ++chanel)
	  allBands[chanel][leafId] = curValue;
	return;
      }
      nodeId = ((DimNodeId) CompAttribute<AttrT>::tree.getLeafParent (nodeId))+CompAttribute<AttrT>::leafCount;
      curId = parentId;
      curValue = CompAttribute<AttrT>::values [curId];
      parentId = CompAttribute<AttrT>::tree.getCompParent (curId);
    }
    // XXX si valeur > root ?
    allBands[chanel][leafId] = apValues [nodeId];
  }
}

// ========================================
template<typename AttrT>
void
CompAttribute<AttrT>::free () {
  values = vector<AttrT> ();
}

template<typename AttrT>
void
CompAttribute<AttrT>::book (const DimImg &leafCount) {
  this->leafCount = leafCount;
  values.resize (leafCount);
}

// ================================================================================
