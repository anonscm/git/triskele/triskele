#!/bin/bash

cd `dirname $0`

while read line
do
    # ignore comment
    if test -z "${line}"
    then
	continue
    fi
    case "${line}" in \#*|//*) continue ;; esac

    # DimImgT class case
    if echo "${line}" | grep -q "DimImgT"
    then
	sizeTypes="DimImg size_t"
	for size in ${sizeTypes}
	do
	    echo "${line}" | sed "s/DimImgT/${size}/g"
	done
	echo
	continue
    fi

    # AttT class case
    if echo "${line}" | grep -q "AttrT"
    then
	attTypes="uint8_t uint16_t int16_t uint32_t int32_t float double"
	if ! echo "${line}" | grep -q "OnlyAttrTScaleType"
	then
	    attTypes="${attTypes} AverageXY"
	fi
	for att in ${attTypes}
	do
	    if echo "${line}" | grep -q "PixelT"
	    then
		for pixel in uint8_t uint16_t int16_t uint32_t int32_t float double
		do
		    echo "${line}" | sed "s/AttrT/${att}/g;s/PixelT/${pixel}/g"
		done
	    else
		echo "${line}" | sed "s/AttrT/${att}/g"
	    fi
	done
	echo
	continue
    fi
    
    # WeightT class case
    if echo "${line}" | grep -q "WeightT"
    then
	for weight in uint8_t uint16_t int16_t uint32_t int32_t float double # PixelT DimImg double
	do
	    if echo "${line}" | grep -q "PixelT"
	    then
		for pixel in uint8_t uint16_t int16_t uint32_t int32_t float double
		do
		    echo "${line}" | sed "s/WeightT/${weight}/g;s/PixelT/${pixel}/g"
		done
	    else
		echo "${line}" | sed "s/WeightT/${weight}/g;"
	    fi
	done
	echo
	continue
    fi

    # PixelT class case
    for pixel in uint8_t uint16_t int16_t uint32_t int32_t float double
    do
	if echo "${line}" | grep -q "PixelGT"
	then
	    for gt in uint8_t uint16_t int16_t uint32_t int32_t float double
	    do
		echo "${line}" | sed "s/PixelT/${pixel}/g;s/PixelGT/${gt}/g"
	    done
	else
	    echo "${line}" | sed "s/PixelT/${pixel}/g"
	fi
    done
    echo

done  < $1.mcpp > $1.tcpp
