// faire test 10000 + lamba + 1...coreCount
#include <boost/chrono.hpp>
#include <boost/thread.hpp>

#include <pthread.h>
#include <tbb/tbb.h>
#include <boost/thread.hpp>

//tbb_allocator<T>
//std::vector<int,cache_aligned_allocator<int> >;

#include "obelixThreads.hpp"
#include "TestThread.hpp"

using namespace std;
using namespace boost::chrono;
using namespace obelix;

/**
   test memory access

   * 0 : local += 1+x
   * 1 : local += 1+localVect[x]
   * 2 : local += 1+commonVect[x]

   * 3 : localVect[x] += 1+x
   * 4 : localVect[x] += 1+localVect[x]
   * 5 : localVect[x] += 1+commonVect[x]

   * 6 : commonVect[x] += 1+x
   * 7 : commonVect[x] += 1+localVect[x]
   * 8 : commonVect[x] += 1+commonVect[x]

   * 9  : commonVect[0] += 1+x
   * 10 : commonVect[0] += 1+localVect[x]
   * 11 : commonVect[0] += 1+commonVect[x]

   vector or *
*/

inline
void
bodyTest (
#if ((TEST_NUM %3) == 2) || (TEST_NUM > 5)
	  vector<DimImg> &commonVect,
#endif
#if ((TEST_NUM %3) == 1) || ((TEST_NUM > 2) && (TEST_NUM < 6))
	  vector<DimImg> &localVect,
#endif
	  const DimImg &nbItem
	  ) {
#if TEST_NUM < 3
  DimImg sum (0);
#endif

  for (DimImg x = 0; x < nbItem; ++x)
#if TEST_NUM < 3
    sum
#elif TEST_NUM < 6
    localVect[x]
#elif TEST_NUM < 9
    commonVect[x]
#else
    commonVect[0]
#endif
  += 1+
#if (TEST_NUM % 3) == 0
  x;
#elif (TEST_NUM % 3) == 1
  localVect[x];
#else
  commonVect[x];
#endif
}

inline void
fThreadTTB (const std::size_t &coreCount,
	    vector<DimImg> &commonVect,
	    vector<vector<DimImg> > &localsVect,
	    const DimImg &nbItem) {
  using namespace tbb;
  task_group g;
  for (std::size_t idCopyValInThread = 0; idCopyValInThread < coreCount; ++idCopyValInThread) {
    vector<DimImg> &localVect (localsVect[idCopyValInThread]);
    g.run ([
#if ((TEST_NUM % 3) == 2) || (TEST_NUM > 5)
	    &commonVect,
#endif
#if ((TEST_NUM % 3) == 1) || ((TEST_NUM > 2) && (TEST_NUM < 6))
	    &localVect,
#endif
	    &nbItem] () {
	bodyTest (
#if ((TEST_NUM % 3) == 2) || (TEST_NUM > 5)
		  commonVect,
#endif
#if ((TEST_NUM % 3) == 1) || ((TEST_NUM > 2) && (TEST_NUM < 6))
		  localVect,
#endif
		  nbItem);
      });
  }
  g.wait ();
}

inline void
fThreadBoost (const std::size_t &coreCount,
	      vector<DimImg> &commonVect,
	      vector<vector<DimImg> > &localsVect,
	      const DimImg &nbItem) {
  std::vector<boost::thread> tasks;
  for (std::size_t idCopyValInThread = 0; idCopyValInThread < coreCount; ++idCopyValInThread) {
    vector<DimImg> &localVect (localsVect[idCopyValInThread]);
    tasks.push_back (boost::thread ([
#if ((TEST_NUM % 3) == 2) || (TEST_NUM > 5)
				     &commonVect,
#endif
#if ((TEST_NUM % 3) == 1) || ((TEST_NUM > 2) && (TEST_NUM < 6))
				     &localVect,
#endif
				     &nbItem] () {
	  bodyTest (
#if ((TEST_NUM % 3) == 2) || (TEST_NUM > 5)
		    commonVect,
#endif
#if ((TEST_NUM % 3) == 1) || ((TEST_NUM > 2) && (TEST_NUM < 6))
		    localVect,
#endif
		    nbItem);
	}));
  }
  for (unsigned int i = 0; i < coreCount; ++i)
    tasks[i].join ();
}

void
TestThread::testDeal (const unsigned int &coreCount, const DimImg &nbItem) {
  vector<vector<DimImg> > localsVect (coreCount, vector<DimImg> (nbItem, 0));
  vector<DimImg> commonVect (nbItem, 0);
  
  // for (unsigned int stg = 0; stg < coreCount; stg++) {
  //   cout << "" << stg << ": " << (((size_t) &global[stg][0]) % 64) << endl;
  // }
  
  // XXX direct
  auto startDirect = high_resolution_clock::now ();
  bodyTest (
#if ((TEST_NUM %3 ) == 2) || (TEST_NUM > 5)
	    commonVect,
#endif
#if ((TEST_NUM %3 ) == 1) || ((TEST_NUM > 2) && (TEST_NUM < 6))
	    localsVect[0],
#endif
	    nbItem);

  auto startThreadTTB = high_resolution_clock::now ();
  fThreadTTB (coreCount, commonVect, localsVect, nbItem);

  auto startThreadBoost = high_resolution_clock::now ();
  fThreadBoost (coreCount, commonVect, localsVect, nbItem);

  auto end = high_resolution_clock::now ();
  addTime (directDealStats, duration_cast<duration<double> > (startThreadTTB-startDirect).count ());
  addTime (threadTTBDealStats, duration_cast<duration<double> > (startThreadBoost-startThreadTTB).count ());
  addTime (threadBoostDealStats, duration_cast<duration<double> > (end-startThreadBoost).count ());
}
