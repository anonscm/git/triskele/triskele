// découpe fichier TestThread.cpp
// calcul alignement
// lecture seq => faire somme
// lecture // => faire somme

// écriture seq => écrire idx
// écriture // => écrire idx

// lecture/écriture seq => écrire x/2
// lecture/écriture // => écrire x/2

#include <cstddef>
#include <iostream>
#include <vector>

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/sum.hpp>
#include <boost/accumulators/statistics/count.hpp>
#include <boost/accumulators/statistics/min.hpp>
#include <boost/accumulators/statistics/max.hpp>
#include <boost/accumulators/statistics/mean.hpp>


#define TEST_NUM 0

namespace obelix {
  namespace triskele {
    using namespace std;
    namespace ba = boost::accumulators;
    typedef ba::accumulator_set<double,
				ba::stats<ba::tag::sum,
					  ba::tag::count,
					  ba::tag::mean,
					  ba::tag::min,
					  ba::tag::max> > AlgoStat;
    enum TimeType {
      directDealStats,
      threadTTBDealStats,
      threadBoostDealStats,
      inThreadDealStats,

      initStats,
      seqReadStats,
      parReadStats,
      seqWriteStats,
      parWriteStats,
      seqRWStats,
      parRWStats,

      TimeTypeCard
    };
    typedef uint32_t DimImg;

    class TestThread {
    public:

      static const unsigned int maxCoreCount;
      static const DimImg nbItem;
      static const std::size_t nbTest;

      AlgoStat timeStats[TimeTypeCard];
      // XXX vérifier l'alignement vector
      //vector<DimImg> global; 

      TestThread ();
      // XXX en commun
      static inline string ns2string (double delta);
      inline void addTime (const TimeType &timeType, const double &duration) {
	timeStats[timeType] (duration);
      }
      ostream &print (ostream &out, const AlgoStat stats[]);
      template <typename T>
      void fillVector (vector<T> &vect);

      void testDeal (const unsigned int &coreCount, const DimImg &nbItem);
    };
  }
}
