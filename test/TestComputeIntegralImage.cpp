#include <iostream>
#include <string>
#include <vector>

#include "triskeleBase.hpp"
#include "Tree.hpp"
#include "IntegralImage.hpp"

#define CATCH_CONFIG_MAIN    /* Let Catch provide main() */
#include <catch.hpp> /* ajouter l'option -I /usr/include/catch2/ dans MakefileNoOTB si besoin */

using namespace obelix;
using namespace obelix::triskele;

typedef uint16_t PixelT;

PixelT pixelsT5_12x8_ones [] = {
  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
};


bool test_integralImage()
{
  Size size(12, 8);

  PixelT *pixels_ones = pixelsT5_12x8_ones;
  cout << "==> pixels_ones (image avec seulement des 1)" << endl
       << printMap (pixels_ones, size, 0) << endl;

  DimImg pixelCount(size.getPixelsCount());
  cout << "==> pixelCount=" << pixelCount << endl;

  vector<double> output(pixelCount,0.);  // initialize with 0.
  cout << "==> output (before calling computeIntegralImage)" << endl
	   << printMap(&output[0], size, 0) << endl;

  // Compute the integral image
  computeIntegralImage (size, pixels_ones, &output[0]);

  cout << "==> output (after calling computeIntegralImage)" << endl
       << printMap(&output[0], size, 0) << endl;

  // Check that for the pixelsT5_12x8_ones input image (with only 1. values),
  // the integral image at (ii,jj) location corresponds to the area (ii+1)*(jj+1)
  cout << "==> Checking the results" << endl;
  int ii(0), jj(0), cpt(0);
  bool testValid(true);
  for (auto iter = output.begin(); iter != output.end(); ++iter) {
      jj = cpt % size.width;
      ii = (cpt - jj)/size.width;
      testValid = testValid && (output[cpt] == (ii+1)*(jj+1));
      std::cout << *iter << ":" << "(" << ii << "," << jj << ") -> " << (output[cpt] == (ii+1)*(jj+1)) << ' ';
      cpt++;
      if (cpt % size.width == 0) cout << endl;
      cout << endl;
  }

  return testValid;
 }

//int main () { cout << "Test result : " << test_integralImage() << endl; }

TEST_CASE("Testing computeIntegralImage...") {
    REQUIRE( test_integralImage() == true );
}
