#ifndef _Obelix_Border_tpp
#define _Obelix_Border_tpp

// ================================================================================
inline bool
Border::isBorder (const DimImg &idx) const {
  BOOST_ASSERT (idx < pixelsCount);
  return map.size () ? map[idx/8] & (1 << (idx%8)) : defaultVal;
}

inline bool
Border::isBorder (const Point &p) const {
  BOOST_ASSERT (p.x < size.width && p.y < size.height && p.z < size.length);
  return isBorder (point2idx (size, p));
}

// ================================================================================
#endif // _Obelix_Border_tpp
