#ifndef _Obelix_Triskele_XMLTreeBuilder_hpp
#define _Obelix_Triskele_XMLTreeBuilder_hpp

#ifdef NO_OTB
#include <tinyxml.h>
#else
#include <otb_tinyxml.h>
#endif

#include "TreeBuilder.hpp"

namespace obelix {
  namespace triskele {
    using namespace std;

    // ================================================================================
    class XMLTreeBuilder : public TreeBuilder {

    public:
      XMLTreeBuilder (const string &fileN) : fileName (fileN) {}

      void
      buildTree (Tree &tree);

      static void
      exportToFile (const Tree &tree, const string &fileName);

      void
      exportToFile (const Tree &tree) const { return exportToFile (tree, fileName); }

    private:
      static void
      writeNodeChildren (const Tree &tree, const DimNodeId &id, TiXmlElement *node);

      DimNodeId
      getNodeCount (const TiXmlElement *node);

      void
      readNodeChildren (const TiXmlElement *node, DimNodeId &id);
    
    private:
      string fileName;
    };

    // ================================================================================
  } // triskele
} // obelix

#endif // _Obelix_Triskele_XMLTreeBuilder_hpp
