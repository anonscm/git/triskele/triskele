#ifndef _Obelix_Triskele_XYZAttributes_hpp
#define _Obelix_Triskele_XYZAttributes_hpp

#include "triskeleBase.hpp"
#include "CompAttribute.hpp"
#include "Attributes/AreaAttributes.hpp"

namespace obelix {
  namespace triskele {

    // ================================================================================
    /*! Fonction non monotonne. */
    struct AverageXYZ {
      double x, y, z;
      inline AverageXYZ () : x(0), y(0), z (0) {}
      friend ostream &operator << (ostream& out, const AverageXYZ &xyz) { return out << "(" << xyz.x << ", " << xyz.y <<  ", " << xyz.z << ")"; }
      operator DimImg () const { return (DimImg) (x*y*z); }
    };

    class XYZAttributes : public CompAttribute<AverageXYZ> {
    public:
      XYZAttributes (const Tree &tree, const AreaAttributes &areaAttributes);
      ~XYZAttributes ();
      virtual inline ostream &print (ostream &out) const { CompAttribute::print (out, "xyz"); return out; }
    protected:
      void compute (const AreaAttributes &areaAttributes);
    };

    // ================================================================================
  } // triskele
} // obelix

#endif // _Obelix_Triskele_XYZAttributes_hpp
