#ifndef _Obelix_Triskele_AreaAttributes_hpp
#define _Obelix_Triskele_AreaAttributes_hpp

#include "triskeleBase.hpp"
#include "CompAttribute.hpp"

namespace obelix {
  namespace triskele {

    // ================================================================================
    /*! Fonction monotonne strictement croissante. */
    class AreaAttributes : public CompAttribute<DimImg> {
    public:
      AreaAttributes (const Tree &tree);
      ~AreaAttributes ();

      template<typename PixelT>
      void cut (vector<vector<PixelT> > &allBands, const AttributeProfiles<PixelT> &attributeProfiles, const vector<DimImg> &thresholds) const;
      virtual inline ostream &print (ostream &out) const { CompAttribute::print (out, "area"); return out; }
    protected:
      void compute ();
    };

    // ================================================================================
  } // triskele
} // obelix

#endif // _Obelix_Triskele_AreaAttributes_hpp
