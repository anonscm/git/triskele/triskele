#ifndef _Obelix_Triskele_XYAttributes_hpp
#define _Obelix_Triskele_XYAttributes_hpp

#include "triskeleBase.hpp"
#include "CompAttribute.hpp"
#include "Attributes/AreaAttributes.hpp"

namespace obelix {
  namespace triskele {

    // ================================================================================
    /*! Fonction non monotonne. */
    struct AverageXY {
      double x, y;
      inline AverageXY () : x(0), y(0) {}
      friend ostream &operator << (ostream& out, const AverageXY &xy) { return out << "(" << xy.x << ", " << xy.y << ")"; }
      operator DimImg () const { return (DimImg) (x*y); }
    };

    class XYAttributes : public CompAttribute<AverageXY> {
    public:
      XYAttributes (const Tree &tree, const AreaAttributes &areaAttributes);
      ~XYAttributes ();
      virtual inline ostream &print (ostream &out) const { CompAttribute::print (out, "xy"); return out; }
    protected:
      void compute (const AreaAttributes &areaAttributes);
    };

    // ================================================================================
  } // triskele
} // obelix

#endif // _Obelix_Triskele_XYAttributes_hpp
