#ifndef _Obelix_Triskele_MoIAttributes_hpp
#define _Obelix_Triskele_MoIAttributes_hpp

#include "triskeleBase.hpp"
#include "CompAttribute.hpp"
#include "Attributes/AreaAttributes.hpp"
#include "Attributes/XYAttributes.hpp"

namespace obelix {
  namespace triskele {

    // ================================================================================
    /*! Fonction monotonne strictement croissante. */
    class MoIAttributes : public CompAttribute<double> {
    public:
      MoIAttributes (const Tree &tree, const AreaAttributes &areaAttributes, const XYAttributes &xyAttributes);
      ~MoIAttributes ();
      virtual inline ostream &print (ostream &out) const { CompAttribute::print (out, "moi"); return out; }

      template<typename PixelT>
      void cut (vector<vector<PixelT> > &allBands, const AttributeProfiles<PixelT> &attributeProfiles,
		const vector<double> &thresholds) const;
    protected:
      void compute (const AreaAttributes &areaAttributes, const XYAttributes &xyAttributes);
    };

    // ================================================================================
  } // triskele
} // obelix

#endif // _Obelix_Triskele_MoIAttributes_hpp
