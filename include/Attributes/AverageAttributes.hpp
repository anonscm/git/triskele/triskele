#ifndef _Obelix_Triskele_AverageAttributes_hpp
#define _Obelix_Triskele_AverageAttributes_hpp

#include "triskeleBase.hpp"
#include "IImage.hpp"
#include "CompAttribute.hpp"
#include "Attributes/AreaAttributes.hpp"

namespace obelix {
  namespace triskele {

    // ================================================================================
    /*! Fonction non monotonne. */
    class AverageAttributes : public CompAttribute<double> {
    public:
      template<typename PixelT>
      AverageAttributes (const Tree &tree, const Raster<PixelT> &raster, const AreaAttributes &areaAttributes);
      ~AverageAttributes ();

      virtual inline ostream &print (ostream &out) const { CompAttribute::print (out, "average"); return out; }
    protected:
      template<typename PixelT>
      void compute (const Raster<PixelT> &raster, const AreaAttributes &areaAttributes);
    };

    // ================================================================================
  } // triskele
} // obelix

#endif // _Obelix_Triskele_AverageAttributes_hpp
