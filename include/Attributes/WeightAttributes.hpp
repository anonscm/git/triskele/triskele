#include "CompAttribute.hpp"
#ifndef _Obelix_Triskele_WeightAttributes_hpp
#define _Obelix_Triskele_WeightAttributes_hpp

#include "triskeleBase.hpp"

namespace obelix {
  namespace triskele {

    // ================================================================================
    /*! Fonction monotonne strict croissante ou décroissante en fonction de la fonction de poid. */
    template<typename WeightT>
    class WeightAttributes : public CompAttribute<WeightT> {
    public:
      WeightAttributes (const Tree &tree, const bool &decr);
      virtual ~WeightAttributes ();

      void resetDecr (bool decr);
      void setWeightBounds (Tree &tree);
      template<typename PixelT>
      void cut (vector<vector<PixelT> > &allBands, const AttributeProfiles<PixelT> &attributeProfiles,
		const vector<WeightT> &thresholds) const;

      virtual inline ostream &print (ostream &out) const { CompAttribute<WeightT>::print (out, "weight"); return out; }
    };

    // ================================================================================
  } // triskele
} // obelix

#endif // _Obelix_Triskele_WeightAttributes_hpp
