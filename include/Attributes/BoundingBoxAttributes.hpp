#ifndef _Obelix_Triskele_BoundingBoxAttributes_hpp
#define _Obelix_Triskele_BoundingBoxAttributes_hpp

#include "triskeleBase.hpp"
#include "CompAttribute.hpp"
#include "Attributes/AreaAttributes.hpp"

namespace obelix {
  namespace triskele {

    // ================================================================================
    /*! Fonction monotonne. */
    struct BoundingBox {
      DimImg minX, minY, maxX, maxY;
      DimChanel minZ, maxZ;
      inline BoundingBox () : minX (DimSideImg_MAX), minY (DimSideImg_MAX), minZ (DimChanel_MAX), maxX (0), maxY (0), maxZ (0) {}
      friend ostream &operator << (ostream& out, const BoundingBox &bb) {
	return out
	  << "(" << bb.minX << ", " << bb.minY <<  ", " << bb.minZ << ")-(" << bb.maxX << ", " << bb.maxY <<  ", " << bb.maxZ << ")";
      }
    };

    class BoundingBoxAttributes : public CompAttribute<BoundingBox> {
    public:
      BoundingBoxAttributes (const Tree &tree);
      ~BoundingBoxAttributes ();
      virtual inline ostream &print (ostream &out) const { CompAttribute::print (out, "bb"); return out; } // XXX
    protected:
      void compute ();
    };

    // ================================================================================
  } // triskele
} // obelix

#endif // _Obelix_Triskele_BoundingBoxAttributes_hpp
