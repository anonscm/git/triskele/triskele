#ifndef _Obelix_Triskele_SDAttributes_hpp
#define _Obelix_Triskele_SDAttributes_hpp

#include "triskeleBase.hpp"
#include "CompAttribute.hpp"
#include "Attributes/AreaAttributes.hpp"

namespace obelix {
  namespace triskele {

    // ================================================================================
    /*! Fonction non monotonne, globalement croissante. */
    class SDAttributes : public CompAttribute<double> {
    public:
      SDAttributes (const Tree &tree, const AreaAttributes &areaAttributes);
      ~SDAttributes ();
      virtual inline ostream &print (ostream &out) const { CompAttribute::print (out, "sd"); return out; }

      template<typename PixelT>
      void cut (vector<vector<PixelT> > &allBands, const AttributeProfiles<PixelT> &attributeProfiles,
		const vector<double> &thresholds) const;
    protected:
      void compute (const AreaAttributes &areaAttributes);
    };

    // ================================================================================
  } // triskele
} // obelix

#endif // _Obelix_Triskele_SDAttributes_hpp
