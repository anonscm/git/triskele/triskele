#ifndef _Obelix_IImage_hpp
#define _Obelix_IImage_hpp

#include <string>
#include <iostream>
#include <boost/assert.hpp>

#include <gdal_priv.h>

#include "triskeleBase.hpp"
#include "triskeleGdalGetType.hpp"

namespace obelix {
  using namespace std;

  // ================================================================================
  template<typename PixelT>
  class Raster {
  private:
    Size		size;
    vector<PixelT>	pixels;

  public:
    void		setSize (const Size &size);
    inline const Size	&getSize () const;
    inline const PixelT	*getPixels () const;
    inline PixelT	*getPixels ();
    inline const PixelT	*getFrame (const DimChanel &frameId) const;
    inline PixelT	*getFrame (const DimChanel &frameId);
    inline vector<PixelT> &getPixelsVector ();
    inline const vector<PixelT> &getPixelsVector () const;
    inline DimNodeId	pointIdx (const Point &p) const;
    inline PixelT	getValue (const DimImg &idx) const;
    inline PixelT	getValue (const Point &point) const;

    		Raster (const Size &size = NullSize);
    		Raster (const Raster &raster, const Rect &tile);
    		~Raster ();
  };

  // ================================================================================
  /** Interface Image */
  class IImage {
  public:
    void			setFileName (string fileName);
    void			getGeo (string& projectionRef, vector<double> &geoTransform) const;
    void			setGeo (const string& projectionRef, vector<double> geoTransform, const Point &topLeft);
    inline const string		&getFileName () const;
    inline const Size		&getSize () const;
    inline const DimChanel	&getBandCount () const;
    inline GDALDataType		getDataType () const;
    inline const bool		isRead () const;
    inline const bool		isEmpty () const;

    IImage (const string &imageFileName = "");
    ~IImage ();

    void	readImage (const DimChanel &spectralDepth = 0, const bool mixedBandFlag = false);
    void	createImage (const Size &size, const GDALDataType &dataType, const DimChanel &nbOutputBands);
    void	createImage (const Size &size, const GDALDataType &dataType, const DimChanel &nbOutputBands,
			     const IImage &inputImage, const Point &topLeft);
    void	close ();

    template<typename PixelT>
    void		readBand (Raster<PixelT> &raster, const DimChanel &band) const;
    template<typename PixelT>
    void		readBand (Raster<PixelT> &raster, DimChanel band, const Point &cropOrig, const Size &cropSize) const;
    template<typename PixelT>
    void		readFrame (PixelT *pixels, DimChanel band, const Point &cropOrig, const Size &cropSize) const;

    template<typename PixelT>
    void		writeBand (PixelT *pixels, DimChanel band) const;
    template<typename PixelT>
    void		writeFrame (PixelT *pixels, DimChanel band) const;
    void		writeDoubleBand (double *pixels, DimChanel band) const;
    void		writeDoubleFrame (double *pixels, DimChanel band) const;
    void		writeDimImgBand (DimImg *pixels, DimChanel band) const;
    void		writeDimImgFrame (DimImg *pixels, DimChanel band) const;

  private:
    IImage (const IImage &o) = delete;
    IImage &operator= (const IImage&) = delete;
    
  private:
    static size_t	gdalCount;
    string		projectionRef;
    vector<double>	geoTransform;
    string		fileName;
    Size		size;
    DimChanel		bandCount;
    bool		mixedBandFlag;
    GDALDataType	dataType;
    GDALDataset		*gdalInputDataset;
    GDALDataset		*gdalOutputDataset;
    bool		read;
  };

  ostream &operator << (ostream &out, const IImage &iImage);

  // ================================================================================
#include "IImage.tpp"

} // obelix

#endif // _Obelix_IImage_hpp
