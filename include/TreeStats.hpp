#ifndef _Obelix_Triskele_TreeStats_hpp
#define _Obelix_Triskele_TreeStats_hpp

#include <iostream>

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/sum.hpp>
#include <boost/accumulators/statistics/count.hpp>
#include <boost/accumulators/statistics/min.hpp>
#include <boost/accumulators/statistics/max.hpp>
#include <boost/accumulators/statistics/mean.hpp>

#include "ArrayTree/triskeleArrayTreeBase.hpp"

namespace obelix {
  namespace triskele {

    using namespace std;
    namespace ba = boost::accumulators;
    typedef ba::accumulator_set<DimImg, ba::stats<ba::tag::count,
						  ba::tag::mean,
						  ba::tag::min,
						  ba::tag::max> > TreeStatsDim;
    typedef ba::accumulator_set<double, ba::stats<ba::tag::sum,
						  ba::tag::count,
						  ba::tag::mean,
						  ba::tag::min,
						  ba::tag::max> > TreeStatsDouble;

    // ================================================================================
    enum TimeType {
      borderStats,
      ndviStats,
      sobelStats,
      iiStats,

      buildTreeStats,
      buildSetupStats,
      buildToTilesStats,
      buildEdgesStats,
      buildParentsStats,
      buildFromTilesStats,
      buildMergeStats,
      buildForestStats,
      buildIndexStats,
      buildCompressStats,
      buildChildrenStats,
      
      setAPStats,
      areaStats,
      averageStats,
      bbStats,
      xyzStats,
      moiStats,
      sdStats,
      filteringStats,
      copyImageStats,
      readStats,
      writeStats,
      allStats,

      learningStats,
      cleanStats,
      checkAccuracyStats,
      predictionStats,

      TimeTypeCard
    };

    // ================================================================================
    class TreeStats {
    public :

      static TreeStats global;
      static const string timeTypeLabels[];

      TreeStats ();

      void reset ();
      inline void addDim (const TreeType &treeType, const DimImg& leafCount, const DimImg& compCount);
      inline void addTime (const TimeType &timeType, const double &duration);
      inline const TreeStatsDouble &getTimeStats (const TimeType &timeType);

      // nice ostream
      struct CPrintDim {
	const TreeStats &treeStats;
	CPrintDim (const TreeStats &treeStats);
	ostream &print (ostream &out) const;
	ostream &print (ostream &out, const vector<TreeStatsDim> stats, const string &msg) const;
      };
      CPrintDim printDim () const;
      friend ostream &operator << (ostream& out, const CPrintDim &cpd) { return cpd.print (out); }

      struct CPrintTime {
	const TreeStats &treeStats;
	CPrintTime (const TreeStats &treeStats);
	ostream &print (ostream &out) const;
      };
      CPrintTime printTime () const;
      friend ostream &operator << (ostream& out, const CPrintTime &cpt) { return cpt.print (out); }

    private :

      vector<TreeStatsDim> leavesStats, compStats;
      vector<TreeStatsDouble> timeStats;
    };

    // ================================================================================
#include "TreeStats.tpp"
  } // triskele
} // obelix

#endif // _Obelix_Triskele_TreeStats_hpp

