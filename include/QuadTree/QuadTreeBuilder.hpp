#ifndef _Obelix_Triskele_QuadTreeBuilder_hpp
#define _Obelix_Triskele_QuadTreeBuilder_hpp

#include "TreeBuilder.hpp"
#include "triskeleBase.hpp"

namespace obelix {
  namespace triskele {

    // ================================================================================
    class QuadTreeBuilder : public TreeBuilder {
    public:
      QuadTreeBuilder (const DimSideImg &width, const DimSideImg &height) : width (width), height (height) {}

      virtual void
      buildTree (Tree &tree);

    private:
      DimImg
      getStepCount (const DimSideImg &x, const DimSideImg &y, const DimSideImg &width, const DimSideImg &height) const;

      DimImg
      setParents (DimImg &parentId,
		  const DimSideImg &x, const DimSideImg &y, const DimSideImg &width, const DimSideImg &height,
		  const DimSideImg &imgWidth, const DimSideImg &imgHeight, DimImg level = 0) const;

    private:
      DimSideImg width, height;
    };

    // ================================================================================
  } // triskele
} // obelix

#endif // _Obelix_Triskele_QuadTreeBuilder_hpp
