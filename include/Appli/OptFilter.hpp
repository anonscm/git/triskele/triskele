#ifndef _Obelix_Triskele_OptFilter_hpp
#define _Obelix_Triskele_OptFilter_hpp

#include <iostream>
#include <fstream>
#include <vector>

#include "triskeleBase.hpp"
#include "ArrayTree/GraphWalker.hpp"
#include "IImage.hpp"
#include "Appli/OptCrop.hpp"
#include "Appli/OptRanges.hpp"
#include "Appli/OptChanel.hpp"
#include "CompAttribute.hpp"

namespace obelix {
  namespace triskele {
    using namespace std;

    // ================================================================================
    class OptFilter {
    public:
      bool		debugFlag;
      bool		timeFlag;
      DimSortCeil	countingSortCeil;
      string		showConfig;
      bool		oneBand;
      bool		includeNoDataFlag;
      IImage		inputImage;
      IImage		outputImage;
      Connectivity	connectivity;
      DimCore		coreCount;

      OptCrop		optCrop;
      OptChanel		optChanel;

      OptFilter ();
      OptFilter (int argc, char** argv);

      ofstream out;

      void usage (string msg = "", bool hidden = false) const;
      void version () const;
      void parse (int argc, char** argv);

    };

    // ================================================================================
    ostream &operator << (ostream &out, const OptFilter &optFilter);

    template <typename T>
    ostream &operator << (ostream& out, const vector<T> &v);

    // ================================================================================
  } // triskele
} // obelix

#endif //_Obelix_Triskele_OptGenerator_hpp
