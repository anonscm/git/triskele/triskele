#ifndef _Obelix_OptCrop_hpp
#define _Obelix_OptCrop_hpp

#include "triskeleBase.hpp"

namespace obelix {

  // ================================================================================
  class OptCrop {
  public:
    Point	topLeft;
    Size	size;

    OptCrop ();
  };

  ostream &operator << (ostream &out, const OptCrop &optCrop);

  // ================================================================================
}//obelix

#endif // _Obelix_Triskele_OptCrop_hpp

