#ifndef _Obelix_Triskele_OptChanel_hpp
#define _Obelix_Triskele_OptChanel_hpp

#include <iostream>
#include <map>
#include <gdal_priv.h>
#include <tinyxml.h>

#include "triskeleBase.hpp"
#include "Appli/OptRanges.hpp"
#include "Appli/OptSizes.hpp"
#include "CompAttribute.hpp"
#include "ArrayTree/triskeleArrayTreeBase.hpp"
#include "misc.hpp"

namespace obelix {
  namespace triskele {
  using namespace std;

  // ================================================================================
    enum ApOrigPos {
      apOrigNoPos, apOrigPosBegin, apOrigPosEnd, apOrigPosBoth,
      apOrigPosEverywhere, apOrigPosBeginMin, apOrigPosBeginMax
    };
    extern const string apOrigPosLabels[];
    extern const map<string, ApOrigPos> ApOrigPosMap;
    ostream &operator << (ostream &out, const ApOrigPos &pos);
    istream &operator >> (istream &in, ApOrigPos &pos);


    // ================================================================================
    enum ChanelType {
      Original, Ndvi, Sobel, AP, DAP, Haar, Stat
    };
    extern const string chanelTypeLabels[];
    extern const map<string, ChanelType> chanelTypeMap;
    ostream &operator << (ostream &out, const ChanelType &chanelType);
    istream &operator >> (istream &in, ChanelType &chanelType);

    // ================================================================================
    class InputChanel {
    public:
      vector<string> localComments;
      ChanelType chanelType;
      DimChanel inputId;

      /* size => Original:0, Ndvi:2, Sobel:1, AP: 1, DAP: 1, Haar:1, Stat:1, *:1 */
      vector<InputChanel*> generateBy;
      vector<InputChanel*> produce;

      vector<Size> winSizes;
      TreeType treeType;
      FeatureType featureType;
      AttributeType attributeType;
      vector<double> thresholds;
      bool dapWeight;
      ApOrigPos apOrigPos;

      bool doWrite;
      bool prodWrite;
      bool prodNewImage, prodInetgralImage, prodDoubleInetgralImage, prodDap;
      DimChanel copyRank, typeRank, origDapRank;
      DimChanel integralImageRank, doubleIntegralImageRank;
      DimChanel outputRank;

      bool isNdvi () const { return chanelType == Ndvi; }
      bool isSobel () const { return chanelType == Sobel; }
      bool updateProdWrite ();

      InputChanel (const ChanelType &ChanelType, const DimChanel &inputId, const bool &doWrite = false);
      InputChanel (const ChanelType &chanelType, const DimChanel &inputId, const bool &doWrite, const vector<InputChanel *> &generateBy);
      void resetTransient ();

      vector<DimChanel>	getRelIdx () const;
      void writeXml (TiXmlElement *cfgNode) const;
    };
    ostream &operator << (ostream &out, const InputChanel &inputChanel);

    // ================================================================================
    class OptChanel {
      string name, created, modified;
      GDALDataType inputType;
      vector<vector <InputChanel *> > chanels;
      vector<InputChanel *> selectedSet;
      TreeType selectedTreeType;
      FeatureType selectedFeatureType;
      AttributeType selectedAttributeType;
      map<DimChanel, InputChanel *> outputOrder, apOrder, dapOrder, haarOrder, statOrder;
      vector<string> outComments, comments;

      DimChanel nextChanelUniqId;
      DimChanel copyCount, originalUsedCount, origDapUsedCount, ndviUsedCount, sobelUsedCount;
      DimChanel integralImageCount, doubleIntegralImageCount;
      DimChanel apCount, dapCount, haarCount, statCount, outputCount;
      void resetTransient ();

    public:
      DimChanel spectralDepth;
      bool mixedBandFlag;
      static const string chExt;

      DimChanel getSize (const ChanelType &chanelType) const;
      DimChanel getCopyCount () const { return copyCount; }
      DimChanel getOrigDapCount () const { return origDapUsedCount; }
      DimChanel getNdviCount () const { return ndviUsedCount; }
      DimChanel getSobelCount () const { return sobelUsedCount; }
      DimChanel getApCount () const { return apCount; }
      DimChanel getDapCount () const { return dapCount; }
      DimChanel getIntegralImageCount () const { return integralImageCount; }
      DimChanel getDoubleIntegralImageCount () const { return doubleIntegralImageCount; }
      DimChanel getHaarCount () const { return haarCount; }
      DimChanel getStatCount () const { return statCount; }
      DimChanel getOutputCount () const { return outputCount; }
      const vector<InputChanel*> getOriginal () const { return chanels [Original];}
      const map<DimChanel, InputChanel *> getApOrder () const { return apOrder;}
      const map<DimChanel, InputChanel *> getDapOrder () const { return dapOrder;}
      const map<DimChanel, InputChanel *> getHaarOrder () const { return haarOrder;}
      const map<DimChanel, InputChanel *> getStatOrder () const { return statOrder;}

      OptChanel ();
      ~OptChanel ();

      bool operator== (const OptChanel &rhs) const;
      bool isEmpty () const;
      bool inputMatches (const GDALDataType &inputType, const DimChanel &inputBandsCount) const;

      void setType (const GDALDataType &inputType, const DimChanel &inputBandsCount);
      void setName (const string &name);

      void loadFile (const string &fileName);
      void saveFile (const string &fileName) const;

      InputChanel *findChanel (const DimChanel &chanelId);
      void updateChanelUniqId ();
      void updateChanels ();
      vector<InputChanel*> printableOrder (vector<InputChanel*> &circularDefinition) const;
      vector<InputChanel*> getActiveOrder () const;

      void selectInputs (const vector<int> &chanelsId);
      void selectTreeType (const TreeType &treeType);
      void selectFeatureType (const FeatureType &featureType);
      void selectAttributeType (const AttributeType &attributeType);
      vector <InputChanel *> selectedAP ();

      void remove (const vector<InputChanel *> &toRemove);
      void remove (const ChanelType &chanelType);
      void removeCopy ();
      void removeThresholds ();

      void addNdvi (const DimChanel &first, const DimChanel &last);
      void addNdvi (const DimChanel &id, const DimChanel &first, const DimChanel &last, const bool &doWrite);
      void addSobel (const DimChanel &chanel);
      void addSobel (const DimChanel &id, const DimChanel &chanel, const bool &doWrite);
      void addCopy (const DimChanel &chanel);

      void addThresholds (const string &thresholds);
      void addThresholds (const vector<double> &thresholds);
      void addThresholds (const DimChanel &id, const DimChanel &from, const bool &doWrite, const vector<string> &localComments,
			  const TreeType &treeType, const FeatureType &featureType, const AttributeType &attributeType, const vector<double> &thresholds);
      void addThresholds (const DimChanel &id, const bool &doWrite,
			  const TreeType &treeType, const FeatureType &featureType, const AttributeType &attributeType, const vector<double> &thresholds,
			  InputChanel *inputChanel, const vector<string> &localComments);

      void addDap (const bool &dapWeight, const ApOrigPos &apOrigPos);
      void addDap (const DimChanel &id, const DimChanel &from, const bool &doWrite, const bool &dapWeight, const ApOrigPos &apOrigPos);
      void addDap (const DimChanel &id, const bool &doWrite, const bool &dapWeight, const ApOrigPos &apOrigPos, InputChanel *inputChanel);

      void addHaar (const vector<Size> &winSizes);
      void addStat (const vector<Size> &winSizes);
      void addTexture (const ChanelType &chanelType, const vector<Size> &winSizes);
      void addTexture (const ChanelType &chanelType, const DimChanel &id, const DimChanel &from, const bool &doWrite, const vector<string> &localComments, const vector<Size> &winSizes);
      void addTexture (const ChanelType &chanelType, const DimChanel &id, const bool &doWrite, const vector<Size> &winSizes, InputChanel *inputChanel, const vector<string> &localComments);

      ostream &printOutput (ostream &out) const;
      ostream &printState (ostream &out) const;
      friend ostream &operator << (ostream &out, const OptChanel &optChanel);
    };
    ostream &operator << (ostream &out, const OptChanel &optChanel);

    // ================================================================================
  } // triskele
} // obelix

#endif //_Obelix_Triskele_OptChanel_hpp
