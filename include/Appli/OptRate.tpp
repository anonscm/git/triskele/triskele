#ifndef _Obelix_OptRate_tpp
#define _Obelix_OptRate_tpp

// ================================================================================
template<typename T>
inline T
OptRate::getRate (const T &value) const {
  return T (round (percent ? value*rate : rate));
}

template<typename T>
inline T
OptRate::getMoreRate (const T &value) const {
  return T (round (percent ? value*(1.+rate) : value+rate));
}

template<typename T>
inline T
OptRate::getLessRate (const T &value) const {
  return T (round (percent ? value/(1.+rate) : value-rate));
}

// ================================================================================
#endif // _Obelix_OptRate_tpp
