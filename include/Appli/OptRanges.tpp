#ifndef _Obelix_OptRanges_tpp
#define _Obelix_OptRanges_tpp

// ================================================================================
inline const vector<int> &
OptRanges::getSet () const {
  return set;
}

inline bool
OptRanges::empty () const {
  return set.empty ();
}

inline bool
OptRanges::uncompleted () const {
  return minFlag || maxFlag;
}

inline int
OptRanges::size () const {
  return set.size ();
}

inline int
OptRanges::first () const {
  return set[0];
}

inline int
OptRanges::last () const {
  return set[set.size ()-1];
}

// ================================================================================
#endif // _Obelix_OptRanges_tpp
