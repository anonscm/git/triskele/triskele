#ifndef _Obelix_OptRanges_hpp
#define _Obelix_OptRanges_hpp

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <numeric>
#include <boost/algorithm/string.hpp>

namespace obelix {
  using namespace std;
  using namespace boost;

  // ================================================================================
  class OptRanges {
  private:
    /*!
     * \brief set Vector qui correspond à l'ensemble
     */
    vector<int> set;
    /*!
     * \brief Indicateur si le max doit etre defini (à l'aide de la methode setMaxValue())
     */
    bool minFlag, maxFlag;
    int minInter, maxInter;
    
  public:
    /*!
     * \brief getSet Getter de l'ensemble
     * \return L'ensemble set
     */
    inline const vector<int> &getSet () const;
    /*!
     * \brief empty Vérifie si l'ensemble est vide ou non
     * \return true si l'ensemble est vide, false sinon
     */
    inline bool empty () const;
    /*!
     * \brief uncompleted indique si la définition de l'intervale est incomplète
     * \return true si l'intervale est incomplète
     */
    inline bool uncompleted () const;
    /*!
     * \brief size Retourne la taille de l'ensemble
     * \return La taille de l'ensemble
     */
    inline int size () const;
    /*!
     * \brief first Retourne la première valeur de l'ensemble
     * \return La première valeur de l'ensemble
     */
    inline int first () const;
    /*!
     * \brief last Retourne la dernière valeur de l'ensemble
     * \return La dernière valeur de l'ensemble
     */
    inline int last () const;

    /*!
     * \brief OptRanges Constructeur qui convertit la chaîne option en ensemble
     * \param option Chaîne de caractère à convertir
     */
    OptRanges (const string &option = "");
    /*!
     * \brief OptRanges Constructeur avec un interval
     */
    OptRanges (int first, int last);
    /*!
     * \brief OptRanges Constructeur de copie
     * \param optRanges Objet à copier
     */
    OptRanges (const OptRanges &optRanges);

    void reset ();
    void init (const string &option);

    void setLimits (int minOpt, int maxOpt);
    /*!
     * \brief toSet Conversion en ensemble (On retire les doublons et ordonne le vector)
     * \return L'objet OptRanges
     */
    OptRanges &toSet ();
    /*!
     * \brief contains Vérifie si la valeur value est contenue dans l'ensemble
     * \param value valeur à vérifier
     * \return true si la valeur est contenue dans l'ensemble, false sinon
     */
    bool contains (int value) const;
  };
  ostream &operator << (ostream &out, const OptRanges &optRanges);
  istream &operator >> (istream &in, OptRanges &optRanges);

  // ================================================================================
#include "OptRanges.tpp"
}//namespace obelix

#endif //_Obelix_OptRanges_hpp
