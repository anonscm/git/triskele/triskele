#ifndef _Obelix_OptSizes_hpp
#define _Obelix_OptSizes_hpp

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <numeric>
#include <boost/algorithm/string.hpp>

#include "triskeleBase.hpp"

namespace obelix {
  using namespace std;
  using namespace boost;

  // ================================================================================
  class OptSizes {
  private:
    vector<Size> sizes;

  public:
    inline const vector<Size> &getSizes () const;
    inline bool empty () const;
    inline int size () const;

    OptSizes (const string &option = "");
    OptSizes (const OptSizes &optSizes);

    void reset ();
    void init (const string &option);
  };
  ostream &operator << (ostream &out, const OptSizes &optSizes);
  istream &operator >> (istream &in, OptSizes &optSizes);

  // ================================================================================
#include "OptSizes.tpp"
}//namespace obelix

#endif //_Obelix_OptSizes_hpp
