#ifndef _Obelix_Triskele_Filter_hpp
#define _Obelix_Triskele_Filter_hpp

#include "IImage.hpp"
#include "TextWin.hpp"
#include "ArrayTree/ArrayTreeBuilder.hpp"
#include "Appli/OptFilter.hpp"

namespace obelix {
  namespace triskele {
    using namespace obelix;

    // ================================================================================
    template <typename PixelT>
    class TriskeleFilter {
    public:
      TriskeleFilter (IImage &inputImage, IImage &outputImage,
		      const Point &topLeft, const Size &size, const OptFilter &optFilter);
      ~TriskeleFilter ();

      void parseInput ();

    private:
      IImage &inputImage;
      IImage &outputImage;
      const OptFilter &optFilter;
      Point topLeft;
      Size size;
      Border border;
      GraphWalker graphWalker;
      DimImg pixelCount;
      Raster<PixelT> inputRaster;
      Raster<PixelT> outputRaster;

      vector<vector<double> > gTmp;
      vector<Raster<PixelT> > ndviRaster;
      vector<Raster<PixelT> > sobelRasters;
      vector<double> integralImage, doubleIntegralImage;
      Tree tree;
      WeightAttributes<PixelT> weightAttributes;
      map<const InputChanel *, map<TreeType, map<AttributeType, const InputChanel *> > > waitingAP;

      Raster<PixelT> &getProducerRaster (const InputChanel &inputChanel);
      void readInput (const DimChanel &chanel);
      void updateBorder ();
      void updateIntegralImage (const InputChanel &inputChanel);
      void updateSobel (const InputChanel &inputChanel, const InputChanel &sobelChanel);
      void updateNdvi (const InputChanel &inputChanel, const InputChanel &ndviChanel);
      void writeHaar (const InputChanel &inputChanel, const InputChanel &haarChanel);
      void writeStat (const InputChanel &inputChanel, const InputChanel &statChanel);
      void writeAP (const InputChanel &inputChanel, const InputChanel &apChanel);

      template <typename OutPixelT, typename APFunct, typename WFunct>
      void writeAP2 (ArrayTreeBuilder<PixelT, PixelT> &atb, const Raster<PixelT> &raster, const APFunct &setAP, const map <AttributeType, const InputChanel*> &attrProd, const WFunct &wFunct);
      template <typename OutPixelT>
      void writeDAP (const vector<const OutPixelT*> &dapSrcIdx, const InputChanel &dapChanel);

      void getOneBandImage (IImage& oneBandImage, const GDALDataType &dataType, const DimChanel &band) const;
      void dumpInput (const InputChanel &inputChanel);
      void writePixelBand (PixelT *pixels, const DimChanel &chanel);
      void writeDoubleBand (double *pixels, const DimChanel &chanel);
      void writeDimImgBand (DimImg *pixels, const DimChanel &chanel);
    };

    // ================================================================================
  } // triskele
} // obelix

#endif // _Obelix_Triskele_Filter_hpp
