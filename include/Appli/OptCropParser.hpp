#ifndef _Obelix_OptCropParser_hpp
#define _Obelix_OptCropParser_hpp

#include "IImage.hpp"
#include "Appli/OptCrop.hpp"

namespace obelix {

  // ================================================================================
  class OptCropParser {
  public:
    long	left, top, width, height;

    boost::program_options::options_description description;

    OptCropParser ();

    void parse (OptCrop &optCrop, const IImage &inputImage, boost::program_options::basic_parsed_options<char> &parsed);
  };

  // ================================================================================
} // obelix

#endif // _Obelix_OptCropParser_hpp
