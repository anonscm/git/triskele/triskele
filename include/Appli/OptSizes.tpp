#ifndef _Obelix_OptSizes_tpp
#define _Obelix_OptSizes_tpp

// ================================================================================
inline const vector<Size> &
OptSizes::getSizes () const {
  return sizes;
}

inline bool
OptSizes::empty () const {
  return sizes.empty ();
}

inline int
OptSizes::size () const {
  return sizes.size ();
}

// ================================================================================
#endif // _Obelix_OptSizes_tpp
