#ifndef _Obelix_Triskele_OptChanelParser_hpp
#define _Obelix_Triskele_OptChanelParser_hpp

#include <boost/program_options.hpp>

#include "IImage.hpp"

#include "OptChanel.hpp"

namespace obelix {
  namespace triskele {

    // ================================================================================
    enum OptionNames {
      ONInvalidOption,
      ONNdviBands,
      ONSobelBands,
      ONCopyBands,
      ONWithBands,
      ONFeatureType,
      ONTreeType,
      ONAttributeType,
      ONThresholds,
      ONDapWeight,
      ONDapPos,
      ONHaarSizes,
      ONStatSizes
    };

    // ================================================================================
    class OptChanelParser {
      static const map<string, OptionNames> OptionNamesLabels;
      static OptionNames resolveOption (const string &input);

      map<string, int> optRank;
      int getOptRank (const string &optName);

      bool noMixedBand, noNdviFlag, noSobelFlag, noCopyFlag, noHaarFlag, noStatFlag, showChanel;
      vector<OptRanges> ndviBands;
      OptRanges sobelBands;

      vector<FeatureType> featureTypes;
      vector<TreeType> treeTypes;
      vector<AttributeType> attributeType;
      vector<string> thresholdsValues;
      vector<ApOrigPos> apOrigPos;
      bool dapWeightFlags;

      OptRanges copyBands;
      vector<OptRanges> selectedBands;
      vector<OptSizes> haarSizes, statSizes;

      string cfgName, loadName, saveName;

    public:
      DimChanel spectralDepth;
      bool mixedBandFlag;
      boost::program_options::options_description description;

      OptChanelParser ();

      void parse (OptChanel &optChanel, IImage &inputImage, boost::program_options::basic_parsed_options<char> &parsed);
    };

    // ================================================================================
  } // triskele
} // obelix

#endif //_Obelix_Triskele_OptChanelParser_hpp
