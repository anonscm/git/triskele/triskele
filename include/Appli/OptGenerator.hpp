#ifndef _Obelix_Triskele_OptGenerator_hpp
#define _Obelix_Triskele_OptGenerator_hpp

#include <iostream>
#include <vector>

#include "triskeleBase.hpp"
#include "ArrayTree/GraphWalker.hpp"
#include "IImage.hpp"
#include "Appli/OptCrop.hpp"
#include "Appli/OptRanges.hpp"
#include "CompAttribute.hpp"

namespace obelix {
  namespace triskele {

    // ================================================================================
    class OptGenerator {
    public:
      bool		debugFlag;
      IImage		inputImage;
      IImage		outputImage;

      DimChanel		spectralDepth;
      bool		mixedBandFlag;
      OptRanges		selectedBand;
      DimSortCeil	countingSortCeil;
      vector<DimImg>	areaThresholds;
      vector<double>	levelThresholds, sdThresholds, moiThresholds;
      DimCore		coreCount;
      bool		maxTreeFlag, minTreeFlag, tosTreeFlag, alphaTreeFlag;
      bool		border, oneBand;
      Connectivity	connectivity;
      FeatureType	featureType;

      OptCrop		optCrop;

      OptGenerator ();
      OptGenerator (int argc, char** argv);

      void usage (string msg = "", bool hidden = false);
      void parse (int argc, char** argv);
    };

    // ================================================================================
  } // triskele
} // obelix

#endif //_Obelix_Triskele_OptGenerator_hpp
