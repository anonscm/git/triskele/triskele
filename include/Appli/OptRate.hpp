#ifndef _Obelix_OptRate_hpp
#define _Obelix_OptRate_hpp

#include <iostream>
#include <string>
#include <boost/lexical_cast.hpp>

namespace obelix {
  using namespace std;

  // ================================================================================
  class OptRate {
    string initOptRate;
    bool percent;
    double rate;

  public:
    bool	isPercent () const;
    double	getValue () const;
    string	getRate () const;

    template<typename T>
    inline T	getRate (const T &value) const;
    template<typename T>
    inline T	getMoreRate (const T &value) const;
    template<typename T>
    inline T	getLessRate (const T &value) const;

    OptRate (const string &option = "");
    void init (const string &option);

    void check (const string &optionName, const bool &less100percent) const;
  };

  ostream &operator << (ostream &out, const OptRate &optRate);
  istream &operator >> (istream &in, OptRate &optRate);

  // ================================================================================
#include "OptRate.tpp"
} // obelix

#endif //_Obelix_OptRate_hpp
