#ifndef _Obelix_TextWin_hpp
#define _Obelix_TextWin_hpp

#include "triskeleBase.hpp"

namespace obelix {
  using namespace triskele;

    // ================================================================================
  class TextWin {
  private:
    DimSideImg wl, wr, ht, hb;
    DimSideImg widthR, heightB;
    double sCount;
    DimImg l, r, t, b, lt, rt, lb, rb;

    // ========================================
  public:
    /*! size = image size, sSize = parttern size */
    TextWin (const Size &size, const Size &sSize)
      : wl (sSize.width >> 1), wr (sSize.width - wl),
	ht (sSize.height >> 1), hb (sSize.height - ht),
	widthR (size.width-wr), heightB (size.height-hb),
	sCount (sSize.width*sSize.height),
	l (wl), r (wr), t (ht*size.width), b (hb*size.width),
	lt (t+l), rt (t-r), lb (b-l), rb (b+r)
    {
      DEF_LOG ("Haar::Haar", "size:" << size <<
	       " wl:" << wl << " wr:" << wr << " ht:" << ht << " hb:" << hb <<
	       " widthR:" << widthR << " heightB:" << heightB << " sCount:" << sCount <<
	       " l:" << l << " r:" << r << " t:" << t << " b:" << b <<
	       " lt:" << lt << " rt:" << rt << " lb:" << lb << " rb:" << rb);
    }

    // ========================================
    // c = x + y*width
    double getHx (const double* iiValues, const DimSideImg &x, const DimSideImg &y, const DimImg &c) const {
      double hx = 0;
      if (y >= ht) {
	if (x >= wl)
	  hx += iiValues [c - lt];
	hx -= 2 * iiValues [c - t];
	if (x < widthR)
	  hx += iiValues [c - rt];
      }
      if (y < heightB) {
	if (x >= wl)
	  hx -= iiValues [c + lb];
	hx += 2 * iiValues [c + b];
	if (x < widthR)
	  hx -= iiValues [c + rb];
      }
      hx /= sCount;
      return hx;
    }

    // ========================================
    // c = x + y*width
    double getHy (const double* iiValues, const DimSideImg &x, const DimSideImg &y, const DimImg &c) const {
      double hy = 0;
      if (x >= wl) {
	if (y >= ht)
	  hy -= iiValues [c - lt];
	hy += 2 * iiValues [c - l];
	if (y < heightB)
	  hy -= iiValues [c + lb];
      }
      if (x < widthR) {
	if (y >= ht)
	  hy += iiValues [c - rt];
	hy -= 2 * iiValues [c + r];
	if (y < heightB)
	  hy += iiValues [c + rb];
      }
      hy /= sCount;
      return hy;
    }

    // ========================================
    // c = x + y*width
    void getMeanStdEnt (double &mean, double &std, double &ent,
			const double* iiValues, const double* doubleiiValues,
			const DimSideImg &x, const DimSideImg &y, const DimImg &c) const {
      double area = 0, doubleArea = 0;
      if (x < widthR && y < heightB) { // bottom-right
	area += iiValues [c + rb];
	doubleArea += doubleiiValues [c + rb];
      }
      if (x >= wl && y >= ht) { // top-left
	area += iiValues [c - lt];
	doubleArea += doubleiiValues [c - lt];
      }
      if (x < widthR && y >= ht) { // top-right
	area -= iiValues [c - rt];
	doubleArea -= doubleiiValues [c - rt];
      }
      if (x >= wl && y < heightB) { // bottom-left
	area -= iiValues [c + lb];
	doubleArea -= doubleiiValues [c + lb];
      }
      mean = area/sCount;
      std = (doubleArea - area*mean)/sCount;
      ent = 1 - doubleArea/(area*area + 0.000000000001);
    }

    // ================================================================================
  };
} // obelix

#endif // _Obelix_TextWin_hpp
