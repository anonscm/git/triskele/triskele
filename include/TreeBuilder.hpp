#ifndef _Obelix_Triskele_TreeBuilder_hpp
#define _Obelix_Triskele_TreeBuilder_hpp

#include "triskeleBase.hpp"
#include "Tree.hpp"

namespace obelix {
  namespace triskele {

    // ================================================================================
    class TreeBuilder {    
    public:
    
      static void	buildTree (Tree &tree, TreeBuilder &builder);
      virtual void	buildTree (Tree &tree);

    protected:
      TreeBuilder ();
      virtual ~TreeBuilder ();
      // Used to set the attributes below with the tree
      void		setTreeSize  (Tree &tree, const Size &size);
      void		updateTranscient (Tree &tree);
      void		setNodeCount (Tree &tree, DimNodeId nodeCount);
      inline DimImg	getCompCount () const;

      // Attributes corresponding to the tree, used to make the construction easier
      DimImg leafCount;
      DimNodeId nodeCount;
      /*! Pointers on the parents of each leafs / nodes */
      DimImg *leafParents, *compParents;

      /*! Pointers on the children and count how many children a parents have */
      DimNodeId *children, *childrenStart;
    };

    // ================================================================================
#include "TreeBuilder.tpp"
  } // triskele
} // obelix

#endif // _Obelix_Triskele_TreeBuilder_hpp
