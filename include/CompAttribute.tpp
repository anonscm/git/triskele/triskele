#ifndef _Obelix_Triskele_CompAttribute_tpp
#define _Obelix_Triskele_CompAttribute_tpp

// ================================================================================
template<typename AttrT>
inline ostream &
CompAttribute<AttrT>::print (ostream &out, const string &msg) const {
  cout << "values: " << msg << endl;
  const Size doubleSize (tree.getSize().width, 2*tree.getSize ().height);
  cout << printMap (&values[0], doubleSize, tree.getCompCount ()) << endl << endl;
  return out;
}

// ================================================================================
template<typename AttrT>
template<typename CumpFunctPSE>
inline void
CompAttribute<AttrT>::computeSameCompLevel (const CumpFunctPSE &cumpFunctPSE) const {
  const vector<DimImg> &weightBounds (CompAttribute<AttrT>::tree.getWeightBounds ());
  unsigned int coreCount = CompAttribute<AttrT>::tree.getCoreCount ();
  DEF_LOG ("CompAttribute::computeSameCompLevel", "coreCount:" << coreCount);
  if (!weightBounds.size () || CompAttribute<AttrT>::tree.getCompCount ()/weightBounds.size () < coreCount) {
    LOG ("CompAttribute::computeSameCompLevel: no thread");
    CompAttribute<AttrT>::tree.forEachComp (cumpFunctPSE);
    return;
  }
  DimImg first = weightBounds [0];
  for (DimImg curBound = 1; curBound < weightBounds.size (); curBound++) {
    DimImg next = weightBounds [curBound];
    dealThread (next-first, coreCount, [&first, &cumpFunctPSE] (const DimCore &threadId, const DimImg &minVal, const DimImg &maxVal) {
	const DimImg end (maxVal+first);
	for (DimImg parentId = minVal+first; parentId < end; ++parentId)
	  cumpFunctPSE (parentId);
      });
    first = next;
  }
}

// ================================================================================
#endif // _Obelix_Triskele_CompAttribute_tpp
