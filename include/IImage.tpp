#ifndef _Obelix_IImage_tpp
#define _Obelix_IImage_tpp

// ================================================================================
template<typename PixelT>
inline const Size &
Raster<PixelT>::getSize () const {
  return size;
}

template<typename PixelT>
inline const PixelT *
Raster<PixelT>::getPixels () const {
  return &pixels[0];
}

template<typename PixelT>
inline PixelT *
Raster<PixelT>::getPixels () {
  return &pixels[0];
}

template<typename PixelT>
inline const PixelT *
Raster<PixelT>::getFrame (const DimChanel &frameId) const {
  return &pixels[size.getPixelsCount ()/size.length*frameId];
}

template<typename PixelT>
inline PixelT *
Raster<PixelT>::getFrame (const DimChanel &frameId) {
  return &pixels[size.getPixelsCount ()/size.length*frameId];
}

template<typename PixelT>
inline vector<PixelT> &
Raster<PixelT>::getPixelsVector () {
  return pixels;
}

template<typename PixelT>
inline const vector<PixelT> &
Raster<PixelT>::getPixelsVector () const {
  return pixels;
}

template<typename PixelT>
inline DimNodeId
Raster<PixelT>::pointIdx (const Point &p) const {
  return point2idx (size, p);
}

template<typename PixelT>
inline PixelT
Raster<PixelT>::getValue (const DimImg &idx) const {
  return pixels[idx];
}

template<typename PixelT>
inline PixelT
Raster<PixelT>::getValue (const Point &point) const {
  return pixels [pointIdx (point)];
}


// ================================================================================
inline const string &
IImage::getFileName () const {
  return fileName;
}

inline const Size &
IImage::getSize () const {
  return size;
}

inline const DimChanel &
IImage::getBandCount () const {
  return bandCount;
}

inline GDALDataType
IImage::getDataType () const {
  return dataType;
}

inline const bool
IImage::isRead () const {
  return read;
}

inline const bool
IImage::isEmpty () const {
  return size.isNull ();
}

// ================================================================================
#endif // _Obelix_IImage_tpp
