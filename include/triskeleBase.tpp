#ifndef _Obelix_Base_tpp
#define _Obelix_Base_tpp

// ================================================================================
inline DimImg point2idx (const Size &size, const Point &p) {
  return DimImg (p.x) +  DimImg (size.width)*(DimImg (p.y) + DimImg (size.height)*DimImg (p.z));
}

inline Point idx2point (const Size &size, const DimImg &idx) {
  const DimImg line = idx / size.width;
  return Point (idx % size.width, line % size.height, line / size.height);
}

// ================================================================================
inline bool
Size::isNull () const {
  return !width || !height || !length;
}

inline bool
operator== (const Size &s1, const Size &s2) {
  return s1.width == s2.width && s1.height == s2.height && s1.length == s2.length;
}

inline bool
operator!= (const Size &s1, const Size &s2) {
  return ! (s1 == s2);
}

// ================================================================================
inline DimImg
Rect::relIdx (const Point &p) const {
  return DimImg (p.x-point.x) +  DimImg (size.width)*(DimImg (p.y-point.y) + DimImg (size.height)*DimImg (p.z-point.z));
}
inline DimImg
Rect::absIdx (const DimImg &relIdx, const Size &fullSize) const {
  DimImg line = relIdx / size.width;
  return (relIdx % size.width)+point.x + fullSize.width*(line%size.height + point.y) + fullSize.height*(line/size.length + point.z);
}

template<typename T>
inline void
Rect::cpToTile (const Size &size, const T* src, vector<T> &dst) const {
  // XXX no border
  const DimImg maxCount = size.getPixelsCount ();
  dst.resize (maxCount);
  for (DimImg idx = 0; idx < maxCount; ++idx) {
    dst [idx] = src [absIdx (idx, size)];
  }
}
template<typename T>
inline void
Rect::cpFromTile (const Size &size, const vector<T> &src, T *dst) const {
  // XXX no border
  const DimImg maxCount = size.getPixelsCount ();
  for (DimImg idx = 0; idx < maxCount; ++idx) {
    dst [absIdx (idx, size)] = src [idx];
  }
}

template<typename T>
inline void
Rect::cpFromTileMove (const Size &size, const vector<T> &src, T *dst, const T &move) const {
  // XXX no border
  const DimImg maxCount = size.getPixelsCount ();
  DEF_LOG ("Rect::cpFromTileTrans", "rectSize:" << size << " move:" << move);
  for (DimImg idx = 0; idx < maxCount; ++idx)
    dst [absIdx (idx, size)] = src [idx] + move;
  SMART_LOG ("tile:"<< *this << " src/dst:" << endl
	     << printMap (&src[0], size, 0) << endl
	     << printMap (dst, size, 0));
}

inline bool
Rect::isNull () const {
  return size.isNull ();
}

// ================================================================================
template <typename T>
inline
CPrintMap<T>::CPrintMap (const T *map, const Size &size, const DimNodeId &maxValues)
  : map (map),
    size (size),
    maxValues (maxValues == 0 ? ((DimNodeId) size.getPixelsCount ()) : maxValues) {
}

// XXX 3D
template <typename T>
inline ostream &
CPrintMap<T>::print (ostream &out) const {
  if (size.width > printMapMaxSide || size.height > printMapMaxSide || size.length > printMapMaxSide) {
    return out << "map too big to print!" << endl;
  }
  const T *map2 = map;
  DimNodeId countDown = maxValues;
  for (DimSideImg z = 0; z < size.length; ++z) {
    for (DimSideImg y = 0; y < size.height; ++y) {
      for (DimSideImg x = 0; x < size.width; ++x, ++map2, --countDown) {
	if (!countDown)
	  return out;
	if ((DimImg (*map2)) == DimImg_MAX)
	  out << "  M ";
	else
	  // XXX if T == uint8_t => print char :-(
	  out << setw(3) << *map2 << " ";
      }
      out << endl;
    }
    out << endl;
  }
  return out;
}

template <typename T>
inline CPrintMap<T>
printMap (const T *map, const Size &size, const DimNodeId &maxValues) {
  return CPrintMap<T> (map, size, maxValues);
}

// ================================================================================
#endif // _Obelix_Base_tpp
