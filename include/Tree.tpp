#ifndef _Obelix_Triskele_Tree_tpp
#define _Obelix_Triskele_Tree_tpp

// ================================================================================
inline DimNodeId *
Tree::getChildSum () {
  return &childrenStart[0];
}

inline void
Tree::setNodeCount (const DimImg &newNodeCount) {
  nodeCount = newNodeCount;
}
inline void
Tree::setSize (const Size &newSize) {
  size = newSize;
}
    
inline DimCore
Tree::getCoreCount () const {
  return coreCount;
}

inline State
Tree::getState () const {
  return state;
}

inline Size
Tree::getSize () const {
  return size;
}

inline const DimSideImg &
Tree::getLeafCount () const {
  return leafCount;
}
inline const DimNodeId &
Tree::getNodeCount () const {
  return nodeCount;
}
inline DimImg
Tree::getCompCount () const {
  return (DimImg) (nodeCount-leafCount);
}
inline DimNodeId
Tree::getNodeRoot () const {
  return nodeCount-1;
}
inline DimImg
Tree::getCompRoot () const {
  return (DimImg) (nodeCount-1-leafCount);
}
inline bool
Tree::isLeaf (const DimNodeId &nodeId) const {
  return nodeId < leafCount;
}
inline DimImg
Tree::getLeafId (const DimNodeId &nodeId) const {
  return (DimImg) nodeId;
}
inline DimImg
Tree::getCompId (const DimNodeId &nodeId) const {
  return (DimImg) (nodeId-leafCount);
}
inline DimNodeId
Tree::getChild (const DimImg &compId, const DimImg &childId) const {
  return children [childrenStart [compId]+childId];
}

inline const DimImg &
Tree::getParent (const DimNodeId &nodeId) const {
  return leafParents[nodeId];
}
inline const DimImg &
Tree::getLeafParent (const DimImg &leafId) const {
  return leafParents[leafId];
}
inline const DimImg &
Tree::getCompParent (const DimImg &compId) const {
  return compParents[compId];
}

inline const DimImg &
Tree::getChildrenCount (const DimImg &compId) const {
  return childrenStart[compId];
}
inline const DimNodeId &
Tree::getChildren (const DimImg &childId) const {
  return children[childId];
}

inline const vector<DimImg> &
Tree::getWeightBounds () const {
  return weightBounds;
}
inline vector<DimImg> &
Tree::getWeightBounds () {
  return weightBounds;
}

// ================================================================================
template<typename FuncToApply>
inline void
Tree::forEachLeaf (const FuncToApply &f /* f (DimImg leafId) */) const {
  for (DimImg leafId = 0; leafId < leafCount; ++leafId)
    f (leafId);
}

template<typename FuncToApply>
inline void
Tree::forEachComp (const FuncToApply &f /* f (DimImg compId) */) const {
  const DimImg compCount = getCompCount ();
  for (DimImg compId = 0; compId < compCount; ++compId)
    f (compId);
}

template<typename FuncToApply>
inline void
Tree::forEachNode (const FuncToApply &f /* f (NodeDimImg nodeId) */) const {
  for (DimNodeId nodeId = 0; nodeId < nodeCount; ++nodeId)
    f (nodeId);
}

template<typename FuncToApply>
inline void
Tree::forEachChild (const DimNodeId &parentId, const FuncToApply &f /* f (DimNodeId childId) */) const {
  DimNodeId minChild = childrenStart[parentId], maxChild = childrenStart[parentId+1];
  for (DimNodeId childId = minChild; childId < maxChild; ++childId)
    f (children[childId]);
}
template<typename FuncToApply>
inline void
Tree::forEachChildTI (const DimNodeId &parentId, const FuncToApply &f /* f (bool isLeaf, DimImg childId) */) const {
  DimNodeId minChild = childrenStart[parentId], maxChild = childrenStart[parentId+1];
  for (DimNodeId childId = minChild; childId < maxChild; ++childId) {
    const DimNodeId &child (getChildren (childId));
    const bool isLeaf = child < leafCount;
    if (child >= getNodeCount ()) {
      cerr << "coucou Tree::forEachChildTI parentId:" << parentId << " compCount:" << getCompCount () << " minChild:" << minChild << " maxChild:" << maxChild << " childId:" << childId << " child:" << child << endl; 
    }
    BOOST_ASSERT (child < getNodeCount ());
    f (isLeaf, isLeaf ? (DimImg) child : (DimImg) (child-leafCount));
  }
}

// ================================================================================
#endif // _Obelix_Triskele_Tree_tpp
