#ifndef _Obelix_Triskele_TreeBuilder_tpp
#define _Obelix_Triskele_TreeBuilder_tpp

inline DimImg
TreeBuilder::getCompCount () const {
  return nodeCount-leafCount;
}

#endif // _Obelix_Triskele_TreeBuilder_tpp
