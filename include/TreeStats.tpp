#ifndef _Obelix_Triskele_TreeStats_tpp
#define _Obelix_Triskele_TreeStats_tpp

// ========================================
inline void
TreeStats::addDim (const TreeType &treeType, const DimImg& leafCount, const DimImg& compCount) {
  BOOST_ASSERT (treeType <= ALPHA);
  leavesStats[treeType] (leafCount);
  compStats[treeType] (compCount);
}

inline void
TreeStats::addTime (const TimeType &timeType, const double &duration) {
  BOOST_ASSERT (timeType < TimeTypeCard);
  timeStats[timeType] (duration);
}

inline const TreeStatsDouble &
TreeStats::getTimeStats (const TimeType &timeType) {
  BOOST_ASSERT (timeType < TimeTypeCard);
  return timeStats [timeType];
}

// ========================================


#endif // _Obelix_Triskele_TreeStats_tpp
