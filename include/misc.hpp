#ifndef _Obelix_Misc_hpp
#define _Obelix_Misc_hpp

#include <cstdint>
#include <iostream>

#include "IImage.hpp"

#include "triskeleBase.hpp"
#include "ArrayTree/triskeleArrayTreeBase.hpp"

namespace obelix {

  using namespace std;
  using namespace triskele;

  // ================================================================================
  template <typename T>
  ostream	&operator << (ostream& out, const vector<T> &v);

  inline uint8_t ones64 (uint64_t x);
  inline uint8_t size64 (uint64_t x);

  string	getOsName ();
  uint16_t	getCols ();
  string	getCurrentTime (const string &format = "%Y-%m-%d %H:%M:%S");
  string	ns2string (const double &delta);
  string	addExtensionIfNone (const string &fileName, const string &ext);
  string	replaceExtension (const string &fileName, const string &ext);

  template<typename PixelT>
  void	computeNdvi (const Raster<PixelT> &inputRaster, Raster<PixelT> &ndviRaster, const bool &minChanel, const bool &reverse);

  template<typename PixelT>
  void	computeSobel (const Raster<PixelT> &inputRaster, Raster<PixelT> &sobelRaster, vector<vector<double> > gTmp, const unsigned int &coreCount);

  // ================================================================================
} // obelix

#include "misc.tpp"

#define SHOW_OPTION(label, value) << setw (leftMarging) << right << label << ": " << value << endl
#define SHOW_OPTION_FLAG(label, value) SHOW_OPTION (label, (value ? "true" : "false"))

#endif // _Obelix_Misc_hpp
