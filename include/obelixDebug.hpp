#ifndef _Obelix_Debug_hpp
#define _Obelix_Debug_hpp

#include <iostream>
#include <string>


#ifdef ENABLE_SMART_LOG

#ifndef SMART_DEF_LOG
#define SMART_DEF_LOG(name, expr) DEF_LOG (name, expr)
#endif

#ifndef SMART_LOG
#define SMART_LOG(expr) LOG (expr)
#endif

#ifndef SMART_LOG_EXPR
#define SMART_LOG_EXPR(expr) {if (::obelix::Log::debug) {expr;} }
#endif

#else

#ifndef SMART_DEF_LOG
#define SMART_DEF_LOG(name, expr)
#endif

#ifndef SMART_LOG
#define SMART_LOG(expr)
#endif

#ifndef SMART_LOG_EXPR
#define SMART_LOG_EXPR(expr)
#endif
#endif

#ifdef DISABLE_LOG

#ifndef DEF_LOG
#define DEF_LOG(name, expr)
#endif
#ifndef LOG
#define LOG(expr) {}
#endif

#ifndef DEBUG
#define DEBUG(expr) {}
#endif

#else

#ifndef DEF_LOG
#define DEF_LOG(name, expr) ::obelix::Log log (name); { if (::obelix::Log::debug) std::cerr << expr << std::endl << std::flush; }
#endif

#ifndef LOG
// _______________________________________________________ Don't forget DEF_LOG
#define LOG(expr) { if (::obelix::Log::debug) std::cerr << log << "| " << expr << std::endl << std::flush; }
#endif

#ifndef DEBUG
#define DEBUG(expr) { if (::obelix::Log::debug) std::cerr << expr << std::endl << std::flush; }
#endif

#endif

namespace obelix {
  // ================================================================================
  using namespace std;

  class Log {
    static size_t indent;
    string functName;
  public:
    static bool debug;

    Log (const string &functName);
    ~Log ();

    static string getLocalTimeStr ();
    friend ostream &operator << (ostream &out, const Log &log);
  };
  ostream &operator << (ostream &out, const Log &log);

  // ================================================================================
} // obelix

#endif //_Obelix_Debug_hpp
