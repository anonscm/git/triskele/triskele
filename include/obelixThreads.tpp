#ifndef _Obelix_Thread_tpp
#define _Obelix_Thread_tpp

using namespace std;

// ================================================================================
template<typename DimImgT, typename FunctThreadMinMax>
inline void
dealThread (const DimImgT &maxId, DimCore coreCount, const FunctThreadMinMax &functThreadMinMax/* functThreadMinMax (threadId, minVal, maxVal) */) {
  //DEF_LOG ("dealThreadBound", "coreCount:" << coreCount << " maxId:" << maxId);
  if (!maxId || !coreCount)
    return;
  vector<DimImgT> maxIds = getDealThreadBounds (maxId, coreCount);
  coreCount = maxIds.size ()-1;
  if (coreCount == 1) {
    functThreadMinMax (0, 0, maxId);
    return;
  }
    
#ifdef THREAD_DISABLE
  for (DimCore idCopyValInThread = 0; idCopyValInThread < coreCount; ++idCopyValInThread) {
    functThreadMinMax (idCopyValInThread, maxIds[idCopyValInThread], maxIds[idCopyValInThread+1]);
  }
#elif INTEL_TBB_THREAD
  using namespace tbb;
#pragma warning(disable: 588)
  parallel_for (size_t (0), coreCount, [&maxIds, &functThreadMinMax] (size_t idCopyValInThread) {
      functThreadMinMax (idCopyValInThread, maxIds[idCopyValInThread], maxIds[idCopyValInThread+1]);
    });
#else /* BOOST thread */
  vector<boost::thread> tasks;
  for (DimCore idCopyValInThread = 0; idCopyValInThread < coreCount; ++idCopyValInThread) {
    tasks.push_back (boost::thread ([/*no ref!!!*/idCopyValInThread, &maxIds, &functThreadMinMax] () {
	  functThreadMinMax (idCopyValInThread, maxIds[idCopyValInThread], maxIds[idCopyValInThread+1]);
	}));
  }
  for (DimCore i = 0; i < coreCount; ++i)
    tasks[i].join ();
#endif
}

// ================================================================================
template <typename DimImgT, class OutputIterator, class T>
inline void dealThreadFill_n (const DimImgT &maxId, DimCore coreCount, OutputIterator first, const T& val) {
  dealThread (maxId, coreCount, [&first, &val] (const DimCore &threadId, const DimImgT &minVal, const DimImgT &maxVal) {
      fill_n (first+minVal, maxVal-minVal, val);
    });
}

// ================================================================================
template<typename DimImgT, typename WeightT, typename WeightFunct, typename CmpFunct, typename CallFunct>
inline void
callOnSortedSets (const vector<DimImgT> &sizes,
		  const WeightFunct &getWeight/* getWeight (vectId, itemId) */,
		  CmpFunct isWeightInf/* isWeightInf (w1, w2) */,
		  const CallFunct &callIdId/* callIdId (vectId, itemId) */) {
  DimImgT size = sizes.size ();
  DEF_LOG ("callOnSortedSets", "size:" << size);
  if (!size)
    return;
  vector<DimImgT> vectIds (size, 0);
  vector<DimImgT> vectCounts (sizes);
  // get min
  bool found = false;
  DimImgT minVectIdx = 0;
  WeightT maxWeight = 0;
  for (DimImgT vectId = 0; vectId < size; ++vectId) {
    if (!vectCounts [vectId])
      continue;
    WeightT tmpWeight = getWeight (vectId, 0);
    if (found && !isWeightInf (tmpWeight, maxWeight))
      continue;
    minVectIdx = vectId;
    maxWeight = tmpWeight;
    found = true;
  }
  LOG ("found:" << found << " minVectIdx:" << minVectIdx << " maxWeight:" << maxWeight);
  // loop
  for ( ; found; ) {
    // get next min
    found = false;
    DimImgT nextMinVectIdx = 0;
    WeightT nextMaxWeight = 0;
    for (DimImgT vectId = minVectIdx; ; ) {
      if (vectCounts [vectId]) {
	WeightT tmpWeight = getWeight (vectId, vectIds [vectId]);
	if (!isWeightInf (maxWeight, tmpWeight)) {
	  // maxWeight == tmpWeight
	  callIdId (vectId, vectIds [vectId]);
	  ++vectIds [vectId];
	  --vectCounts [vectId];
	  continue;
	}
	if (!found || isWeightInf (tmpWeight, nextMaxWeight)) {
	  nextMinVectIdx = vectId;
	  nextMaxWeight = tmpWeight;
	  found = true;
	}
      }
      vectId = (vectId+1)%size;
      if (vectId == minVectIdx)
	break;
    }
    minVectIdx = nextMinVectIdx;
    maxWeight = nextMaxWeight;
  }
}

// ================================================================================
#endif // _Obelix_Thread_tpp
