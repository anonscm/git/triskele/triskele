#ifndef _Obelix_Base_hpp
#define _Obelix_Base_hpp

#include <iostream>
#include <iomanip>
#include <vector>
#include <algorithm>
#include <functional>
#include <cstdint>

#include "obelixDebug.hpp"

#define DimImg_MAX UINT32_MAX
#define DimSideImg_MAX UINT32_MAX
#define DimChanel_MAX UINT16_MAX

namespace obelix {

  using namespace std;

  // ================================================================================
  /*! sort size type */
  typedef uint8_t DimSortCeil;

  /*! Image size type */
  typedef uint32_t DimSideImg;

  /*! Image band type */
  typedef uint16_t DimChanel; // hyperspectral > 256

  /*! Number of pixels */
  typedef uint32_t DimImg;

  /*! Number of nodes */
  typedef uint32_t DimNodeId;

  /*! Compte le nombre de bits utilisés pour chaque nombre : bitCount[nombre] = nombre de bits*/
  extern int bitCount[];

  // ================================================================================
  struct Size;

  struct Point {
    DimSideImg x, y;
    DimChanel z;

    Point ();
    Point (const DimSideImg x, const DimSideImg y, const DimChanel z = 0);
    Point (const Size &size, const DimImg &idx);
  };
  ostream &operator << (ostream &out, const Point &p);
  extern const Point NullPoint;

  /*! Convertit un point d'un tableau (on peut imaginer une image à 2 dimension) en un index */
  inline DimImg point2idx (const Size &size, const Point &p);

  /*! Convertit un index d'un tableau (on peut imaginer une image à 2 dimension) en point correspondant à des coordonnées */
  inline Point idx2point (const Size &size, const DimImg &idx);

  // ================================================================================
  struct Size {
    DimSideImg width, height;
    DimChanel length;

    Size ();
    Size (const DimSideImg w, const DimSideImg h, const DimChanel l = 1);

    DimSideImg getPixelsCount () const;
    inline bool isNull () const;
  };
  inline bool operator== (const Size &s1, const Size &s2);
  inline bool operator!= (const Size &s1, const Size &s2);
  ostream &operator << (ostream &out, const Size &s);
  extern const Size NullSize;

  // ================================================================================
  struct Rect {
    Point point;
    Size size;

    Rect ();
    Rect (const Point &orig, const Size &size);

    inline DimImg relIdx (const Point &absPos) const;
    inline DimImg absIdx (const DimImg &relIdx, const Size &fullSize) const;

    template<typename T>
    inline void cpToTile (const Size &size, const T *src, vector<T> &dst) const;
    template<typename T>
    inline void cpFromTile (const Size &size, const vector<T> &src, T *dst) const;
    template<typename T>
    inline void cpFromTileMove (const Size &size, const vector<T> &src, T *dst, const T &move) const;

    inline bool isNull () const;
  };
  ostream &operator << (ostream &out, const Rect &r);
  extern const Rect NullRect;

  // ================================================================================
  static const DimSideImg printMapMaxSide = 25;

  /*! Affiche le contenu d'un tableau en spécifiant sa taille */
  template <typename T>
  struct CPrintMap {
    const T	*map;
    const Size	&size;
    DimNodeId	maxValues;
    inline CPrintMap (const T *map, const Size &size, const DimNodeId &maxValues = 0);
    inline ostream &print (ostream &out) const;
  };

  template <typename T>
  inline CPrintMap<T> printMap (const T *map, const Size &size, const DimNodeId &maxValues);

  template <typename T>
  inline ostream &operator << (ostream& out, const CPrintMap<T> &cpm) { return cpm.print (out); }

  // ================================================================================
#include "triskeleBase.tpp"

} // namespace obelix

#endif // _Obelix_Base_hpp
