#ifndef _Obelix_Triskele_Tree_hpp
#define _Obelix_Triskele_Tree_hpp

#include <vector>
#include <map>
#include <string>
#include <numeric>
#include <boost/thread.hpp>

#include "obelixThreads.hpp"
#include "triskeleBase.hpp"
#include "Border.hpp"

namespace obelix {
  namespace triskele {

    enum State {
      Void = 0,
      Initialized = 1 << 0,
      Constructed = 1 << 1
    };
  
// ================================================================================
    class Tree {
      friend class TreeBuilder;
    private:
      inline DimNodeId *getChildSum ();

    protected:
      /*! nb core for build and compute attributes */
      const DimCore coreCount;

      /*! Info about the picture */
      Size size;

      /*! Number of pixels / leaf */
      DimImg leafCount;
      /*! number of node */
      DimNodeId nodeCount; /* nodeCount = leafCount+compCount */

      /*! Pointers on the parents of each leafs / nodes */
      vector<DimImg> leafParents;
      DimImg *compParents;

      /*! Pointers on the children and count how many children a parents have */
      vector<DimImg> childrenStart;
      vector<DimNodeId> children;

      /*! Pointers of same weight in parents (+1 : last is root)*/
      vector<DimImg> weightBounds;

      /*! State of the tree */
      State state;

      /*! Allocate the size according to the size previously defined */
      void book (const DimImg &leafCount);

      /*! Free all the memory and set all pointers to nullptr */
      void free ();

    public:
      // Constructors, destructor and resize method (does the same as the constructors)

      // XXX test sans defaut treeCoreCount
      Tree (const Size &size, DimCore coreCount = boost::thread::hardware_concurrency ());
      Tree (DimCore coreCount = boost::thread::hardware_concurrency ());
      ~Tree ();

      /*! clear values according to the size defined */
      void clear ();

      void resize (const Size &size);

      // Setter for nodeCount and size
      inline void setNodeCount (const DimNodeId &newNodeCount);
      inline void setSize (const Size &newSize);
    
      inline DimCore getCoreCount () const;

      // Get the tree state and the size
      inline State getState () const;
      inline Size getSize () const;

      // Getters for tree structure
      inline const DimImg	&getLeafCount () const;
      inline const DimNodeId	&getNodeCount () const;
      inline DimImg		getCompCount () const;
      inline DimNodeId		getNodeRoot () const;
      inline DimImg		getCompRoot () const;
      inline bool		isLeaf (const DimNodeId &nodeId) const;
      inline DimImg		getLeafId (const DimNodeId &nodeId) const;
      inline DimImg		getCompId (const DimNodeId &nodeId) const;
      inline DimNodeId		getChild (const DimImg &compId, const DimImg &childId) const;

      inline const DimImg	&getParent (const DimNodeId &nodeId) const;
      inline const DimImg	&getLeafParent (const DimImg &leafId) const;
      inline const DimImg	&getCompParent (const DimImg &compId) const;

      inline const DimImg	&getChildrenCount (const DimImg &CompId) const;
      inline const DimNodeId	&getChildren (const DimImg &childId) const;

      inline const vector<DimImg> &getWeightBounds () const;
      inline vector<DimImg>	&getWeightBounds ();

      // Functions to apply to specific entities
      template<typename FuncToApply>
      void forEachLeaf (const FuncToApply &f /* f (DimImg leafId) */) const;

      template<typename FuncToApply>
      inline void forEachComp (const FuncToApply &f /* f (DimImg compId) */) const;

      template<typename FuncToApply>
      inline void forEachNode (const FuncToApply &f /* f (DimNodeId nodeId) */) const;

      template<typename FuncToApply>
      inline void forEachChild (const DimNodeId &parentId, const FuncToApply &f /* f (DimNodeId childId) */) const;
      template<typename FuncToApply>
      inline void forEachChildTI (const DimNodeId &parentId, const FuncToApply &f /* f (bool isLeaf, DimImg childId) */) const;


      bool compareTo (const Tree &tree, bool testChildren = false) const;

      void checkSpare (const Border& border) const;
      void check (const Border& border) const;
      // XXX void checkWeightCurve (bool incr) const;

      // nice ostream
      struct CPrintTree {
	const Tree &tree;
	const bool onRecord;
	const DimNodeId nodeCount;
	CPrintTree (const Tree &tree, DimNodeId nodeCount);
	ostream &print (ostream &out) const;
      };
      CPrintTree printTree (DimNodeId nodeCount = 0) const;
      friend ostream &operator << (ostream& out, const CPrintTree &cpt) { return cpt.print (out); }
    };

// ================================================================================
#include "Tree.tpp"
  } // triskele
} // obelix

#endif // _Obelix_Triskele_Tree_hpp
