#ifndef _Obelix_Gdal_hpp
#define _Obelix_Gdal_hpp

#ifdef NO_OTB
#include <gdal_priv.h>
#else
#include <otbGdalDataTypeBridge.h>
#endif

namespace obelix {
  namespace triskele {

    enum DataType {
      Unknown = 0,
      Byte    = 1 << 0,
      UInt16  = 1 << 1,
      Int16   = 1 << 2,
      UInt32  = 1 << 3,
      Int32   = 1 << 4,
      Float   = 1 << 5,
      Double  = 1 << 6
    };
  
    // ================================================================================
    template<typename Type>
    inline DataType
    getType (Type v) { return DataType::Unknown; };

    template<>
    inline DataType
    getType<uint8_t> (uint8_t v) { return DataType::Byte; };

    template<>
    inline DataType
    getType<uint16_t> (uint16_t v) { return DataType::UInt16; };

    template<>
    inline DataType
    getType<int16_t> (int16_t v) { return DataType::Int16; };

    template<>
    inline DataType
    getType<uint32_t> (uint32_t v) { return DataType::UInt32; };

    template<>
    inline DataType
    getType<int32_t> (int32_t v) { return DataType::Int32; };

    template<>
    inline DataType
    getType<float> (float v) { return DataType::Float; };

    template<>
    inline DataType
    getType<double> (double v) { return DataType::Double; };

    // ================================================================================
    inline GDALDataType
    toGDALType (DataType type) {
      switch (type) {
      case DataType::Byte:	return GDT_Byte;
      case DataType::UInt16:	return GDT_UInt16;
      case DataType::Int16:	return GDT_Int16;
      case DataType::UInt32:	return GDT_UInt32;
      case DataType::Int32:	return GDT_Int32;
      case DataType::Float:	return GDT_Float32;
      case DataType::Double:	return GDT_Float64;
      default:			return GDT_Unknown;
      }
    }

    // ================================================================================
  } // triskele
} // obelix

#endif // _Obelix_Gdal_hpp
