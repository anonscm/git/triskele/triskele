#ifndef _Obelix_Thread_hpp
#define _Obelix_Thread_hpp

#ifdef INTEL_TBB_THREAD
#include <tbb/tbb.h>
#endif
#include <boost/thread.hpp>

#include "obelixDebug.hpp"

namespace obelix {

  // ================================================================================
  /*! core size type */
  typedef size_t DimCore;

  template<typename DimImgT>
  std::vector<DimImgT>
  getDealThreadBounds (const DimImgT &maxId, const DimCore &coreCount);

  template<typename DimImgT, typename FunctThreadMinMax>
  inline void
  dealThread (const DimImgT &maxId, DimCore coreCount, const FunctThreadMinMax &functThreadMinMax/* functThreadMinMax (threadId, minVal, maxVal) */);

  template <typename DimImgT, class OutputIterator, class T>
  inline void
  dealThreadFill_n (const DimImgT &maxId, DimCore coreCount, OutputIterator first, const T& val);

  template<typename DimImgT, typename WeightT, typename WeightFunct, typename CmpFunct, typename CallFunct>
  inline void
  callOnSortedSets (const std::vector<DimImgT> &sizes,
		    const WeightFunct &getWeight/* getWeight (vectId, itemId) */,
		    CmpFunct isWeightInf/* isWeightInf (w1, w2) */,
		    const CallFunct &callIdId/* callIdId (vectId, itemId) */);

  // ================================================================================
#include "obelixThreads.tpp"
} // obelix


#endif // _Obelix_Thread_hpp
