#ifndef _Obelix_Triskele_Leader_tpp
#define _Obelix_Triskele_Leader_tpp

// ================================================================================
inline DimImg
Leader::find (DimImg a) const {
  BOOST_ASSERT (a < size);
  for (;;) {
    // On recherche le leader de a dans le tableau
    DimImg p = leaders[a];
    // Si p est size, p n'a pas de leader donc p était le leader de a
    if (p == DimImg_MAX)
      break;
    a = p;
  }
  BOOST_ASSERT (a < size);
  return a;
}

inline void
Leader::link (DimImg a, const DimImg &r) {
  BOOST_ASSERT (a < size);
  BOOST_ASSERT (r < size);
  for (; a != r;) {
    // On affecte une variable "temporaire" qui joue le rôle du leader de a
    DimImg p = leaders[a];
    //
    leaders[a] = r;
    if (p == DimImg_MAX)
      return;
    a = p;
  }
}

// ================================================================================

#endif // _Obelix_Triskele_Leader_tpp
