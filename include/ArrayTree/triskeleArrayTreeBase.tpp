#ifndef _Obelix_Triskele_ArrayBase_tpp
#define _Obelix_Triskele_ArrayBase_tpp

inline bool
getDecrFromTreetype (const TreeType &treeType) {
  switch (treeType) {
  case MIN: return true;
  default: return false; 
  }
}

// ================================================================================
template <typename WeightT>
inline void
swapEdge (Edge<WeightT> &a, Edge<WeightT> &b) {
  Edge<WeightT> c = a;
  a = b;
  b = c;
}

template <typename WeightT>
inline
CPrintEdge<WeightT>::CPrintEdge (const Edge<WeightT> &edge, const Size &size)
  : edge (edge),
    size (size) {
}

template <typename WeightT>
inline ostream &
CPrintEdge<WeightT>::print (ostream &out) const {
  // XXX if WeightT == uint8_t => print char :-(
  return out << edge.points[0] << ":" << edge.points[1] << ":" << edge.weight << ":"
	     << point2idx (size, edge.points[0]) << ":" << point2idx (size, edge.points[1]);
}

template <typename WeightT>
inline CPrintEdge<WeightT>
printEdge (const Edge<WeightT> &edge, const Size &size) {
  return CPrintEdge<WeightT> (edge, size);
}

// ================================================================================
template <typename WeightT>
inline
CPrintEdges<WeightT>::CPrintEdges (const Edge<WeightT> edges[], const Size &size, const DimEdge edgesCount)
  : edges (edges),
    size (size),
    edgesCount (edgesCount) {
}

template <typename WeightT>
inline
ostream &
CPrintEdges<WeightT>::print (ostream &out) const {
  for (int i = 0; i < edgesCount; ++i)
    out << printEdge (edges[i], size) << endl;
  return out;
}

template <typename WeightT>
inline CPrintEdges<WeightT>
printEdges (const Edge<WeightT> edges[], const Size &size, const DimEdge edgesCount) {
  return CPrintEdges<WeightT> (edges, size, edgesCount);
}

// ================================================================================
#endif // _Obelix_Triskele_ArrayBase_tpp
