#ifndef _Obelix_Triskele_Weight_hpp
#define _Obelix_Triskele_Weight_hpp

#include <iostream>
#include <typeinfo>

#include "triskeleBase.hpp"
#include "IImage.hpp"
#include "triskeleArrayTreeBase.hpp"

namespace obelix {
  namespace triskele {

    class GraphWalker;
      
    // ================================================================================
    /*! Structure définissant la base d'un poids entre les pixels */
    template <typename PixelT, typename WeightT> struct WeightBase {
    protected:
      const PixelT *pixels;
      Size size;
      inline DimNodeId pointIdx (const Point &p) const;
      PixelT halfPixel, maxPixel;
      inline void reset (const PixelT *pixels, const Size &size);
    public:
      const PixelT *getPixels () const { return pixels; }

      inline const Size &getSize () const;
      inline const PixelT &getValue (const DimImg &idx) const;
      inline const PixelT &getValue (const Point &p) const;
      inline bool getDecr () const;
      inline const PixelT &getMaxPixel () const;
      inline const PixelT &getHalfPixel () const;

      inline WeightBase (const PixelT *pixels, const Size &size);

      static inline bool isWeightInf (const WeightT &a, const WeightT &b);
      static inline bool isEdgeInf (const Edge<WeightT> &a, const Edge<WeightT> &b);
      static inline void sort (Edge<WeightT> *edges, DimEdge count);

      inline void copyPixelsBound (PixelT *leafAPTree,
				   const DimImg &minVal, const DimImg &maxVal) const;
      inline void weight2valueBound (PixelT *compAPTree, const WeightT *compWeights,
				     const DimImg &minVal, const DimImg &maxVal) const;
    };

    // ================================================================================
    /*! Structure intégrant la façon dont est géré un poids pour un MaxTree car la fonction calcul le poids minimum */
    template <typename PixelT, typename WeightT> struct MinWeight : public WeightBase<PixelT, WeightT> {
      typedef WeightBase<PixelT, WeightT> WB;

      inline bool getDecr () const;
      static inline bool isWeightInf (const WeightT &a, const WeightT &b);
      static inline bool isEdgeInf (const Edge<WeightT> &a, const Edge<WeightT> &b);
      static inline void sort (Edge<WeightT> *edges, DimEdge count);

      inline MinWeight ();
      inline MinWeight (const PixelT *pixels, const Size &size);
      inline MinWeight (const MinWeight &model, const PixelT *pixels, const Size &size);

      inline WeightT getWeight (const DimImg &idx) const;
      inline WeightT getWeight (const Point &a, const Point &b) const;
    };

    // ================================================================================
    /*! Structure intégrant la façon dont est géré un poids pour un MinTree car la fonction calcul le poids maximum. */
    template <typename PixelT, typename WeightT> struct MaxWeight : public WeightBase<PixelT, WeightT> {
      typedef WeightBase<PixelT, WeightT> WB;

      inline MaxWeight ();
      inline MaxWeight (const PixelT *pixels, const Size &size);
      inline MaxWeight (const MaxWeight &model, const PixelT *pixels, const Size &size);

      inline WeightT getWeight (const DimImg &idx) const;
      inline WeightT getWeight (const Point &a, const Point &b) const;
    };

    // ================================================================================
    /*! Structure intégrant la façon dont est géré un poids pour un AlphaTree car la fonction calcul la distance minimum (0 dans les feuilles comme MinTree donc comme MaxWeight) */
    template <typename PixelT, typename WeightT> struct DiffWeight : public WeightBase<PixelT, WeightT> {
      typedef WeightBase<PixelT, WeightT> WB;

      inline DiffWeight ();
      inline DiffWeight (const PixelT *pixels, const Size &size);
      inline DiffWeight (const DiffWeight &model, const PixelT *pixels, const Size &size);

      inline WeightT getWeight (const DimImg &idx) const;
      inline WeightT getWeight (const Point &a, const Point &b) const;
    };

    // ================================================================================
    /*! Structure intégrant la façon dont est géré un poids pour un TreeOfShape car la fonction calcul la distance à la médiane (distance maximum dans les feuilles donc comme MaxTree donc comme MinWeight) */
    template <typename PixelT, typename WeightT> struct MedianWeight : public WeightBase<PixelT, WeightT> {
      typedef WeightBase<PixelT, WeightT> WB;
    protected:
      PixelT median, thresholdPixel;
      WeightT thresholdWeight;

    public:
      inline bool getDecr () const;
      static inline bool isWeightInf (const WeightT &a, const WeightT &b);
      static inline bool isEdgeInf (const Edge<WeightT> &a, const Edge<WeightT> &b);
      static inline void sort (Edge<WeightT> *edges, DimEdge count);

      inline const PixelT &getMedian () const;
      inline const PixelT &getThresholdPixel () const;
      inline const WeightT &getThresholdWeight () const;

      inline WeightT value2weight (const PixelT &val) const;
      inline PixelT weight2value (const WeightT &weight) const;

      inline MedianWeight ();
      inline MedianWeight (const PixelT *pixels, const GraphWalker &graphWalker);
      inline MedianWeight (const MedianWeight &model, const PixelT *pixels, const Size &size);

      inline WeightT getWeight (const DimImg &idx) const;
      inline WeightT getWeight (const Point &a, const Point &b) const;
      inline void weight2valueBound (PixelT *compAPTree, const WeightT *compWeights,
				     const DimImg &minVal, const DimImg &maxVal) const;
    };
  
    // ================================================================================
  } // triskele
} // obelix

#include "GraphWalker.hpp"

namespace obelix {
  namespace triskele {
#include "Weight.tpp"
  } // triskele
} // obelix

#endif // _Obelix_Triskele_Weight_hpp
