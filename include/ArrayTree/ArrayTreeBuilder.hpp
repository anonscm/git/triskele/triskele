#ifndef _Obelix_Triskele_ArrayTreeBuilder_hpp
#define _Obelix_Triskele_ArrayTreeBuilder_hpp

#ifdef ENABLE_LOG
#include <iostream>
#endif // ENABLE_LOG

#include <vector>

#include "triskeleBase.hpp"
#include "triskeleSort.hpp"
#include "obelixThreads.hpp"
#include "TreeBuilder.hpp"
#include "Attributes/WeightAttributes.hpp"
#include "AttributeProfiles.hpp"
#include "IImage.hpp"
#include "GraphWalker.hpp"
#include "Weight.hpp"
#include "Leader.hpp"

namespace obelix {
  namespace triskele {

    // ================================================================================
    template <typename WeightT, typename PixelT>
    class ArrayTreeBuilder : public TreeBuilder {
    protected:
      DimCore			coreCount;
      const Raster<PixelT>	&raster;
      const GraphWalker		&graphWalker;
      TreeType			treeType;
      const DimSortCeil		countingSortCeil;
      Leader			leaders;

      // transcient
      DimNodeId			*childCount;
      DimImg			*newCompId;
      WeightT			*compWeights;
    public:
      ArrayTreeBuilder (const Raster<PixelT> &raster, const GraphWalker &graphWalker,
			const TreeType &treeType, const DimSortCeil &countingSortCeil = 2);
      ~ArrayTreeBuilder ();

      void	buildTree (Tree &tree, WeightAttributes<WeightT> &weightAttributes);
      void	setAttributProfiles (AttributeProfiles<PixelT> &attributeProfiles);
    protected:
      template<typename WeightFunct>
      void	buildTree (Tree &tree, const WeightFunct &weightFunct);

      template<typename WeightFunct>
      inline void
      setAttributProfiles (AttributeProfiles<PixelT> &attributeProfiles, const WeightFunct &weightFunct);

      template<typename WeightFunct>
      void	buildParents (Edge<WeightT> *edges, const WeightFunct &weightFunct, const Rect &tile, DimImg &topParent);

      template<typename WeightFunct>
      void	connectLeaf (DimImg a, DimImg b, const WeightT &weight, DimImg &parCount, const WeightFunct &weightFunct);

      void	unlinkParent (const DimImg &par);

      template<typename WeightFunct>
      void	connect3Comp (DimImg newComp, DimImg topA, DimImg topB, const WeightFunct &weightFunct);

      template<typename WeightFunct>
      void	connectComp (DimImg topA, DimImg topB, const WeightFunct &weightFunct);

      template<typename WeightFunct>
      DimImg	updateNewId (const vector<DimImg> &compBases, const vector<DimImg> &compTops, const WeightFunct &weightFunct);

      void	updateNewId (const DimImg curComp, DimImg &compCount);

      void	compress (const DimImg &compTop);

      DimImg	createParent (DimImg &topParent, const WeightT &weight, DimImg &childA, DimImg &childB);

      void	addChild (const DimImg &parent, DimImg &child);

      void	addChildren (const DimImg &parent, const DimImg &sibling);

      DimImg	findRoot (DimImg comp) const;

      template<typename WeightFunct>
      DimImg	findTopComp (const DimImg &comp, const WeightFunct &weightFunct) const;

      template<typename WeightFunct>
      DimImg
      findTopComp (DimImg comp, const WeightT &weight, const WeightFunct &weightFunct) const;

      DimImg	findNotLonelyTop (DimImg comp) const;
      DimImg	findMultiChildrenTop (DimImg comp) const;

      void	buildChildren ();

      // nice ostream
      struct CPrintComp {
	const ArrayTreeBuilder &atb;
	const DimImg &compId;
	CPrintComp (const ArrayTreeBuilder &atb, const DimImg &compId);
	ostream &print (ostream &out) const;
      };
      inline CPrintComp printComp (const DimImg &compId) const { return CPrintComp (*this, compId); }
      friend inline ostream &operator << (ostream& out, const CPrintComp &lc) { return lc.print (out); }
    };

    // ================================================================================
#include "ArrayTreeBuilder.tpp"

  } // triskele
} // obelix

#endif //  _Obelix_Triskele_ArrayTreeBuilder_hpp
