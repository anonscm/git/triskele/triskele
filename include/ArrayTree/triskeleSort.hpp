#ifndef _Obelix_Triskele_Sort_hpp
#define _Obelix_Triskele_Sort_hpp

#include <vector>
//#include <cstdlib>

namespace obelix {
  namespace triskele {

    // ================================================================================
    template <typename T>
    inline std::vector<T> getRandReduce(std::vector<T> index, T reduceCount);

    template <typename E, typename WeightT, typename DimImg, typename WeightFunct>
    inline DimImg mergeSort(E *dst, const std::vector<E *> &tab, const std::vector<DimImg> &tabSize, const WeightFunct &weightFunct);

    // ================================================================================
#include "triskeleSort.tpp"
  } // triskele
} // obelix

#endif // _Obelix_Triskele_Sort_hpp
