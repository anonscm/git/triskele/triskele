#ifndef _Obelix_Triskele_Weight_tpp
#define _Obelix_Triskele_Weight_tpp

// ================================================================================
template <typename PixelT, typename WeightT>
inline DimNodeId
WeightBase<PixelT, WeightT>::pointIdx (const Point &p) const {
  return point2idx (size, p);
}

template <typename PixelT, typename WeightT>
inline void
WeightBase<PixelT, WeightT>::reset (const PixelT *pixels, const Size &size) {
  this->pixels = pixels;
  this->size = size;
  maxPixel = std::numeric_limits<PixelT>::max ();
  // only if unsigned
  halfPixel = maxPixel / 2;
  if (typeid (PixelT) != typeid (float))
    halfPixel++;
}

template <typename PixelT, typename WeightT>
inline const Size &
WeightBase<PixelT, WeightT>::getSize () const {
  return size;
}

template <typename PixelT, typename WeightT>
inline const PixelT &
WeightBase<PixelT, WeightT>::getValue (const DimImg &idx) const {
  return pixels[idx];
}

template <typename PixelT, typename WeightT>
inline const PixelT &
WeightBase<PixelT, WeightT>::getValue (const Point &p) const {
  return getValue (pointIdx (p));
}

template <typename PixelT, typename WeightT>
inline bool
WeightBase<PixelT, WeightT>::getDecr () const {
  return false;
}

template <typename PixelT, typename WeightT>
inline const PixelT &
WeightBase<PixelT, WeightT>::getMaxPixel () const {
  return maxPixel;
}

template <typename PixelT, typename WeightT>
inline const PixelT &
WeightBase<PixelT, WeightT>::getHalfPixel () const {
  return halfPixel;
}


// ================================================================================
template <typename PixelT, typename WeightT>
inline
WeightBase<PixelT, WeightT>::WeightBase (const PixelT *pixels, const Size &size)
  : pixels (nullptr),
    size (NullSize) {
  reset (pixels, size);
    }

template <typename PixelT, typename WeightT>
inline bool
WeightBase<PixelT, WeightT>::isWeightInf (const WeightT &a, const WeightT &b) {
  return a < b;
}

template <typename PixelT, typename WeightT>
inline bool
WeightBase<PixelT, WeightT>::isEdgeInf (const Edge<WeightT> &a, const Edge<WeightT> &b) {
  return isWeightInf (a.weight, b.weight);
}

template <typename PixelT, typename WeightT>
inline void
WeightBase<PixelT, WeightT>::sort (Edge<WeightT> *edges, DimEdge count) {
  std::sort (edges, edges+count, isEdgeInf);
}

template <typename PixelT, typename WeightT>
inline void
WeightBase<PixelT, WeightT>::copyPixelsBound (PixelT *leafAPTree, const DimImg &minVal, const DimImg &maxVal) const {
  for (DimImg i = minVal; i < maxVal; ++i)
    leafAPTree[i] = pixels [i];
}

template <typename PixelT, typename WeightT>
inline void
WeightBase<PixelT, WeightT>::weight2valueBound (PixelT *compAPTree, const WeightT *compWeights,
						const DimImg &minVal, const DimImg &maxVal) const {
  //memcpy (compAPTree+minVal, compWeights+minVal, maxVal-minVal);
  for (DimImg compIdx = minVal; compIdx < maxVal; ++compIdx)
    compAPTree[compIdx] = compWeights[compIdx];
}

// ================================================================================
template <typename PixelT, typename WeightT>
inline bool
MinWeight<PixelT, WeightT>::getDecr () const {
  return true;
}

template <typename PixelT, typename WeightT>
inline bool
MinWeight<PixelT, WeightT>::isWeightInf (const WeightT &a, const WeightT &b) {
  return a > b;
}

template <typename PixelT, typename WeightT>
inline bool
MinWeight<PixelT, WeightT>::isEdgeInf (const Edge<WeightT> &a, const Edge<WeightT> &b) {
  return isWeightInf (a.weight, b.weight);
}

template <typename PixelT, typename WeightT>
inline void
MinWeight<PixelT, WeightT>::sort (Edge<WeightT> *edges, DimEdge count) {
  std::sort (edges, edges+count, isEdgeInf);
}

template <typename PixelT, typename WeightT>
inline
MinWeight<PixelT, WeightT>::MinWeight ()
  : WB (nullptr, NullSize) {
}

template <typename PixelT, typename WeightT>
inline
MinWeight<PixelT, WeightT>::MinWeight (const PixelT *pixels, const Size &size)
  : WB (pixels, size) {
}

template <typename PixelT, typename WeightT>
inline
MinWeight<PixelT, WeightT>::MinWeight (const MinWeight &model, const PixelT *pixels, const Size &size)
  : WB (pixels, size) {
}

template <typename PixelT, typename WeightT>
inline WeightT
MinWeight<PixelT, WeightT>::getWeight (const DimImg &idx) const {
  return WB::getValue (idx);
}

template <typename PixelT, typename WeightT>
inline WeightT
MinWeight<PixelT, WeightT>::getWeight (const Point &a, const Point &b) const {
  return std::min (getWeight (WB::pointIdx (a)),
		   getWeight (WB::pointIdx (b)));
}

// ================================================================================
template <typename PixelT, typename WeightT>
inline
MaxWeight<PixelT, WeightT>::MaxWeight ()
  : WB (nullptr, NullSize) {
}

template <typename PixelT, typename WeightT>
inline
MaxWeight<PixelT, WeightT>::MaxWeight (const PixelT *pixels, const Size &size)
  : WB (pixels, size) {
}

template <typename PixelT, typename WeightT>
inline
MaxWeight<PixelT, WeightT>::MaxWeight (const MaxWeight &model, const PixelT *pixels, const Size &size)
  : WB (pixels, size) {
}

template <typename PixelT, typename WeightT>
inline WeightT
MaxWeight<PixelT, WeightT>::getWeight (const DimImg &idx) const {
  return WB::getValue (idx);
}

template <typename PixelT, typename WeightT>
inline WeightT
MaxWeight<PixelT, WeightT>::getWeight (const Point &a, const Point &b) const {
  return std::max (getWeight (WB::pointIdx (a)),
		   getWeight (WB::pointIdx (b)));
}

// ========================================
template <typename PixelT, typename WeightT>
inline
DiffWeight<PixelT, WeightT>::DiffWeight ()
  : WB (nullptr, NullSize) {
}

template <typename PixelT, typename WeightT>
inline
DiffWeight<PixelT, WeightT>::DiffWeight (const PixelT *pixels, const Size &size)
  : WB (pixels, size) {
}

template <typename PixelT, typename WeightT>
inline
DiffWeight<PixelT, WeightT>::DiffWeight (const DiffWeight &model, const PixelT *pixels, const Size &size)
  : WB (pixels, size) {
}

template <typename PixelT, typename WeightT>
inline WeightT
DiffWeight<PixelT, WeightT>::getWeight (const DimImg &idx) const {
  return 0;
}

template <typename PixelT, typename WeightT>
inline WeightT
DiffWeight<PixelT, WeightT>::getWeight (const Point &a, const Point &b) const {
  PixelT va = WB::getValue (a), vb = WB::getValue (b);
  return std::max (va, vb) - std::min (va, vb);
}

// ================================================================================
template <typename PixelT, typename WeightT>
inline bool
MedianWeight<PixelT, WeightT>::getDecr () const {
  return true;
}

template <typename PixelT, typename WeightT>
inline bool
MedianWeight<PixelT, WeightT>::isWeightInf (const WeightT &a, const WeightT &b) {
  return a > b;
}

template <typename PixelT, typename WeightT>
inline bool
MedianWeight<PixelT, WeightT>::isEdgeInf (const Edge<WeightT> &a, const Edge<WeightT> &b) {
  return isWeightInf (a.weight, b.weight);
}

template <typename PixelT, typename WeightT>
inline void
MedianWeight<PixelT, WeightT>::sort (Edge<WeightT> *edges, DimEdge count) {
  std::sort (edges, edges+count, isEdgeInf);
}

template <typename PixelT, typename WeightT>
inline const PixelT &
MedianWeight<PixelT, WeightT>::getMedian () const {
  return median;
}

template <typename PixelT, typename WeightT>
inline const PixelT &
MedianWeight<PixelT, WeightT>::getThresholdPixel () const {
  return thresholdPixel;
}

template <typename PixelT, typename WeightT>
inline const WeightT &
MedianWeight<PixelT, WeightT>::getThresholdWeight () const {
  return thresholdWeight;
}

template <typename PixelT, typename WeightT>
inline WeightT
MedianWeight<PixelT, WeightT>::value2weight (const PixelT &val) const {
  if (median < WB::halfPixel) {
    if (val >= thresholdPixel)
      return val;
    return val < median ? (median-val)*2 - 1 : (val-median)*2;
  }
  if (val < thresholdPixel)
    return WB::maxPixel - val;
  return val < median ? (median-val)*2 - 1 : (val-median)*2;
}

template <typename PixelT, typename WeightT>
inline PixelT
MedianWeight<PixelT, WeightT>::weight2value (const WeightT &weight) const {
  if (median < WB::halfPixel) {
    if (weight >= thresholdWeight)
      return weight;
    return int (weight) % 2 ? median - 1 - weight/2 : median + weight/2;
  }
  if (weight > thresholdWeight)
    return WB::maxPixel - weight;
  return int (weight) % 2 ? median - weight/2 : median + weight/2;
}

// ================================================================================
template <typename PixelT, typename WeightT>
inline
MedianWeight<PixelT, WeightT>::MedianWeight ()
  : WB (nullptr, NullSize),
    median (0),
    thresholdPixel (0),
    thresholdWeight (0) {
}

template <typename PixelT, typename WeightT>
inline
MedianWeight<PixelT, WeightT>::MedianWeight (const PixelT *pixels, const GraphWalker &graphWalker)
  : WB (pixels, graphWalker.getSize ()) {
  median = graphWalker.getMedian<WeightT> (*this);
  thresholdPixel = median < WB::halfPixel ? median * 2 : WB::maxPixel - (WB::maxPixel-median) * 2;
  thresholdWeight = median < WB::halfPixel ? median * 2 : (WB::maxPixel-median) * 2;
}

template <typename PixelT, typename WeightT>
inline
MedianWeight<PixelT, WeightT>::MedianWeight (const MedianWeight &model, const PixelT *pixels, const Size &size)
  : WB (pixels, size),
    median (model.median),
    thresholdPixel (model.thresholdPixel),
    thresholdWeight (model.thresholdWeight) {
}

template <typename PixelT, typename WeightT>
inline WeightT
MedianWeight<PixelT, WeightT>::getWeight (const DimImg &idx) const {
  return value2weight (WB::getValue (idx));
}

template <typename PixelT, typename WeightT>
inline WeightT
MedianWeight<PixelT, WeightT>::getWeight (const Point &a, const Point &b) const {
  return std::min (getWeight (WB::pointIdx (a)),
		   getWeight (WB::pointIdx (b)));
}

template <typename PixelT, typename WeightT>
inline void
MedianWeight<PixelT, WeightT>::weight2valueBound (PixelT *compAPTree, const WeightT *compWeights,
						  const DimImg &minVal, const DimImg &maxVal) const {
  for (DimImg compIdx = minVal; compIdx < maxVal; ++compIdx)
    compAPTree[compIdx] = weight2value (compWeights[compIdx]);
}

// ================================================================================

#endif // _Obelix_Triskele_Weight_tpp
