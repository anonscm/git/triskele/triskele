#ifndef _Obelix_Triskele_GraphWalker_hpp
#define _Obelix_Triskele_GraphWalker_hpp

#include <boost/assert.hpp>
#include <algorithm>
#include <numeric>
#include <vector>

#include "obelixDebug.hpp"
#include "triskeleBase.hpp"
#include "triskeleArrayTreeBase.hpp"
#include "Border.hpp"

namespace obelix {
  namespace triskele {

    using namespace std;

    // ================================================================================
    /** GraphWalker */
    class GraphWalker {
    public:
      const Border &border;
      const Connectivity connectivity;

      const Size &getSize () const;

      GraphWalker (const Border &border, const Connectivity &connectivity = Connectivity::C4);

      DimEdge	edgeMaxCount () const;
      DimEdge	edgeMaxCount (const Size &tileSize) const;
      DimEdge	edgeBoundarySideMaxCount (const TileShape &boundaryAxe, const Size &size) const;

      void setTiles (const unsigned int &coreCount, vector<Rect> &tiles, vector<Rect> &boundaries, vector<TileShape> &boundaryAxes) const;
      void setTiles (const unsigned int &coreCount, const Rect &rect, vector<Rect> &tiles, vector<Rect> &boundaries, vector<TileShape> &boundaryAxes) const;
      void setMaxBounds (const vector<Rect> &tiles, vector<DimImg> &vertexMaxBounds, vector<DimImg> &edgesMaxBounds) const;

      template<typename Funct>
      inline DimImg forEachVertexIdx (const Funct &lambda /*lambda (idx)*/) const;
      template<typename Funct>
      inline DimImg forEachVertexIdx (const Rect &rect, const Funct &lambda /*lambda (idx)*/) const;
      template<typename Funct>
      inline DimImg forEachVertexPt (const Rect &rect, const Funct &lambda /*lambda (p)*/) const;

      template<typename Funct>
      inline DimEdge forEachEdgePt (const Rect &rect, TileShape tileShape, const Funct &lambda /*lambda (p0, p1)*/) const;

      template <typename WeightT, typename EdgeWeightFunction>
      inline WeightT getMedian (const EdgeWeightFunction &ef /*ef.getValue (idx)*/) const;

      template<typename WeightT, typename EdgeWeightFunction>
      inline DimEdge getEdges (const Rect &rect, TileShape tileShape, Edge<WeightT> *edges, const EdgeWeightFunction &ef /*ef.getWeight (p0, p1)*/) const;
      template<typename WeightT, typename EdgeWeightFunction>
      inline DimEdge getSortedEdges (const Rect &rect, TileShape tileShape, Edge<WeightT> *edges, const EdgeWeightFunction &ef /*ef.getWeight (p0, p1) ef.sort ()*/) const;
      template<typename WeightT, typename EdgeWeightFunction>
      inline DimEdge getCountingSortedEdges (const Rect &rect, TileShape tileShape, Edge<WeightT> *edges, const EdgeWeightFunction &ef /*ef.getWeight (p0, p1)*/) const;
    };

    // ================================================================================
#include "GraphWalker.tpp"
  
  } // triskele
} // obelix


#endif // _Obelix_Triskele_GraphWalker_hpp
