#ifndef _Obelix_Triskele_GraphWalker_tpp
#define _Obelix_Triskele_GraphWalker_tpp

// ================================================================================
template<typename Funct>
inline DimImg
GraphWalker::forEachVertexIdx (const Funct &lambda /*lambda (DimImg idx)*/) const {
  DimImg maxCount = border.getSize ().getPixelsCount ();
  DimImg vertexCount = 0;
  for (DimImg idx = 0; idx < maxCount; ++idx)
    if (!border.isBorder (idx)) {
      lambda (idx);
      ++vertexCount;
    }
  return vertexCount;
}

template<typename Funct>
inline DimImg
GraphWalker::forEachVertexIdx (const Rect &rect, const Funct &lambda /*lambda (DimImg idx)*/) const {
  const Size &size (border.getSize ());
  DimImg vertexCount = 0;
  DimChanel const lastZ = rect.point.z+rect.size.length;
  DimSideImg const lastX = rect.point.x+rect.size.width;
  DimSideImg const lastY = rect.point.y+rect.size.height;
  for (DimSideImg z = rect.point.z; z < lastZ; ++z)
    for (DimSideImg y = rect.point.y; y < lastY; ++y)
      for (DimSideImg x = rect.point.x; x < lastX; ++x) {
	const Point p (x, y, z);
	DimImg idx = point2idx (size, p);
	if (!border.isBorder (idx)) {
	  lambda (idx);
	  ++vertexCount;
	}
      }
  return vertexCount;
}

//
template<typename Funct>
inline DimImg
GraphWalker::forEachVertexPt (const Rect &rect, const Funct &lambda /*lambda (Point p)*/) const {
  const Size &size (border.getSize ());
  DimImg vertexCount = 0;
  DimChanel const lastZ = rect.point.z+rect.size.length;
  DimSideImg const lastX = rect.point.x+rect.size.width;
  DimSideImg const lastY = rect.point.y+rect.size.height;
  for (DimSideImg z = rect.point.z; z < lastZ; ++z)
    for (DimSideImg y = rect.point.y; y < lastY; ++y)
      for (DimSideImg x = rect.point.x; x < lastX; ++x) {
	const Point p (x, y, z);
	if (!border.isBorder (p)) {
	  lambda (p);
	  ++vertexCount;
	}
      }
  return vertexCount;
}

// ================================================================================
#define LAMBDA_IF_NOBORDER(p1, p2) if (!(border.isBorder (p1) || border.isBorder (p2))) { lambda (p1, p2); ++edgeCount; }

// ================================================================================
template<typename Funct>
inline DimEdge
GraphWalker::forEachEdgePt (const Rect &rect, TileShape tileShape, const Funct &lambda /*lambda (Point p0, Point p1)*/) const {
  DimImg edgeCount = 0;
  if (rect.isNull ())
    return edgeCount;
  const DimSideImg firstX = rect.point.x, endX = rect.point.x+rect.size.width;
  const DimSideImg firstY = rect.point.y, endY = rect.point.y+rect.size.height;
  const DimSideImg firstZ = rect.point.z, endZ = rect.point.z+rect.size.length;
  switch (tileShape) {

  case Volume:
    for (DimChanel z = firstZ; z < endZ; ++z) {
      if (connectivity & Connectivity::C4) {
	for (DimSideImg x = firstX+1; x < endX; ++x) {
	  Point pha1 (x-1, firstY, z), pha2 (x, firstY, z);
	  LAMBDA_IF_NOBORDER (pha1, pha2);
	}
	for (DimSideImg y = firstY+1; y < endY; ++y) {
	  Point pva1 (firstX, y-1, z), pva2 (firstX, y, z);
	  LAMBDA_IF_NOBORDER (pva1, pva2);
	  for (DimSideImg x = firstX+1; x < endX; ++x) {
	    Point ph1 (x-1, y, z), pv1 (x, y-1, z), p2 (x, y, z);
	    LAMBDA_IF_NOBORDER (ph1, p2);
	    LAMBDA_IF_NOBORDER (pv1, p2);
	  }
	}
      }
      if (connectivity & Connectivity::C6P)
	for (DimSideImg y = firstY+1; y < endY; ++y)
	  for (DimSideImg x = firstX+1; x < endX; ++x) {
	    Point p1 (x-1, y-1, z), p2 (x, y, z);
	    LAMBDA_IF_NOBORDER (p1, p2);
	  }
      if (connectivity & Connectivity::C6N)
	for (DimSideImg y = firstY+1; y < endY; ++y)
	  for (DimSideImg x = firstX+1; x < endX; ++x) {
	    Point p1 (x-1, y, z), p2 (x, y-1, z);
	    LAMBDA_IF_NOBORDER (p1, p2);
	  }
    }
    for (DimChanel z = firstZ+1; z < endZ; ++z) {
      if (connectivity & Connectivity::CT)
	for (DimSideImg x = firstX; x < endX; ++x)
	  for (DimSideImg y = firstY; y < endY; ++y) {
	    Point p1 (x, y, z-1), p2 (x, y, z);
	    LAMBDA_IF_NOBORDER (p1, p2);
	  }
      if (connectivity & Connectivity::CTP) {
	for (DimSideImg x = firstX+1; x < endX; ++x) {
	  Point pha1 (x-1, firstY, z-1), pha2 (x, firstY, z);
	  LAMBDA_IF_NOBORDER (pha1, pha2);
	  Point phb1 (x-1, firstY, z), phb2 (x, firstY, z-1);
	  LAMBDA_IF_NOBORDER (phb1, phb2);
	}
	for (DimSideImg y = firstY+1; y < endY; ++y) {
	  Point pva1 (firstX, y-1, z-1), pva2 (firstX, y, z);
	  LAMBDA_IF_NOBORDER (pva1, pva2);
	  Point pvb1 (firstX, y-1, z), pvb2 (firstX, y, z-1);
	  LAMBDA_IF_NOBORDER (pvb1, pvb2);
	  for (DimSideImg x = firstX+1; x < endX; ++x) {
	    Point pha1 (x-1, y, z), pva1 (x, y-1, z), pa2 (x, y, z-1);
	    LAMBDA_IF_NOBORDER (pha1, pa2);
	    LAMBDA_IF_NOBORDER (pva1, pa2);
	    Point phb1 (x-1, y, z-1), pvb1 (x, y-1, z-1), pb2 (x, y, z);
	    LAMBDA_IF_NOBORDER (phb1, pb2);
	    LAMBDA_IF_NOBORDER (pvb1, pb2);
	  }
	}
      }
      if (connectivity & Connectivity::CTN)
	for (DimSideImg y = firstY+1; y < endY; ++y)
	  for (DimSideImg x = firstX+1; x < endX; ++x) {
	    Point ppa1 (x-1, y-1, z), ppa2 (x, y, z-1);
	    LAMBDA_IF_NOBORDER (ppa1, ppa2);
	    Point pna1 (x-1, y, z), pna2 (x, y-1, z-1);
	    LAMBDA_IF_NOBORDER (pna1, pna2);
	    Point ppb1 (x-1, y-1, z-1), ppb2 (x, y, z);
	    LAMBDA_IF_NOBORDER (ppa1, ppa2);
	    Point pnb1 (x-1, y, z-1), pnb2 (x, y-1, z);
	    LAMBDA_IF_NOBORDER (pna1, pna2);
	  }
    }
    break;

  case Horizontal: {
    const DimSideImg y1 = rect.point.y-1, y2 = rect.point.y;
    for (DimChanel z = firstZ; z < endZ; ++z) {
      if (connectivity & Connectivity::C4)
	for (DimSideImg x = firstX; x < endX; ++x) {
	  Point p1 (x, y1, z), p2 (x, y2, z);
	  LAMBDA_IF_NOBORDER (p1, p2);
	}
      if (connectivity & Connectivity::C6P)
	for (DimSideImg x = firstX+1; x < endX; ++x) {
	  Point p1 (x-1, y1, z), p2 (x, y2, z);
	  LAMBDA_IF_NOBORDER (p1, p2);
	}
      if (connectivity & Connectivity::C6N)
	for (DimSideImg x = firstX+1; x < endX; ++x) {
	  Point p1 (x, y1, z), p2 (x-1, y2, z);
	  LAMBDA_IF_NOBORDER (p1, p2);
	}
    }
    for (DimChanel z = firstZ+1; z < endZ; ++z) {
      if (connectivity & Connectivity::CTP) {
	for (DimSideImg x = firstX; x < endX; ++x) {
	  Point pa1 (x, y1, z), pa2 (x, y2, z-1);
	  LAMBDA_IF_NOBORDER (pa1, pa2);
	  Point pb1 (x, y1, z-1), pb2 (x, y2, z);
	  LAMBDA_IF_NOBORDER (pb1, pb2);
	}
      }
      if (connectivity & Connectivity::CTN) {
	for (DimSideImg x = firstX+1; x < endX; ++x) {
	  Point ppa1 (x-1, y1, z-1), ppa2 (x, y2, z);
	  LAMBDA_IF_NOBORDER (ppa1, ppa2);
	  Point pna1 (x, y1, z-1), pna2 (x-1, y2, z);
	  LAMBDA_IF_NOBORDER (pna1, pna2);
	  Point ppb1 (x-1, y1, z), ppb2 (x, y2, z-1);
	  LAMBDA_IF_NOBORDER (ppa1, ppa2);
	  Point pnb1 (x, y1, z), pnb2 (x-1, y2, z-1);
	  LAMBDA_IF_NOBORDER (pna1, pna2);
	}
      }
    }
  }
    break;

  case Vertical: {
    const DimSideImg x1 = rect.point.x-1, x2 = rect.point.x;
    for (DimChanel z = firstZ; z < endZ; ++z) {
      if (connectivity & Connectivity::C4) {
	for (DimSideImg y = firstY; y < endY; ++y) {
	  Point p1 (x1, y, z), p2 (x2, y, z);
	  LAMBDA_IF_NOBORDER (p1, p2);
	}
      }
      if (connectivity & Connectivity::C6P)
	for (DimSideImg y = firstY+1; y < endY; ++y) {
	  Point p1 (x1, y-1, z), p2 (x2, y, z);
	  LAMBDA_IF_NOBORDER (p1, p2);
	}
      if (connectivity & Connectivity::C6N)
	for (DimSideImg y = firstY+1; y < endY; ++y)  {
	  Point p1 (x1, y, z), p2 (x2, y-1, z);
	  LAMBDA_IF_NOBORDER (p1, p2);
	}
    }
    for (DimChanel z = firstZ+1; z < endZ; ++z) {
      if (connectivity & Connectivity::CTP)
	for (DimSideImg y = firstY; y < endY; ++y) {
	  Point pa1 (x1, y, z-1), pa2 (x2, y, z);
	  LAMBDA_IF_NOBORDER (pa1, pa2);
	  Point pb1 (x1, y, z), pb2 (x2, y, z-1);
	  LAMBDA_IF_NOBORDER (pb1, pb2);
	}
      if (connectivity & Connectivity::CTN)
	for (DimSideImg y = firstY+1; y < endY; ++y) {
	  Point ppa1 (x1, y-1, z-1), ppa2 (x2, y, z);
	  LAMBDA_IF_NOBORDER (ppa1, ppa2);
	  Point pna1 (x1, y, z-1), pna2 (x2, y-1, z);
	  LAMBDA_IF_NOBORDER (pna1, pna2);
	  Point ppb1 (x1, y-1, z), ppb2 (x2, y, z-1);
	  LAMBDA_IF_NOBORDER (ppa1, ppa2);
	  Point pnb1 (x1, y, z), pnb2 (x2, y-1, z-1);
	  LAMBDA_IF_NOBORDER (pna1, pna2);
	}
    }
  }
    break;

  case Plane: {
    const DimSideImg z1 = rect.point.z-1, z2 = rect.point.z;
    if (connectivity & Connectivity::CT)
      for (DimSideImg y = firstY; y < endY; ++y)
	for (DimSideImg x = firstX; x < endX; ++x) {
	  Point p1 (x, y, z1), p2 (x, y, z2);
	  LAMBDA_IF_NOBORDER (p1, p2);
	}
    if (connectivity & Connectivity::CTP) {
      for (DimSideImg x = firstX+1; x < endX; ++x) {
	Point pha1 (x-1, firstY, z1), pha2 (x, firstY, z2);
	LAMBDA_IF_NOBORDER (pha1, pha2);
	Point phb1 (x-1, firstY, z2), phb2 (x, firstY, z1);
	LAMBDA_IF_NOBORDER (phb1, phb2);
      }
      for (DimSideImg y = firstY+1; y < endY; ++y) {
	Point pva1 (firstX, y-1, z1), pva2 (firstX, y, z2);
	LAMBDA_IF_NOBORDER (pva1, pva2);
	Point pvb1 (firstX, y-1, z2), pvb2 (firstX, y, z1);
	LAMBDA_IF_NOBORDER (pvb1, pvb2);
	for (DimSideImg x = firstX+1; x < endX; ++x) {
	  Point pha1 (x-1, y, z1), pva1 (x, y-1, z1), pa2 (x, y, z2);
	  LAMBDA_IF_NOBORDER (pha1, pa2);
	  LAMBDA_IF_NOBORDER (pva1, pa2);
	  Point phb1 (x-1, y, z2), pvb1 (x, y-1, z2), pb2 (x, y, z1);
	  LAMBDA_IF_NOBORDER (phb1, pb2);
	  LAMBDA_IF_NOBORDER (pvb1, pb2);
	}
      }
    }
    if (connectivity & Connectivity::CTN)
      for (DimSideImg y = firstY+1; y < endY; ++y)
	for (DimSideImg x = firstX; x < endX; ++x) {
	  Point ppa1 (x-1, y-1, z1), ppa2 (x, y, z2);
	  LAMBDA_IF_NOBORDER (ppa1, ppa2);
	  Point pna1 (x-1, y, z1), pna2 (x, y-1, z2);
	  LAMBDA_IF_NOBORDER (pna1, pna2);
	  Point ppb1 (x-1, y-1, z2), ppb2 (x, y, z1);
	  LAMBDA_IF_NOBORDER (ppa1, ppa2);
	  Point pnb1 (x-1, y, z2), pnb2 (x, y-1, z1);
	  LAMBDA_IF_NOBORDER (pna1, pna2);
	}
  }
    break;

  default:
    BOOST_ASSERT (false);
  }
  return edgeCount;
}

// ========================================
template <typename WeightT, typename EdgeWeightFunction>
inline WeightT
GraphWalker::getMedian (const EdgeWeightFunction &ef /*ef.getValue (idx)*/) const {
  int bits = 8*sizeof (WeightT);
  BOOST_ASSERT (bits < 17);
  DimEdge dim = 1UL << bits;
  vector<DimImg> indices (dim+1, 0);
  DimImg *histogram = &indices[1];
  forEachVertexIdx ([&histogram, &ef] (const DimImg &idx) {
      ++histogram[(size_t) ef.getValue (idx)];
    });
  partial_sum (histogram, histogram+dim, histogram);
  return lower_bound (indices.begin (), indices.end (), indices[dim] >> 1) - indices.begin ();
}

// ================================================================================
template <typename WeightT, typename EdgeWeightFunction>
inline DimEdge
GraphWalker::getEdges (const Rect &rect, TileShape tileShape, Edge<WeightT> *edges, const EdgeWeightFunction &ef /*ef.getWeight (p0, p1)*/) const {
  BOOST_ASSERT (!rect.isNull ());
  BOOST_ASSERT (edges);
  DimEdge edgeCount = 0;
  forEachEdgePt (rect, tileShape,
		 [&edges, &edgeCount, &ef] (Point const &a, Point const &b) {
		   Edge<WeightT> &edge = edges[edgeCount++];
		   edge.points[0] = a;
		   edge.points[1] = b;
		   edge.weight = ef.getWeight (a, b);
		 });
  return edgeCount;
}

template <typename WeightT, typename EdgeWeightFunction>
inline DimEdge
GraphWalker::getSortedEdges (const Rect &rect, TileShape tileShape, Edge<WeightT> *edges, const EdgeWeightFunction &ef /*ef.getWeight (p0, p1) ef.sort ()*/) const {
  SMART_DEF_LOG ("GraphWalker::getSortedEdges", "");
  DimEdge edgeCount = getEdges (rect, tileShape, edges, ef);
  // SMART_LOG (endl << printEdges (edges, border.getSize (), edgeCount));
  ef.sort (edges, edgeCount);
  SMART_LOG (endl << printEdges (edges, border.getSize (), edgeCount))
    return edgeCount;
}

template <typename WeightT, typename EdgeWeightFunction>
// template DimImg
inline DimEdge
GraphWalker::getCountingSortedEdges (const Rect &rect, TileShape tileShape, Edge<WeightT> *edges, const EdgeWeightFunction &ef /*ef.getWeight (p0, p1)*/) const {
  SMART_DEF_LOG ("GraphWalker::getCountingSortedEdges", "");
  int bits = 8*sizeof (WeightT);
  BOOST_ASSERT (bits < 17);
  DimEdge dim = 1UL << bits;
  vector<DimImg> counters (dim+1, 0);
  DimImg *indices = &counters [0];
  DimImg *histogram = &indices [1];
  DimImg sum = 0;
  forEachEdgePt (rect, tileShape,
		 [&histogram, &ef] (Point const &a, Point const &b) {
		   ++histogram[(size_t) ef.getWeight (a, b)];
		 });
  
  // get indices by prefix sum
  partial_sum (histogram, histogram+dim, histogram);
  sum = indices[dim];
  if (ef.getDecr ()) {
    for (size_t i = 0; i < dim; ++i)
      histogram[i] = sum - histogram[i];
    indices++;
  } else
    indices[0] = 0;
  // extract edges
#ifdef ENABLE_SMART_LOG
  const Size &size (border.getSize ());
#endif
  forEachEdgePt (rect, tileShape,
		 [
#ifdef ENABLE_SMART_LOG
		  &size,
#endif
		  &ef, &edges, &indices] (Point const &a, Point const &b) {
		   WeightT weight = ef.getWeight (a, b);
		   Edge<WeightT> &edge = edges[indices[(size_t) weight]++];
		   edge.points[0] = a;
		   edge.points[1] = b;
		   edge.weight = weight;
		   SMART_LOG_EXPR (cerr << printEdge (edge, size) << endl);
		 });
  SMART_LOG (endl << printEdges (edges, size, sum));
  return sum;
}

// ================================================================================
#endif // _Obelix_Triskele_GraphWalker_tpp
