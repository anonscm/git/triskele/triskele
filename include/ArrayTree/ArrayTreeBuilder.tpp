#ifndef _Obelix_Triskele_ArrayTreeBuilder_tpp
#define _Obelix_Triskele_ArrayTreeBuilder_tpp


// ================================================================================
template<typename WeightT, typename PixelT>
template<typename WeightFunct>
inline void
ArrayTreeBuilder<WeightT, PixelT>::setAttributProfiles (AttributeProfiles<PixelT> &attributeProfiles, const WeightFunct &weightFunct) {
  PixelT *leafAP = attributeProfiles.getValues ();
  dealThread (leafCount, coreCount, [&weightFunct, &leafAP] (const DimCore &threadId, const DimImg &minVal, const DimImg &maxVal) {
      weightFunct.copyPixelsBound (leafAP, minVal, maxVal);
    });
  PixelT *compAP = leafAP+leafCount;
  dealThread (getCompCount (), coreCount, [this, &weightFunct, &compAP] (const DimCore &threadId, const DimImg &minVal, const DimImg &maxVal) {
      weightFunct.weight2valueBound (compAP, compWeights, minVal, maxVal);
    });
}

// template<typename WeightT, typename PixelT>
// inline ArrayTreeBuilder<WeightT, PixelT>::CPrintComp
// ArrayTreeBuilder<WeightT, PixelT>::printComp (const DimImg &compId) const {
//   return ArrayTreeBuilder<WeightT, PixelT>::CPrintComp (*this, compId);
// }

// template<typename WeightT, typename PixelT>
// inline ostream&
// operator << (ostream& out, const ArrayTreeBuilder<WeightT, PixelT>::CPrintComp &lc) {
//   return lc.print (out);
// }

// ================================================================================
  
#endif // _Obelix_Triskele_ArrayTreeBuilder_tpp
