#ifndef _Obelix_Triskele_Leader_hpp
#define _Obelix_Triskele_Leader_hpp

#include <memory>
#include <vector>
#include <boost/assert.hpp>

#include "triskeleBase.hpp"
#include "triskeleArrayTreeBase.hpp"

namespace obelix {
  namespace triskele {

    // ================================================================================
    class Leader {
    private:

      /*! Taille de l'image (donc des tableaux leaderSetSize et leader) */
      DimImg size;

      /*! Tableau des leaders, chaque case contient une référence vers principal leader connu */
      vector<DimImg> leaders;

    public:
      Leader (DimImg vertexCount = 0);
      ~Leader ();

      /*! Remet à 0 et redéfinit la taille des tableaux */
      void book (DimImg vertexCount);

      /*! Libère la mémoire allouée par les tableaux et met size à 0 */
      void free ();

      /*! Cherche le leaders du pixel a, Si a n'en a pas, cela retourne a */
      inline DimImg	find (DimImg a) const;

      /*! Rédéfinit les leaders : a et tous les leaders de a ont pour leader r */
      inline void	link (DimImg a, const DimImg &r);

      DimImg *getLeaders ();
      vector<DimImg> &getLeadersVector ();
    };

    // ================================================================================
#include "Leader.tpp"

  } // triskele
} // obelix


#endif // _Obelix_Triskele_Leader_hpp
