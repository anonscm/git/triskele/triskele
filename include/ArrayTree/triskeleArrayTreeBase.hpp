#ifndef _Obelix_Triskele_ArrayBase_hpp
#define _Obelix_Triskele_ArrayBase_hpp

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <boost/assert.hpp>
#include <climits>

#include "triskeleBase.hpp"

namespace obelix {
  namespace triskele {

    // ================================================================================
    class GraphWalker;
    using namespace std;

    /*! Nombre de voisins */
    typedef uint32_t DimEdge;		// ~4*nbPixels (8-connectivity)

    /*! Type de connectivité possible pour le voisinage */
    enum Connectivity { C1 = 0, C4 = 1, C6P = 2, C6N = 4, C8 = (C4|C6P|C6N), CT = 8, CTP = 16, CTN = 32};
    /*! Définit de quel type est la "sélection" */
    enum TileShape { Volume, Horizontal, Vertical, Plane };
    /*! Définit le type d'arbre à construire */
    enum TreeType { MIN, MAX, TOS, ALPHA};

    inline bool getDecrFromTreetype (const TreeType &treeType);
      
    /*! Définit les noms des tileShape (utile pour le debug) */
    extern const string tileShapeLabels[];
    extern const map<string, TileShape> tileShapeMap;
    /*! Définit les noms des arbres (utile pour le debug) */
    extern const string treeTypeLabels[];
    extern const map<string, TreeType> treeTypeMap;

    /*! Opérateur de flux sur les connectivités */
    ostream &operator << (ostream &out, const Connectivity &c);
    istream &operator >> (istream &in, Connectivity &c);
    /*! Opérateur de flux sur les TileShape */
    ostream &operator << (ostream &out, const TileShape &tileShape);
    istream &operator >> (istream &in, TileShape &tileShape);
    /*! Opérateur de flux sur les connectivités */
    ostream &operator << (ostream &out, const TreeType &treeType);
    istream &operator >> (istream &in, TreeType &treeType);

    // ================================================================================
    template <typename WeightT> struct Edge {
      Point points[2];
      WeightT weight;
    };

    /*! Effectue l'échange entre 2 Edge */
    template <typename WeightT>
    inline void swapEdge (Edge<WeightT> &a, Edge<WeightT> &b);

    /*! Opérateur de flux sur les voisins */
    template <typename WeightT>
    struct CPrintEdge {
      const Edge<WeightT> &edge;
      const Size &size;
      inline CPrintEdge (const Edge<WeightT> &edge, const Size &size);
      inline ostream &print (ostream &out) const;
    };
    template <typename WeightT>
    inline CPrintEdge<WeightT> printEdge (const Edge<WeightT> &edge, const Size &size);

    template <typename WeightT>
    inline ostream &operator << (ostream& out, const CPrintEdge<WeightT> &cpe) { return cpe.print (out); }

    template <typename WeightT>
    struct CPrintEdges {
      const Edge<WeightT> *edges;
      const Size &size;
      const DimEdge edgesCount;
      inline CPrintEdges (const Edge<WeightT> edges[], const Size &size, const DimEdge edgesCount);
      inline ostream &print (ostream &out) const;
    };
    template <typename WeightT>
    inline CPrintEdges<WeightT> printEdges (const Edge<WeightT> edges[], const Size &size, const DimEdge edgesCount);

    template <typename WeightT>
    inline ostream &operator << (ostream& out, const CPrintEdges<WeightT> &cpe) { return cpe.print (out); }

    // ================================================================================
#include "triskeleArrayTreeBase.tpp"
  } // triskele
} // obelix


#endif // _Obelix_Triskele_ArrayBase_hpp
