#ifndef _Obelix_Triskele_CompAttribute_hpp
#define _Obelix_Triskele_CompAttribute_hpp

#include <iostream>
#include <vector>
#include <map>

#include "obelixThreads.hpp"
#include "triskeleBase.hpp"
#include "Tree.hpp"
#include "TreeStats.hpp"
#include "AttributeProfiles.hpp"

namespace obelix {
  namespace triskele {

    using namespace std;

    enum AttributeType {
      AArea, AWeight, ASD, AMoI //, Average, Bound
    };
    extern const string attributeLabels[];
    extern const map<string, AttributeType> attributeMap;
    ostream &operator << (ostream &out, const AttributeType &attributeType);
    istream &operator >> (istream &in, AttributeType &attributeType);

    
    /*! Type d'attribut pour réaliser un filtrage */
    enum FeatureType {
      FAP, FArea, FMean, FSD, FMoI
    };
    /*! Définit les noms des connectivités (utile pour le debug) */
    extern const string featureTypeLabels[];
    extern const map<string, FeatureType> featureTypeMap;
    /*! Opérateur de flux sur les connectivités */
    ostream &operator << (ostream &out, const FeatureType &featureType);
    istream &operator >> (istream &in, FeatureType &featureType);

    // ================================================================================
    template<typename AttrT>
    class CompAttribute {
    public:
      static vector<AttrT> getScaledThresholds (const vector<double> &thresholds, const AttrT &maxValue);
      static vector<AttrT> getConvertedThresholds (const vector<double> &thresholds);

      CompAttribute (const Tree &tree, const bool &decr = false);
      virtual ~CompAttribute ();

      void updateTranscient ();
      const AttrT *getValues () const;
      AttrT *getValues ();
      AttrT getMaxValue () const;
      bool getDecr () const;

      template<typename PixelT>
      void cut (vector<vector<PixelT> > &allBands, const AttributeProfiles<PixelT> &attributeProfiles,
		const AttrT &pixelAttrValue, const vector<AttrT> &thresholds) const;

      template<typename PixelT>
      void cutOnPos (vector<vector<PixelT> > &allBands, const AttributeProfiles<PixelT> &attributeProfiles,
		     const DimImg &pixelId, const AttrT &pixelAttrValue, const vector<AttrT> &thresholds) const;

      virtual inline ostream &print (ostream &out) const { print (out, ""); return out; }

    protected:
      const Tree &tree;
      DimNodeId leafCount;
      vector<AttrT> values;
      bool decr;

      void free ();
      void book (const DimImg &leafCount);

      template<typename CumpFunctPSE>
      inline void computeSameCompLevel (const CumpFunctPSE &cumpFunctPSE /* cumpFunctPSE (DimImg parentId)*/) const;

      ostream &print (ostream &out, const string &msg) const;
      friend ostream &operator << (ostream& out, const CompAttribute &ca) { return ca.print (out); return out; }

    };

    // ================================================================================

  } // triskele
} // obelix
#include "Attributes/WeightAttributes.hpp"
namespace obelix {
  namespace triskele {
#include "CompAttribute.tpp"
  } // triskele
} // obelix

#endif // _Obelix_Triskele_CompAttribute_hpp
