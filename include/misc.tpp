#ifndef _Obelix_Misc_tpp
#define _Obelix_Misc_tpp

inline uint8_t
obelix::ones64 (uint64_t x) {
  x  = ((x >> 1) & 0x5555555555555555UL) + (x & 0x5555555555555555UL);
  x  = ((x >> 2) & 0x3333333333333333UL) + (x & 0x3333333333333333UL);
  x  = ((x >> 4) + x) & 0x0f0f0f0f0f0f0f0fUL;
  x += (x >> 8);
  x += (x >> 16);
  x += (x >> 32);
  return (x & 0x3f);
}

inline uint8_t
obelix::size64 (uint64_t x) {
  x |= (x >> 1);
  x |= (x >> 2);
  x |= (x >> 4);
  x |= (x >> 8);
  x |= (x >> 16);
  x |= (x >> 32);
  return ones64 (x);
}

#endif // _Obelix_Misc_tpp
