#ifndef _Obelix_InteGralImage_hpp
#define _Obelix_InteGralImage_hpp

namespace obelix {

  // ================================================================================
  template<typename PixelT>
  inline void computeIntegralImage (const Size &size, const PixelT *inputPixels, double *iiValues);

  template<typename PixelT>
  inline void doubleMatrix (const Size &size, const PixelT *inputPixels, double *doubleValues);

  // ================================================================================
  template<typename PixelT>
  inline void
  computeIntegralImage (const Size &size, const PixelT *inputPixels, double *iiValues) {
    const DimImg firstX = 1, lastX = DimImg (size.width);
    const DimImg firstY = DimImg (size.width), lastY = DimImg (size.height)*DimImg (size.width);
    double value = 0;
    // [0][0]
    // iiValues [0 + 0] = double (inputPixels [0 + 0]);
    for (DimImg x = 0; x < lastX; ++x)
      // [x][0]
      iiValues [x + 0] = value += double (inputPixels [x + 0]);
    value = double (inputPixels [0 + 0]);
    for (DimImg y = firstY; y < lastY; y += size.width)
      // [0][y]
      iiValues [0 + y] = value += double (inputPixels [0 + y]);
    for (DimImg prevY = 0, y = firstY; y < lastY; prevY = y, y += size.width) {
      for (DimImg prevX = 0, x = firstX; x < lastX; prevX = x, ++x)
	// [x][y]
	iiValues [x + y] =
	  double (inputPixels [x + y])
	  + iiValues [x + prevY]
	  + iiValues [prevX + y]
	  - iiValues [prevX + prevY];
    }
  }

  // ================================================================================
  template<typename PixelT>
  inline void
  doubleMatrix (const Size &size, const PixelT *inputPixels, double *doubleValues) {
    const DimImg pixelCount = DimImg (size.width)*DimImg (size.height);
    for (DimImg i = 0; i < pixelCount; ++i, ++inputPixels, ++doubleValues)
      *doubleValues = double (*inputPixels) * double (*inputPixels);
  }

  // ================================================================================
}

#endif // _Obelix_InteGralImage_hpp
