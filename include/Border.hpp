#ifndef _Obelix_Border_hpp
#define _Obelix_Border_hpp

#include <iostream>
#include <vector>
#include <algorithm>
#include <boost/assert.hpp>

#include "triskeleBase.hpp"
//#include "triskeleArrayTreeBase.hpp"


namespace obelix {
	using namespace std;
  // ================================================================================
  /** Border */
  class Border {
  public:
    /*! Retourne la taille que doit faire le tableau simplifié */
    static DimImg getMapLength (const DimImg &pixelsCount);

    /*! Indique qu'une carte de bordure est présente (donc potentiellement des pixels de bordures). */
    bool exists () const;

    /*! Vérifie si un index est sur la map ou une bordure, en se basant sur le bit de l'index idx de la map */
    inline bool isBorder (const DimImg &idx) const;

    /*! Vérifie si un point est sur la map ou une bordure en appelant la méthode précédente après avoir converti le point en index */
    inline bool isBorder (const Point &p) const;

    /*! Supprime toutes les occurences de bordure à l'index idx */
    void clearBorder (const DimImg &idx);

    /*! Supprime toutes les occurences de bordure en appelant la méthode précédente après avoir converti le point en index */
    void clearBorder (const Point &p);

    /*! Rajoute une occurence de bordure à l'index indiqué */
    void setBorder (const DimImg &idx);

    /*! Rajoute une occurence de bordure en appelant la méthode précédente après avoir converti le point en index */
    void setBorder (const Point &p);

    /*! Construit par défault sans aucune bordure */
    Border ();

    /*! Construit Border, les valeurs de map dépendent de defaultVal */
    Border (const Size &size, bool defaultVal);
    Border (const Border &border, const Rect &tile);
    ~Border ();

    void reset (bool defaultVal);

    void reduce ();

    const Size &getSize () const;

    /*! Compte le nombre de pixels considérés comme de la bordure */
    DimImg borderCount () const;

  private:
    /*! Nombre de pixels dans l'image */
    DimImg pixelsCount;

    /*! Taille du tableau "simplifié" des pixels */
    DimImg mapLength;

    /*! Dimensions de l'image */
    const Size size;

    /*! Tableau "simplifié" des pixels */
    vector<uint8_t> map;

    bool defaultVal;

    void createMap ();

    friend ostream &operator << (ostream &out, const Border &border);
  };
  ostream &operator << (ostream &out, const Border &border);

  // ================================================================================
#include "Border.tpp"
  
} // triskele

#endif // _Obelix_Border_hpp
