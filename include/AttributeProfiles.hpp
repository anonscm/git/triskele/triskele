#ifndef _Obelix_Triskele_AttributeProfiles_hpp
#define _Obelix_Triskele_AttributeProfiles_hpp

#include "Tree.hpp"

namespace obelix {
  namespace triskele {

    // ================================================================================
    template<typename PixelT>
    class AttributeProfiles {
    public:
      AttributeProfiles (const Tree &tree);
      ~AttributeProfiles ();

      void		updateTranscient ();
      PixelT		*getValues ();
      const PixelT	*getValues () const;

      template<typename WeightT>
      void setValues (const PixelT defaultValue, const WeightT *weights);

      template<typename WeightT>
      void setValues (const PixelT *pixels, const WeightT *weights);

    protected:
      const Tree	&tree;
      DimNodeId		leafCount;
      vector<PixelT>	values;

      void free ();
      void book (const DimImg &leafCount);

      ostream &print (ostream &out) const;
      friend ostream &operator << (ostream& out, const AttributeProfiles &ap) { return ap.print (out); }
    };

    // ================================================================================
  } // triskele
} // obelix

#endif // _Obelix_Triskele_AttributeProfiles_hpp
