# IGRIDA flags
DFLAGS = -g -DENABLE_LOG -DNO_OTB # -DTHREAD_DISABLE -DENABLE_SMART_LOG
IFLAGS = $(DFLAGS) -MMD -I$(HPP_DIR) -I/soft/igrida/gdal/gdal-2.3.0/include -I/soft/igrida/boost/boost-1.67.0/include/boost
LFLAGS = -L$(LIB_DIR) -L/soft/igrida/gdal/gdal-2.3.0/lib -L/soft/igrida/boost/boost-1.67.0/lib -ltriskele -lstdc++ -lpthread -lboost_system -lboost_chrono -lboost_thread -lboost_program_options -lboost_date_time -lboost_serialization -lboost_filesystem -lboost_unit_test_framework -lgdal -ltbb
CC = g++ -std=c++11
